# -*- coding: utf-8 -*-

"""
:synopsis: Classes used to import and fetch data into db_models.py model classes.

:created:    2011/03/08

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.16.2'
__UPDATED__ = '2018-11-19'

#===============================================================================
# Imports
#===============================================================================
import datetime
import contextlib
import csv
import os
import re
import urllib
import urllib2
from collections import defaultdict
from operator import attrgetter
from xml.etree.cElementTree import ElementTree  #X-NOTE: Faster C version imported
from zipfile import ZipFile, BadZipfile

import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker

import proteomics.pepprot_func as ppf
import proteomics.db_models as dbm
from general.utilities import Logger
from general.basic_func import iter_strs_with_strs_in
# from math_extras.pystats import mean #Used in :method:`Nguyen16PhosFetcher._get_mean_mass4row`.


#===============================================================================
# Global variables
#===============================================================================
# Folder containing ZIP DB files:
DBS_FOLDER = '/home/oga/Laboratori de Proteomica CSIC-UAB/Bases de Dades/'
# Folder containing ZIP TSV paper files:
PAPERS_FOLDER = 'PTM articles for PHOS UB AC in Jurkat 2015-03-16'
# Folder containing MS Data Results files to be imported:
RESULTS_FOLDER = '/home/oga/Laboratori de Proteomica CSIC-UAB/LymPHOS-UB-AC/'

MIN_PEPLENGTH = 6 # Default Minimum number of amino-acids for a Peptide sequence to be imported.

# ZIP file where local UniProt XML and FASTA files reside:
UNIPROT_FILE = os.path.join(DBS_FOLDER, 'UniProtKB/Human reference proteome - UP000005640 rev.02-2016 - 2016-04-18/splitted_compressed.zip')
# ZIP file containing a "Gene Ontology Terms" OBO file:
OBO_GO_FILE = os.path.join(DBS_FOLDER, 'GOTerms/go-basic-obo.zip')
# ZIP file containing an "Evidence and Conclusions Ontology" OBO file:
OBO_ECO_FILE = os.path.join(DBS_FOLDER, 'Evidence and Conclusions Ontology/eco-obo.zip')
# ZIP file containing an UniProt gaf-version 2.0 "gene_association.goa_<species>" file:
UNIPROTGOA_FILE = os.path.join(DBS_FOLDER, 'UniProt-GOA/gene_association.goa_human.zip')
# ZIP file containing Experiments and SupraExperiments information TSV tables:
EXPERIMENTS_FILE = os.path.join(DBS_FOLDER, 'Experiments/crosstalk_experiments_worksheet.zip')
# ZIP file containing Choudhary et al. 2009 supplementary information TSV tables:
CHOUDHARY09_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Choudhary_09_Acetylations_SupplementaryTableS4.zip')
# ZIP file containing Udeshi et al. 2013 supplementary information TSV tables:
UDESHI13_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Udeshi_13_Ubiquitinations_SupplementaryTable2.zip')
# ZIP file containing Mayya et al. 2009 supplementary information TSV tables:
MAYYA09_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Mayya_09_Phosphorylations_TableS1.zip')
# ZIP file containing Svinkina et al. 2015 supplementary information TSV tables:
SVINKINA15_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Svinkina_14_Acetylations_3-All_Jurkat_SAHA_KacPeptides.zip')
# ZIP file containing Nguyen et al. 2016 supplementary information TSV tables:
NGUYEN16_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Nguyen_16_Phosphorylations_Jurkat_antiCD3CD28.zip')
# ZIP file containing Watts et al. 1994 TSV tables:
WATTS94_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Watts 1994 ZAP-70 pTyr.zip')
# ZIP file containing Salomon et al. 2003 TSV tables:
SALOMON03_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Salomon 2003 Table 1 pTyr.zip')
# ZIP file containing Ficarro et al. 2005 TSV tables:
FICARRO05_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Ficarro 2005 Table 3 pTyr.zip')
# ZIP file containing Tao et al. 2005 TSV tables:
TAO05_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Tao 2005 Supplementary Table 1 pTyr.zip')
# ZIP file containing Mertins et al. 2013 TSV tables:
MERTINS13_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Mertins_13_SupplementaryTable1.zip')
# ZIP file containing Jouy et al. 2015 TSV tables:
JOUY17_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Jouy_15_Phosphorylations_toplists_SupplementaryTables1and2.zip')
# ZIP file containing Beltejar et al. 2017 TSV dataset:
BELTEJAR17_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Beltejar_17_Phosphorylations_SupplementaryDataset1_RAW.zip')

# ZIP file containing Proteome Discoverer 1.4 PSMs as TSV tables:
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB Fracionament bRP - PD1.4 - 2016-04-10/IAP_bRP_30_F1-F5_PSMs.tsv.zip') #Preliminar Ub only data from a fractionation experiment.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP - PD1.4 - 2016-07-14/Dades_UB_and_AC_PD1.4_2016-07-14.zip') #Ub and Ac Data from fractionation experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP - PD1.4 - 2016-10-17/Dades UB i AC Fracionament bRP - PD1.4 - 2016-10-17.zip') #Ub and Ac Data from fractionation experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Exp. 5 Reports - PD1.4 - 2016-10-17/Dades UB i AC Exp. 5 Reports - PD1.4 - 2016-10-17.zip') #Ub and Ac Data from 5 Reports experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - 2016-10-17/Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - 2016-10-17.zip') #Ub and Ac Data from fractionation and 5 Reports experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Test 2016-10-26/Test 2016-10-26.zip') #Test for Ub and Ac Data from fractionation and 5 Reports experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - 2017-11-24/Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - Search Engine Rank 1 - 2017-11-24.TSV.zip') #Ub and Ac Data from fractionation AND 5 Reports experiments.
PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - 2017-11-24/Dades UB i AC Fracionament bRP ONLY (acE17 and RPL310) - PD1.4 - Search Engine Rank 1 - 2017-11-24.TSV.zip') #Ub and Ac Data from fractionation experiments ONLY.

# URI for the local MySQL LymPHOS DataBase:
LYMPHOSDB_URI = 'mysql://root@localhost/lymphos_lymphos2allpepd' #Development
# LYMPHOSDB_URI = 'mysql://root@localhost/lymphos_lymphos2allpep' #Production

# PTMs Data:
PHOSPHORYLATION = {'ModelCls': dbm.ModificationType, 'id': 21, 'name': 'Phospho', 
                   'only_physio': True}
PHOSPHORYLATION_EXTRADATA = { 'unimod_id': 21, 'delta_mass': 79.966331,
                              'unimod_name': 'Phospho', 'residues': ['S','T','Y'],
                              'full_name': 'Phosphorylation', 'abbreviation': 'phos' }
ACETYLATION = {'ModelCls': dbm.ModificationType, 'id': 1, 'name': 'Acetyl', 
               'only_physio': True}
ACETYLATION_EXTRADATA = { 'unimod_id': 1, 'delta_mass': 42.010565,
                          'unimod_name': 'Acetyl', 'residues': ['K','N-Term'],
                          'full_name': 'Acetylation', 'abbreviation': 'ac' }
METHYLATION = {'ModelCls': dbm.ModificationType, 'id': 34, 'name': 'Methyl', 
               'only_physio': True}
METHYLATION_EXTRADATA = { 'unimod_id': 34, 'delta_mass': 14.015650,
                          'unimod_name': 'Methyl', 'residues': ['R','K','N-Term'],
                          'full_name': 'Methylation', 'abbreviation': 'mth' }
UBIQUITINATION = {'ModelCls': dbm.ModificationType, 'id': 121, 'name': 'GG', 
                  'only_physio': True}
UBIQUITINATION_EXTRADATA = { 'unimod_id': 121, 'delta_mass': 114.042927,
                             'unimod_name': 'GG', 'residues': ['K'],
                             'full_name': 'Ubiquitination', 'abbreviation': 'ub' }
OXIDATION = {'ModelCls': dbm.ModificationType, 'id': 35, 'name': 'Oxidation', 
             'only_physio': False}
OXIDATION_EXTRADATA = { 'unimod_id': 35, 'delta_mass': 15.994915,
                        'unimod_name': 'Oxidation', 'residues': ['M'],
                        'full_name': 'Oxidation', 'abbreviation': 'ox' }
# IDs (Unimod Accessions) of modifications WITHOUT a Clear Physiological Origin, 
# so they can be excluded from some operations (`seq_w_mods` and others):
NO_PHYSIO_MDDIDS = dbm.NO_PHYSIO_MDDIDS #It's the same global object as of `db_models` module (so changing it would change also the one in `db_models`).
NO_PHYSIO_MDDIDS.update( {4, 35} ) #Update with Cys Carbamidomethylation and Met Oxidation.

# Organisms and Cell Lines Data:
HUMAN = {'ModelCls': dbm.Organism, 'id': 9606, 'name': 'Homo sapiens'}
TCELLS = {'ModelCls': dbm.CellLine, 'name': 'Primary T lymphocyte'}
JURKAT = {'ModelCls': dbm.CellLine, 'name': 'Jurkat T lymphocyte'}
JURKAT_E6_1 = {'ModelCls': dbm.CellLine, 'name': 'Jurkat E6-1 T lymphocyte'}
CELLLINES = (TCELLS, JURKAT, JURKAT_E6_1)

# Repositories/Sources Data:
OBOGOTERMS = {'ModelCls': dbm.Source, 'name': 'Gene Ontology OBO file', 
              'url': 'http://geneontology.org'}
UNIPROTKB = {'ModelCls': dbm.Source, 'name': 'UniProt', 
             'url': 'http://www.uniprot.org'}
UNIPROTGOA = {'ModelCls': dbm.Source, 'name': 'UniProt-GOA', 
              'url': 'http://www.ebi.ac.uk/GOA'}
LYMPHOS2 = {'ModelCls': dbm.Source, 'name': 'LymPHOS2', 
            'url': 'https://www.lymphos.org'}
CHOUDHARY = {'ModelCls': dbm.Source, 'name': 'Choudhary et al. 2009', 
             'version': '2009', 'url': 'http://www.sciencemag.org/content/325/5942/834'}
SVINKINA = {'ModelCls': dbm.Source, 'name': 'Svinkina et al. 2015', 
            'version': '2015', 'url': 'http://www.mcponline.org/content/14/9/2429'}
UDESHI = {'ModelCls': dbm.Source, 'name': 'Udeshi et al. 2013', 
          'version': '2013', 'url': 'http://www.mcponline.org/content/12/3/825'}
MAYYA = {'ModelCls': dbm.Source, 'name': 'Mayya et al. 2009', 
         'version': '2009', 'url': 'http://stke.sciencemag.org/content/2/84/ra46.abstract'}
NGUYEN = {'ModelCls': dbm.Source, 'name': 'Nguyen et al. 2016', 
          'version': '2016', 'url': 'http://www.sciencedirect.com/science/article/pii/S1874391915301731'}
WATTS = {'ModelCls': dbm.Source, 'name': 'Watts et al. 1994', 'version': '1994', 
         'url': 'http://www.jbc.org/content/269/47/29520'}
SALOMON = {'ModelCls': dbm.Source, 'name': 'Salomon et al. 2003', 
           'version': '2003', 'url': 'http://www.ncbi.nlm.nih.gov/pmc/articles/PMC141014/'}
FICARRO = {'ModelCls': dbm.Source, 'name': 'Ficarro et al. 2005', 
           'version': '2005', 'url': 'http://onlinelibrary.wiley.com/doi/10.1002/rcm.1746/'}
TAO = {'ModelCls': dbm.Source, 'name': 'Tao et al. 2005', 'version': '2005', 
       'url': 'https://www.nature.com/nmeth/journal/v2/n8/full/nmeth776.html'}
MERTINS = {'ModelCls': dbm.Source, 'name': 'Mertins et al. 2013', 
           'version': '2013', 'url': 'https://www.nature.com/articles/nmeth.2518'}
JOUY = {'ModelCls': dbm.Source, 'name': 'Jouy et al. 2015', 'version': '2015', 
        'url': 'https://onlinelibrary.wiley.com/doi/abs/10.1002/pmic.201400119'}
BELTEJAR = {'ModelCls': dbm.Source, 'name': 'Beltejar et al. 2017', 'version': '2017', 
            'url': 'http://www.pnas.org/content/114/30/E6240.long'}
CROSSTALK = {'ModelCls': dbm.Source, 'name': 'TCellXTalkDB', 'version': '2017', 
             'url': 'https://www.crosstalk.org'}
REPOSITORIES = (OBOGOTERMS, UNIPROTKB, UNIPROTGOA, LYMPHOS2, CHOUDHARY, 
                SVINKINA, UDESHI, MAYYA, NGUYEN, WATTS, SALOMON, FICARRO, TAO, 
                MERTINS, JOUY, BELTEJAR, CROSSTALK)

# Repository/Source Types:
ST_PROTEINS = {'ModelCls': dbm.SourceType, 'id': 1, 'name': 'Proteins'} # Protein data.
ST_MSDATA = {'ModelCls': dbm.SourceType, 'id': 2, 'name': 'MS Data'} # Mass Spectrometry data.
ST_GO = {'ModelCls': dbm.SourceType, 'id': 3, 'name': 'Gene Ontology'} # Gene Ontology (GO) data.


#===============================================================================
# Importer/converter class definitions
#===============================================================================
class Fetcher(object):
    """
    Base class for data filters/importers.
    """
    _DEFAULT_REPO_DATA = None # Dictionary containing data for a default repository (:class:`Source` instance), if None is suplied.
    _DEFAULT_REPO_TYPES = None # Iterable of dictionaries containing data for a repository type (:class:`SourceType` instance) to link to the default repository.
    
    def __init__(self, db_session=None, repository=None, caching=True, 
                 cache_limit=None):
        """
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to get stored data and put some of the new created data 
        (see :method:`_select_or_create` below). 
        Usually it's the database session that will also be used by the 
        function/method/program using the `Fetcher` instance to store the 
        imported data.
        Defaults to None (no database Session).
        :param object repository: optional, :class:`Source` object representing 
        the repository used to fetch the data from. Defaults to None.
        :param bool caching: indicates if the converter should use a caching
        strategy for some created objects. Defaults to True.
        :param int cache_limit: optional, maximum number of objects allowed in
        the cache; or None (no limit). Defaults to None (no limit).
        """
        self._db_session = None #SQLAlchemy database session.
        self._repository = None #A :class:`Source` object.
        self._models_cache = defaultdict(dict) #The cache object: {model class: {ID: model object, ...}, ...}
        self.__cache_count = 0 #Number of model objects in the cache.
        #
        self._caching = caching #True -> Use cache (``_models_cache``). False -> No models caching.
        self._cache_limit = cache_limit #Maximum number of model objects in the cache before emptying it.
        #
        self.db_session = db_session
        self.repository = repository
        self.logger = Logger()
    
    def _select_or_create(self, ModelCls, **kwargs):
        """
        If :attr:``db_session`` is set, try to select a `ModelCls` instance
        from the Database, according to the keyword arguments passed as
        filters, if it already exists; otherwise creates a new model instance.
        
        :param class model: a SQLAlchemy model class.
        
        :return tuple : an instance of the `model` passed, and a boolean
        indicating if this is a newly created instance (True) or not (False).
        """
        # This method is Dynamically Set, at :method:`db_session` setter, to
        # one of :method:`__select_or_create`, or :method:`__select_create`,
        # wrapping it with cache machinery (:metod:`__cache_wrapper`) or not.
        raise NotImplementedError('This method is Dynamically Set, at `self.db_session` setter')
    
    def __select_or_create(self, ModelCls, **kwargs):
        return dbm.select_or_create(self.db_session, ModelCls, **kwargs)
    
    def __select_create(self, ModelCls, **kwargs):
        return ModelCls(**kwargs), True
    
    def _get_or_create(self, ModelCls, pk, **extrakwargs):
        """
        If :attr:``db_session`` is set, try to get a `ModelCls` instance from
        the Database, using its Primary Key (`pk`), if it already exists;
        otherwise creates a new model instance with the supplied `pk` and
        optional extra keyword arguments (`extrakwargs`).
        
        :param class ModelCls: a SQLAlchemy model class.
        :param object pk: a Primary Key value for the specified `ModelCls`.
        
        :return tuple : an instance of the `ModelCls` passed, and a boolean
        indicating if this is a newly created instance (True) or not (False).
        """
        # This method is Dynamically Set, at :method:`db_session` setter, to
        # one of :method:`__get_or_create`, or :method:`__get_create`, wrapping
        # it with cache machinery (:metod:`__cache_wrapper`) or not.
        raise NotImplementedError('This method is Dynamically set at `self.db_session` setter')
    
    def __get_or_create(self, ModelCls, pk, **extrakwargs):
        return dbm.get_or_create(self.db_session, ModelCls, pk, **extrakwargs)
    
    def __get_create(self, ModelCls, pk, **extrakwargs):
        extrakwargs.update( {sqlalchemy.inspect(ModelCls).primary_key[0].name: pk} )
        return ModelCls(**extrakwargs), True
    
    def __cache_wrapper(self, wrapped_method):
        """
        Decorator/Wrapper to add caching functionality to other methods.
        """
        def cached_method(self, ModelCls, *args, **kwargs):
            gid = frozenset( [args] + kwargs.items() ) #GroupID for a tuple of positional arguments and a group of keyword arguments and their values.
            cached_obj = self._models_cache[ModelCls].get(gid, None)
            if cached_obj:
                return cached_obj, False
            else:
                model_obj, isnew = wrapped_method(ModelCls, *args, **kwargs)
                # Clear the cache if there is a maximum limit of objects to be 
                # cached and it's reached:
                if self._cache_limit is not None:
                    if self.__cache_count < self._cache_limit: #Maximum limit Not reached:
                        self.__cache_count += 1
                    else: #Maximum limit reached:
                        self._models_cache.clear()
                        self.__cache_count = 1
                # Add new model object to the cache:
                self._models_cache[ModelCls][gid] = model_obj
                return model_obj, isnew
        return cached_method.__get__(self) #X-NOTE: turn :func:`cached_method` into a bound method of instance ``self`` ( http://stackoverflow.com/a/28060251 ).
    
    # ``db_session`` read/write property:
    @property
    def db_session(self):
        return self._db_session
    @db_session.setter
    def db_session(self, session):
        self._db_session = session
        self._repository = None
        self._models_cache.clear()
        # Dynamically Sets both :method:`_select_or_create` and
        # :method:`_get_or_create`, with o without the cache machinery of
        # :method:`__cache_wrapper`:
        if session is None: # No :class:`Session` instance available, so directly Create a model instance:
            if self._caching:
                self._select_or_create = self.__cache_wrapper(self.__select_create)
                self._get_or_create = self.__cache_wrapper(self.__get_create)
            else:
                self._select_or_create = self.__select_create
                self._get_or_create = self.__get_create
        else: # A :class:`Session` instance is available, so Look-up first in the Database:
            if self._caching:
                self._select_or_create = self.__cache_wrapper(self.__select_or_create)
                self._get_or_create = self.__cache_wrapper(self.__get_or_create)
            else:
                self._select_or_create = self.__select_or_create
                self._get_or_create = self.__get_or_create
    
    def _get_default_repository(self):
        """
        Simple method to get/create a default repository, when none is
        provided. In this case, it would be called once by :prop:`repository` 
        getter.
        Overwrite this method in subclasses instead of overwriting
        :prop:`repository` getter.
        
        :return object : a default :class:`Source` instance from data contained
        in :attr:`_DEFAULT_REPO_DATA` dictionary; or None.
        """
        repository = None
        if self._DEFAULT_REPO_DATA:
            repository, _ = self._select_or_create(**self._DEFAULT_REPO_DATA)
            # Update Source.source_types:
            source_types = set()
            for dflt_repotype in self._DEFAULT_REPO_TYPES: 
                src_type, _ = self._select_or_create(**dflt_repotype)
                source_types.add(src_type)
            if source_types.difference(repository.source_types):
                repository.source_types.update(source_types)
        return repository
    
    # ``repository`` read/write property:
    @property
    def repository(self):
        """
        Memoized property with fall-back initialization using
        :method:`_get_default_repository`.
        """
        if self._repository is None:
            self._repository = self._get_default_repository()
        return self._repository
    @repository.setter
    def repository(self, repository):
        """
        :param object repository: optional, :class:`Source` object representing
        the repository used to fetch the data from; or None.
        """
        if repository is None:
            self._repository = None
        elif isinstance(repository, dbm.Source):
            self._repository = repository
        else:
            raise TypeError('You must supply a :class:`Source` object, or `None`.')


class RepoFileFetcherMixin(object):
    """
    Mixin extension class to add a ZIP Repository File ( :prop:`repo_file` ) to
    subclasses of :class:`Fetcher`.
    """
    def __init__(self, repo_file, *args, **kwargs):
        """
        :param object repo_file: zip file containing the repository data files. 
        It should be either a string containing the path to the file, or a 
        file-like object; or None.
        """
        super(RepoFileFetcherMixin, self).__init__(*args, **kwargs)
        #
        self._repo_file = None #Will contain a :class:`ZipFile` instance, or None.
        #
        self.repo_file = repo_file
    
    def __del__(self):
        """
        Close the repository zip file before instances are removed from memory.
        """
        if self._repo_file:
            self._repo_file.close()
    
    def _get_repo_file_version(self):
        """
        Get the repository version from the repository zip file.
        
        This is a basic implementation that simply infers repository version 
        from the repository file's filesystem creation date-time.
        Overwrite it in sub-classes to return another (better) kind of inferred 
        version.
        """
        repo_filen = self.repo_filename
        if repo_filen:
            repo_file_ctime = datetime.datetime.fromtimestamp( os.path.getctime(repo_filen) )
            return repo_file_ctime.strftime('%Y-%m-%d') #Version as a string of type '2015-02-25'.
        else:
            return None
    
    def _get_default_repository(self):
        """
        Method to get/create a default repository, when none is provided, but a 
        repository file is available.
        This method overwrites :class:`Fetcher` method to add functionality.
        
        :return object : a default :class:`Source` instance from data contained
        in :attr:`_DEFAULT_REPO_DATA` dictionary and repository file version as 
        returned by :method:`_get_repo_file_version`; or None.
        """
        repository = None
        repo_data = self._DEFAULT_REPO_DATA
        if self._repo_file is not None and repo_data:
            # Use the repository file version, if available. Otherwise the
            # default version as in :attr:`_DEFAULT_REPO_DATA` would be used:
            repo_file_version = self._get_repo_file_version()
            if repo_file_version:
                repo_data = repo_data.copy()
                repo_data['version'] = repo_file_version
            repository, _ = self._select_or_create(**repo_data)
            # Update Source.source_types:
            source_types = set()
            for dflt_repotype in self._DEFAULT_REPO_TYPES: 
                src_type, _ = self._select_or_create(**dflt_repotype)
                source_types.add(src_type)
            if source_types.difference(repository.source_types):
                repository.source_types.update(source_types)
        return repository
    
    # ``repo_filename`` read-only property:
    @property
    def repo_filename(self):
        """
        :return object : the filename of the zipped repository file (a string 
        containing the path to the file), or None.
        """
        if self._repo_file:
            return self._repo_file.filename
        else:
            return None
    
    # ``repo_file`` read/write property:
    @property
    def repo_file(self):
        """
        :return object : a :class:`ZipFile` instance; or None.
        """
        return self._repo_file
    @repo_file.setter
    def repo_file(self, repo_file):
        """
        Create a :class:`ZipFile` object and assign it to :attr:``_repo_file``.
        Also, if no repository version has been supplied, try to infer it from 
        the repository zip file.
        
        :param object repo_file: zip file containing the repository data files. 
        It should be either a string containing the path to the file, or a 
        file-like object; or None.
        """
        # None supplied as :param:`repo_file`:
        if repo_file is None:
            self._repo_file = None
            return
        # Otherwise, open zip file:
        zrepo_file = None
        try:
            zrepo_file = ZipFile(repo_file, 'r')
        except IOError as exception:
            zip_error_msg = exception.strerror + ' : ' + exception.filename
            self.logger.put_with_key('IO Error', zip_error_msg)
        except BadZipfile as exception:
            zip_error_msg = exception.args[0]
            self.logger.put_with_key('Zip Error', zip_error_msg)
        finally:
            self._repo_file = zrepo_file


class OBOGOTermFetcher(RepoFileFetcherMixin, Fetcher):
    """
    Class to get Gene Ontology Terms from a zipped OBO file ( see GO OBO files 
    at http://geneontology.org/page/download-ontology ).
    For now it's unable to use a DataBase to fetch data.
    """
    _DEFAULT_REPO_DATA = OBOGOTERMS
    _DEFAULT_REPO_TYPES = (ST_GO,)
    # Ontology Namespace in OBO file -> Ontology Key:
    __ONTOLOGIES__ = {'biological_process': 'P',
                      'molecular_function': 'F',
                      'cellular_component': 'C',
                      None: None}

    _repofile_goid2goterm = None # Cache dictionary for file data: {'GO:0000001': GOTerm(), ...}

    def __init__(self, repo_file, repository=None):
        """
        :param object repo_file: zip file containing an UniProt gaf-version 2.0
        gene_association.goa_<species> file. It should be either a string
        containing the path to the file, or a file-like object.
        :param object repository: optional, :class:`Source` object representing 
        the repository used to fetch the data from. Defaults to None.
        """
        super(OBOGOTermFetcher, self).__init__(repo_file, 
                                               db_session=None, #Make it unable to use a DataBase to fetch data.
                                               repository=repository, 
                                               caching=False) #It uses :attr:`_repofile_goid2goterm` as cache dictionary.

    def _get_repo_file_version(self):
        version = None
        # Try to get version from repository file:
        with self.repo_file.open(self.repo_file.namelist()[0], 'rU') as io_file:
            for line in io_file:
                # Version inside repository file:
                if line.startswith('data-version:'):
                    version = line.lstrip('data-version:').strip()
                    break
                # Otherwise (malformed OBO file) get filesystem creation date-time from repository file:
                elif line.startswith('['):
                    version = super(OBOGOTermFetcher, self)._get_repo_file_version()
                    break
        return version

    def _iter_repofile_goterms(self):
        with self.repo_file.open(self.repo_file.namelist()[0], 'rU') as io_file:
            in_term_def = False # In the middle of a Term Definition?
            obokey2values = None # defaultdict(list): {obokey: [values,...], ...}.
            for line in io_file:
                # In the middle of a Term Definition:
                if in_term_def:
                    if line == '\n' or line.startswith('['): # End of a Term (and possible beginning of a new one):
                        goids = obokey2values.pop( 'id', tuple() ) #Extract all the GO IDs.
                        name = obokey2values.pop( 'name', [None] )[0] #Extract the GO Name.
                        obonamespace = obokey2values.pop( 'namespace', [None] )[0]
                        ontology = self.__ONTOLOGIES__[obonamespace] #Get the Ontology Key from the OBO 'namespace' field/key.
                        # For every GO ID, yield a new :class:`GOTerm` object:
                        for goid in goids:
                            goterm = dbm.GOTerm(id=goid, name=name,
                                                ontology=ontology,
                                                extradata=obokey2values, #extradata: remaining information in ``obokey2values``
                                                source=self.repository)
                            yield goterm
                        in_term_def = False
                    else: # The middle of a Term:
                        obokey, _, value = line.partition(':')
                        if obokey == 'alt_id': obokey = 'id' #Special case: an "alternative id" is treated like a "normal id".
                        obokey2values[obokey].append( value.strip() )
                # New beginning of a Term Definition:
                if line.startswith('[Term]'):
                    in_term_def = True
                    obokey2values = defaultdict(list)
    
    @property
    def repofile_goid2goterm(self):
        """
        Property with Lazy initialization from a zipped OBO file.
        """
        if self._repofile_goid2goterm is None:
            # Lazy loading of GO Terms from a zipped OBO file:
            print('\nLoading GO Terms from file...')
            self._repofile_goid2goterm = { goterm.id: goterm for goterm in 
                                           self._iter_repofile_goterms() }
            print('Finished loading GO Terms from file.')
        return self._repofile_goid2goterm
    
    def __getitem__(self, goid):
        return self.repofile_goid2goterm[goid]
    
    def get(self, goid, default=None):
        return self.repofile_goid2goterm.get(goid, default)
    
    def iter_repofile_goterms(self):
        return self.repofile_goid2goterm.itervalues()


class OBOECO2GOEvidence(object):
    """
    Read-only minimal dictionary-like class to convert a Evidence and 
    Conclusions Ontology Code from an OBO file (see GO OBO files at
    http://geneontology.org/page/download-ontology) to a GO Evidence code/type.
    Ex.: 'ECO:0000314' -> 'IDA'
    """
    __VALID_ETYPES__ = dbm.Evidence.__TYPES__.keys() #Valid GO Evidence codes/types from :class:`dbm.Evidence` accepted types.

    def __init__(self, oboeco_file):
        """
        :param object oboeco_file: zip file containing an Evidence and 
        Conclusions Ontology file in OBO format (eco.obo). It should be either 
        a string containing the path to the OBO file, or a file-like object.
        """
        self._oboeco_file = None #OBO Evidence and Conclusions Ontology filename.
        self._eco2etype = None #Dictionary containing OBO Evidence and Conclusions Ontology codes to GO Evidence codes. None: "not initialized" from file (lazy loading).

        self.oboeco_file = oboeco_file

    @property
    def oboeco_file(self):
        return self._oboeco_file
    @oboeco_file.setter
    def oboeco_file(self, oboeco_file):
        if oboeco_file != self._oboeco_file:
            self._oboeco_file = oboeco_file
            self._eco2etype = None #Turn dictinoary to "not initialized" from file (lazy loading).

    def process_ecoterms(self): #REFACTORIZE
        valid_etypes_re = "|".join(self.__VALID_ETYPES__)
        eco2etype = dict()
        undef_ecoids2isaids = dict()
        # Process zipped ECO OBO file:
        zobo_file = ZipFile(self.oboeco_file, 'r')
        with zobo_file.open(zobo_file.namelist()[0], 'rU') as io_file:
            in_term_def = False #In the middle of a Term Definition.
            eco_ids = [] #Evidence and Conclusions Ontology ID (ECO ID) numbers (a list of int).
            etype = None #The GO Evidence code/type corresponding to that ECO ID (a str).
            is_a_ids = set() #Related ECO ID numbers (a set of int).
            for line in io_file:
                # In the middle of a Term Definition:
                if in_term_def:
                    if line == '\n' or line.startswith('['): # End of a Term (and possible beginning of a new one):
                        undef_ecoids2isaids[tuple( eco_ids )] = is_a_ids
                        in_term_def = False
                    else: # The middle of a Term:
                        key, _, value = line.partition(':') #Split line in key: value.
                        if key in ('alt_id', 'id'): # ECO ID numbers:
                            eco_ids.append(int( value.partition(':')[-1].strip() ))
                        elif key in ('def', 'synonym'): # May contain the GO Evidence code/type corresponding to an ECO ID:
                            possible_etypes = re.findall(valid_etypes_re, value)
                            if len( set(possible_etypes) ) == 1:
                                etype = possible_etypes[0]
                                for eco_id in eco_ids: #Add association ECO ID -> GO Evidence code/type:
                                    eco2etype[eco_id] = etype
                                in_term_def = False
                        elif key == 'is_a': # Related ECO ID numbers:
                            is_a_ids.update(map( int, re.findall('ECO:(\d+)', value) ))
                # New beginning of a Term Definition:
                if line.startswith('[Term]'):
                    in_term_def = True
                    eco_ids = []
                    etype = None
                    is_a_ids = set()
        zobo_file.close()
        # Re-process unidentified ECO ID numbers:
        end_undef_len = len(undef_ecoids2isaids)
        start_undef_len = end_undef_len + 1
        while end_undef_len < start_undef_len:
            start_undef_len = end_undef_len
            for eco_ids, is_a_ids in undef_ecoids2isaids.items():
                # Explore related ECO IDs for an Evidence Type:
                for is_a_id in is_a_ids:
                    etype = eco2etype.get(is_a_id, None)
                    if etype: # Evidence Type found from a related ECO ID:
                        for eco_id in eco_ids: #Add association ECO ID -> Evidence Type:
                            eco2etype[eco_id] = etype
                        undef_ecoids2isaids.pop(eco_ids) # :caution: in situ remove of dictionary key->value
                        break
            end_undef_len = len(undef_ecoids2isaids)
        #
        print( "  GO Evidence codes found for %s ECO IDs." % len(eco2etype) ) #DEBUG
        print("  NO GO Evidence codes found for other %s ECO IDs." % end_undef_len) #DEBUG
        #
        return eco2etype

    def __getitem__(self, eco_key):
        """
        :param object eco_key: an integer, or a string (such as 'ECO:0000123'
        or '0000123'), representing an Evidence and Conclusions Ontology (ECO) 
        code.

        :return str : a GO Evidence code/type (such as 'IDA' or 'IC').
        """
        # Lazy initialization at first access:
        if self._eco2etype is None:
            print('\nLoading Evidence and Conclusions Ontology Codes from file %s...' % self.oboeco_file) #DEBUG
            self._eco2etype = self.process_ecoterms()
            print('Finished loading Evidence and Conclusions Ontology Codes from file %s...' % self.oboeco_file) #DEBUG
        # `eco_key` (str or int) -> `eco_id` (int) :
        eco_id = None
        if isinstance(eco_key, str):
            if eco_key.isdigit(): # '0000123':
                eco_id = int(eco_key)
            elif ':' in eco_key: # 'ECO:0000123':
                eco_id = int( eco_key.split(':')[-1] )
        elif isinstance(eco_key, int): # 123:
            eco_id = eco_key
        if eco_id is None:
            raise ValueError("You must provide an integer, or a string (such"\
                             " as 'ECO:0000123' or '0000123').")
        #
        return self._eco2etype[eco_id]

    def get(self, eco_key, default=None):
        """
        :param object eco_key: an integer, or a string (such as 'ECO:0000123'
        or '0000123'), representing a Evidence and Conclusions Ontology code.
        :param object default: default value to return if `eco_key` is not 
        found. Defaults to None.
        
        :return str : a GO Evidence code/type string (such as 'IDA' or 'IC') 
        corresponding to the supplied `eco_key`. If none is found, returns 
        `default`.
        """
        try:
            return self.__getitem__(eco_key)
        except ValueError as _:
            return default


class CommonGOFetcher(Fetcher):                                      #REFACTOR?
    """
    Specialized :class:`Fetcher` subclass to act as an ancestor class for other 
    subclasses that need to get Gene Ontology Terms and/or process Gene Ontology 
    Annotations.
    """
    def __init__(self, db_session=None, repository=None, caching=False,
                 cache_limit=None, obofilefetcher=None):
        """
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to fetch stored data. Defaults to None (no database Session).
        :param object repository: optional, :class:`Source` object representing 
        the repository used to fetch the data from. Defaults to None.
        :param bool caching: indicates if the converter should use a caching
        strategy form some created objects. Defaults to False.
        :param int cache_limit: maximum number of objects allowed in the cache,
        or None (no limit). Defaults to None.
        :param OBOGOTermFetcher obofilefetcher: an :class:`OBOGOTermFetcher` 
        instance used for fetching GOTerms. If None is supplied, a new one will 
        be created using global `OBO_GO_FILE` as the source OBO file.
        """
        if obofilefetcher is None:
            obofilefetcher = OBOGOTermFetcher(OBO_GO_FILE)
        self._obofilefetcher = obofilefetcher
        super(CommonGOFetcher, self).__init__(db_session=db_session,
                                              repository=repository,
                                              caching=caching,
                                              cache_limit=cache_limit)

    def _get_or_obogoterm(self, go_id):
        """
        Get a `GOTerm` instance from `self.db_session` using its primary key
        (`go_id`), if it already exists in the Database; otherwise get the 
        instance from an :class:`OBOGOTermFetcher` instance, and adds it to 
        `self.db_session`.

        :param str go_id: a Go Term ID.

        :return tuple : a :class:`GOTerm` instance, and a boolean indicating 
        if it has been obtained from a OBO file (True) or from the Database 
        Session (False).
        """
        # This method is Dynamically set to :method:`__get_or_obogoterm` or
        # :method:`__obogoterm`, at :method:`db_session` setter
        raise NotImplementedError('This method is Dynamically set at `self.db_session` setter')

    def __get_or_obogoterm(self, go_id):
        """
        Get a `GOTerm` instance from `self.db_session` using its primary key
        (`go_id`), if it already exists; otherwise get the instance from an
        :class:`OBOGOTermFetcher` instance.

        :param str go_id: a Go Term ID.

        :return tuple : a :class:`GOTerm` instance, and a boolean indicating if
        this is from a OBO file (True) or from the database session (False).
        """
        instance = self.db_session.query(dbm.GOTerm).get(go_id)
        if instance:
            return instance, False
        else:
            instance = self._obofilefetcher.get(go_id)
#             self.db_session.add(instance) #X-NOTE: it seems that adding GOTerm to session before its children causes problems...
            return instance, True

    def __obogoterm(self, go_id):
        """
        Get a `GOTerm` instance from an :class:`OBOGOTermFetcher` instance.

        :param str go_id: a Go Term ID.

        :return tuple : a :class:`GOTerm` instance, and True (indicating this 
        is from a OBO file).
        """
        return self._obofilefetcher.get(go_id), True

    # ``db_session`` property setter override:
    @Fetcher.db_session.setter
    def db_session(self, session):
        Fetcher.db_session.fset(self, session) #Call :method:`fset` of the superclass :property:`db_session`.
        # Share the session with the :class:`OBOGOTermFetcher` instance:
        self._obofilefetcher.db_session = session
        # Set `self._get_or_obogoterm` method to `self.__get_or_obogoterm` or
        # `self.__obogoterm`:
        if session is None:
            self._get_or_obogoterm = self.__obogoterm
        else:
            self._get_or_obogoterm = self.__get_or_obogoterm

    @staticmethod
    def _unpack_qgostrdict(strdict):
        """
        'InterPro:IPR015421|InterPro:IPR015422' -> {'InterPro': ['IPR015421', 'IPR015422'] }
        """
        db2ids = defaultdict(list)
        qgomapping = [ record.split(':') for record in strdict.split('|') ]
        for record in qgomapping:
            db_key = record[0]
            for db_id in record[1:]: #X-NOTE: for one key many IDs cases, like 'UniProtKP:Q00001:PRO_12300001'.
                db2ids[db_key].append(db_id)
        return db2ids

    @staticmethod
    def summarize_goannotations_withfrom(goannotations):
        """
        GOAnnotation1.with_from = { repo11: id11, repo12: id12 }
        GOAnnotation2.with_from = { repo2: id2}
            |
            V
        Summarized_GOAnnotation.with_from = { repo11: id11, repo12: id12, repo2: id2}
        
        :caution: Do this only with fresh GOAnnotaions, not with Database ones.
        """
        # Group Go Annotations by GID (Group ID: attribute values) in a
        # dictionary of sets:
        groups = defaultdict(set)
        for goann in goannotations:
            gid = (goann.go.id,
                   goann.evidence.type,
                   frozenset( (key, frozenset(values)) for key, values in
                              goann.evidence.linked_ids.items() ),
                   goann.original_source
                   )
            groups[gid].add(goann)
        # Summarize each group, mixin the with_from attribute of all its
        # members, in only one GO Annotation:
        grouped_goanns = set()
        for group in groups.values():
            db2ids = defaultdict(set)
            for goann in group:
                for db_key, db_id in goann.with_from.items():
                    db2ids[db_key].update(db_id)
            ref_goann = group.pop() #X-NOTE: This can be dangerous if done with GOAnnotations from a database still assiciated with a session that can be commited.
            ref_goann.with_from = db2ids
            grouped_goanns.add(ref_goann)
        return grouped_goanns


class UniProtRepositoryFetcher(RepoFileFetcherMixin, CommonGOFetcher):
    """
    Class to get Protein information from a zipped file containing UniProt XML,
    FASTA and neXtprot chromosome information files.
    """
    _DEFAULT_REPO_DATA = UNIPROTKB
    _DEFAULT_REPO_TYPES = (ST_PROTEINS, ST_GO)
    
    __PREFIX2XMLNAMESPC__ = {'up': 'http://uniprot.org/uniprot'} # Prefix -> XML Namespace for tags in UniProt XML files
    _CHRM_AC_FILE_TMPLT = "nextprot_ac_list_chromosome_{0}.txt" # File template for neXtprot files inside UniProt ZIP `repo_file` containing ACs in each chromosome.
    CHRMS = tuple( str(num) for num in range(1,23) ) + ('X', 'Y', 'MT') # Chromosomes (MT: mitochondrial)

    def __init__(self, repo_file=None, db_session=None, repository=None,
                 caching=True, cache_limit=10000, obofilefetcher=None,
                 oboeco2goevidence=None, only_reviewed=False):
        """
        :param object repo_file: optional zip file containing UniProt XML, 
        FASTA and neXtprot chromosome information. It should be either a string
        containing the path to the file, or a file-like object.
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to fetch stored data. Defaults to None (no database Session).
        :param object repository: optional, :class:`Source` object representing 
        the repository used to fetch the data from. Defaults to None.
        :param bool caching: indicates if the converter should use a caching
        strategy form some created objects. Defaults to True (a lot of cache 
        hits will be produced by Evidences).
        :param int cache_limit: maximum number of objects allowed in the cache,
        or None (no limit). Defaults to 10000 (not 'recalled' proteins can
        consume too much memory).
        :param OBOGOTermFetcher obofilefetcher: an :class:`OBOGOTermFetcher` 
        instance to use for fetching GOTerms. If None is supplied, a new one 
        will be created using global `OBO_GO_FILE` as the source OBO file.
        :param dict oboeco2goevidence: dict-like object to convert OBO Evidence 
        and Conclusions Ontology codes to GO Evidence codes/types. If None is 
        supplied, a new one will be created from :class:`OBOECO2GOEvidence` and 
        global `OBO_ECO_FILE` as its source OBO file.
        :param bool only_reviewed: sets whether Only reviewed Proteins must be
        imported (True) or All proteins must (False). Defaults to False (import
        all proteins, reviewed or not).
        """
        self._last_ac = None
        self.protac2chrm = dict()
        if oboeco2goevidence is None:
            self.oboeco2goevidence = OBOECO2GOEvidence(OBO_ECO_FILE)
        self.only_reviewed = only_reviewed
        #
        super(UniProtRepositoryFetcher, self).__init__(repo_file,
                                                       db_session=db_session,
                                                       repository=repository,
                                                       caching=caching,
                                                       cache_limit=cache_limit,
                                                       obofilefetcher=obofilefetcher)

    def _get_repo_file_version(self):
        repo_filen = self._repo_file.filename
        if repo_filen:
            repo_file_ctime = datetime.datetime.fromtimestamp( os.path.getctime(repo_filen) ) #Get creation date-time from repository file
            return repo_file_ctime.strftime("%Y.%m") #Version as a string of type 'year.month'.
        else:
            return None

    # ``repo_file`` property setter override:
    @RepoFileFetcherMixin.repo_file.setter
    def repo_file(self, repo_file):
        """
        :param object repo_file: zip file containing UniProt XML and FASTA files, 
        and neXtProt chromosome information. It should be either a string
        containing the path to the file, or a file-like object.
        """
        RepoFileFetcherMixin.repo_file.fset(self, repo_file) # Call :method:`fset` of the superclass mixin :property:`repo_file`.
        # Dynamically Set :method:`_open_from_repository`, and the Chromosome 
        # information:
        self.protac2chrm.clear()
        if self.repo_file:
            # - Set :method:`_open_from_repository` to :method:`__open_from_repo_file`:
            self._open_from_repository = self.__open_from_repo_file
            # - Set also Chromosome information for each protein ACcession:
            for chrm in self.CHRMS:
                self.protac2chrm.update( 
                            dict.fromkeys(self.iter_prots_in_chrm(chrm), chrm)
                                        )
        else:
            # - Set :method:`_open_from_repository` to :method:`__open_from_internet`:
            self._open_from_repository = self.__open_from_internet
    
    # ``db_session`` property setter override:
    @CommonGOFetcher.db_session.setter
    def db_session(self, session):
        CommonGOFetcher.db_session.fset(self, session) # Call :method:`fset` of the superclass :property:`db_session`.
        # Set default PTMs instances as attributes:
        for attr, ptm_modeldata, ptm_modelextradata in (
                ('phosphorylation', PHOSPHORYLATION, PHOSPHORYLATION_EXTRADATA),
                ('acetylation', ACETYLATION, ACETYLATION_EXTRADATA), 
                ('ubiquitination', UBIQUITINATION, UBIQUITINATION_EXTRADATA), 
#                 ('oxidation', OXIDATION, OXIDATION_EXTRADATA)
                                                         ):
            ptm_modelobj, isnew = self._select_or_create(**ptm_modeldata)
            if isnew:
                ptm_modelobj.extradata = ptm_modelextradata
            setattr(self, attr, ptm_modelobj)
    
    def _open_from_repository(self, filen):
        """
        Open a file name (`filen`) from :attr:``repo_file`` or from Internet.
        Delegates to :method:`__open_from_repo_file` or 
        :method:`__open_from_internet`.

        :param str filen: a file name.

        :return file : an input/output :class:`file` instance.
        """
        # This method is Dynamically Set, at `self.repo_file` setter, to one of:
        #  :method:`__open_from_repo_file`, or
        #  :method:`__open_from_internet`
        pass
    
    def __open_from_repo_file(self, filen):
        """
        Open a file name (`filen`) from :attr:``repo_file``.

        :param str filen: file name inside :attr:``repo_file`` ZipFile to be 
        opened.

        :return file : an input/output :class:`file` instance (``io_file``). If
        there is some error, logs it and returns None.
        """
        io_file = None
        try:
            io_file = self.repo_file.open(filen, 'rU')
        except (IOError, KeyError, BadZipfile) as exception:
            file_error_msg = ' : '.join(( filen, 
                                          getattr(exception, 'strerror', ''),
                                          getattr(exception, 'message', ''),
                                          getattr(exception, 'filename', '')
                                          ))
            self.logger.put_with_key('Open from repo Zip-File', file_error_msg)
            if io_file:
                io_file.close()
                io_file = None
        #
        return io_file

    def __open_from_internet(self, filen):                         #IMPLEMENT!
        """
        Open a file name (`filen`) from Internet UniPort repository 
        (http://www.uniprot.org).
        
        :param str filen: a file name.
        
        :return file : an input/output :class:`file` instance.
        """
        raise NotImplementedError()

    def iter_prots_in_chrm(self, chrm):
        """
        Yields the ACcession numbers of Proteins encoded in a chromosome.

        :param str chrm: chromosome name (see :attr:`CHRMS`).
        """
        # Open the Chromosome file corresponding to `chrm`:
        chr_io = self._open_from_repository( self._CHRM_AC_FILE_TMPLT.format(chrm) )
        # Get the protein accession codes:
        if chr_io:
            for row in chr_io:
                yield row.strip().split('_')[-1]
            chr_io.close()

    def _get_prot_xml(self, ac=None, ac_noisoform=None):
        """
        :param str ac: a protein ACcession number. Ex. 'P12345-1'
        :param str ac_noisoform: a protein ACcession number without isoform 
        information. Ex. 'P12345' (no "-1")
        
        :caution: only one parameter :param:`ac` or :param:`ac_noisoform` should 
        be provided. If both are provided, :param:`ac` takes preference; 
        altought :param:`ac_noisoform` would be preferred (no need to derive it 
        from :param:`ac`).
        """
        if ac is not None:
            self._last_ac = ac
            ac_noisoform, _, _ = ac.partition('-') # 'P00001-2' -> 'P00001'
        # Get UniProt XML file object (xml_io):
        xml_io = self._open_from_repository(ac_noisoform + ".xml")
        if not xml_io: # No xml file for this AC in UniProt:
            return None
        # Parse XML data structure fetched from the UniProt XML file:
        prot_xml = ElementTree()
        prot_xml.parse(xml_io)
        xml_io.close()
        #
        return prot_xml

    def _get_dataset4protxml(self, prot_xml):
        """
        Get the Protein dataset origin (TrEMBL or Swiss-Prot) from the protein
        XML data (`prot_xml`).

        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return str : return the Protein dataset origin ('TrEMBL' or 
        'Swiss-Prot').
        """
        return prot_xml.find('up:entry', self.__PREFIX2XMLNAMESPC__).get('dataset', None)
    
    def get_dataset(self, ac=None, ac_noisoform=None):
        # Get xml for protein with `ac` ACcession number:
        prot_xml = self._get_prot_xml(ac, ac_noisoform)
        # Return the protein dataset origin ('TrEMBL' or 'Swiss-Prot'):
        return self._get_dataset4protxml(prot_xml)
    
    def _get_uniprotid4protxml(self, prot_xml):
        """
        Get the Protein UniProt ID/internal name (AC_Organism) from the protein
        XML data (`prot_xml`).

        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return str : return the Protein UniProt ID/internal name (EX.:
        'A0A576_HUMAN') from the UniProtKB protein XML record.
        """
        return prot_xml.find('up:entry/up:name', self.__PREFIX2XMLNAMESPC__).text
    
    def get_uniprotid(self, ac=None, ac_noisoform=None):
        # Get xml for protein with `ac` ACcession number:
        prot_xml = self._get_prot_xml(ac, ac_noisoform)
        # Return the protein name:
        return self._get_uniprotid4protxml(prot_xml)
    
    def _get_name4protxml(self, prot_xml):
        """
        Get the Protein full name from the protein XML data (`prot_xml`).

        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return str : return the Protein full name from the UniProtKB protein
        XML record.
        """
        xml_elem = prot_xml.findall('up:entry/up:protein/*/up:fullName',
                                    self.__PREFIX2XMLNAMESPC__)
        return '. '.join( fullname.text for fullname in xml_elem )

    def get_name(self, ac=None, ac_noisoform=None):
        # Get xml for protein with `ac` ACcession number:
        prot_xml = self._get_prot_xml(ac, ac_noisoform)
        # Return the protein name:
        return self._get_name4protxml(prot_xml)

    def _get_isoform_refs4acprotxml(self, ac, prot_xml):
        """
        Get the isoform name and the reference identifiers for the <feature>
        XML elements that describe the changes present in an isoform sequence
        relatives to the canonical sequence.

        :param str ac: a protein full ACcession number (Ex.: 'P00001-1').
        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return tuple : the isoform current name/ID, or 'cannonical' if the
        isoform is the canonical form; and a list of reference identifiers for
        <feature> XML elements (empty if it's the cannonical form).
        """
        xml_isoforms = prot_xml.findall('up:entry/up:comment[@type="alternative products"]/up:isoform',
                                        self.__PREFIX2XMLNAMESPC__)
        for xml_isoform in xml_isoforms:
            isoid = xml_isoform.find('up:id', self.__PREFIX2XMLNAMESPC__).text # 'P00001-1'
            isoname = xml_isoform.find('up:name', self.__PREFIX2XMLNAMESPC__).text # '1'
            isodata = xml_isoform.find('up:sequence', self.__PREFIX2XMLNAMESPC__)
            if isoid == ac and isodata.get('type') != 'displayed': # No-cannonical (has referenced sequence changes):
                return isoname, isodata.get('ref').split()
        return 'cannonical', list() # Cannonical (no referenced sequence changes).
    
    def _iter_isoform_chgs4refsprotxml(self, refs, prot_xml):
        """
        Iterates over <feature> XML elements that describe the changes present
        in an isoform, to yield the start and end positions in the canonical
        sequence for each change, as well as the original cannonical sequence
        and the variant isoform sequence for this region of change.

        :param iterable refs: reference identifiers for <feature> XML elements.
        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return generator : yields the start and end positions in the canonical
        sequence for each change, as well as the original cannonical sequence
        and the variant isoform sequence for this region of change.
        """
        for ref in refs:
            xe_isomod = prot_xml.find('up:entry/up:feature[@id="{0}"]'.format(ref),
                                      self.__PREFIX2XMLNAMESPC__)
            xe_original = xe_isomod.find('up:original', self.__PREFIX2XMLNAMESPC__)
            original = xe_original.text if xe_original is not None else None
            xe_variantion = xe_isomod.find('up:variation', self.__PREFIX2XMLNAMESPC__)
            variation = xe_variantion.text if xe_variantion is not None else None
            xe_position = xe_isomod.find('up:location/up:position', self.__PREFIX2XMLNAMESPC__)
            if xe_position is not None:
                start = end = int( xe_position.get('position') )
            else:
                start = int( xe_isomod.find('up:location/up:begin',
                                            self.__PREFIX2XMLNAMESPC__).get('position') )
                end = int( xe_isomod.find('up:location/up:end',
                                          self.__PREFIX2XMLNAMESPC__).get('position') )
            yield start, end, original, variation
    
    def _clean_isoform_ptms(self, ac, modifications, prot_xml):
        """
        Clean UniProt PTMs in protein isoforms, deleting those related only to
        the cannonical protein, and re-locating PTMs positions to the isoform
        rigth positions.

        :param str ac: a protein ACcession number for a protein isoform.
        :param iterable modifications: an iterable of UniPort 
        :class:`ProteinModification` instances.
        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return set : a set containing UniPort :class:`ProteinModification`
        instances filtered and with the position modified to match the current
        protein isoform.
        """
        if '-' not in ac: #Do not clean cannonical proteins:
            return modifications
        # Get the reference identifiers for this isoform changes:
        _, isorefs = self._get_isoform_refs4acprotxml(ac, prot_xml)
        if not isorefs:
            return modifications
        # {position in cannonical prot.: {PTMs} }:
        cannonical_pos2ptms = defaultdict(set)
        for ptm in modifications:
            cannonical_pos2ptms[ptm.position].add(ptm)
        # Modify PTMs:
        session = self.db_session
        for start, end, original, variation in self._iter_isoform_chgs4refsprotxml(isorefs, prot_xml):
            if original: #AA sequence Modifications:
                post_shift = len(variation) - len(original)
            else: #AA sequence Deletions:
                post_shift = -(end - start) - 1
            #
            for pos, ptms in cannonical_pos2ptms.items():
                if start <= pos <= end: #Deletes the PTMs inside the sequence modification or deletion:
#                     del cannonical_pos2ptms[pos] #This is not enough to avoid the deleted PTMs to enter the DB...
                    discarded_ptms = cannonical_pos2ptms.pop(pos)
                    for ptm in discarded_ptms:
                        # Clear the PTM's relationships:
                        ptm.mod_type = None
                        ptm.source = None
                        ptm.evidences = set()
                        # Take the PTM out of the session:
                        if session is not None and ptm in session: 
                            session.expunge(ptm)
                elif end < pos and post_shift: #Moves/shifts the PTMs after the sequence modification or deletion:
                    for ptm in ptms:
                        ptm.position += post_shift
        # Collect and return modified PTMs:
        cleaned_mods = set()
        for ptms in cannonical_pos2ptms.values():
            cleaned_mods.update(ptms)
        return cleaned_mods

    def _get_organism4protxml(self, prot_xml): #TO_DO: cache this for speed-up.
        """
        Get the organism from the protein xml data (`prot_xml`).

        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return Organism: return an :class:`Organism` instance with data
        fetched from the UniProtKB protein XML record.
        """
        xml_organism = prot_xml.find('up:entry/up:organism',
                                     self.__PREFIX2XMLNAMESPC__)
        organism_id = xml_organism.find("up:dbReference[@type='NCBI Taxonomy']",
                                        self.__PREFIX2XMLNAMESPC__).get('id')
        organism, isnew = self._select_or_create(dbm.Organism, id=organism_id)
        if isnew:
            organism.name = xml_organism.find("up:name[@type='scientific']",
                                              self.__PREFIX2XMLNAMESPC__).text
            organism.extradata['common_name'] = xml_organism.find("up:name[@type='common']",
                                                                  self.__PREFIX2XMLNAMESPC__).text
        return organism

    def get_organism(self, ac=None, ac_noisoform=None):
        # Get xml for protein with `ac` ACcession number:
        prot_xml = self._get_prot_xml(ac, ac_noisoform)
        # Return the protein organism:
        return self._get_organism4protxml(prot_xml)

    def _get_chr4protxml(self, prot_xml):
        chr_name = prot_xml.find("up:entry/up:dbReference[@type='Proteomes']/"\
                                 "up:property[@type='component']",
                                 self.__PREFIX2XMLNAMESPC__).get('value')
        return chr_name.partition(' ')[-1] # "Chromosome 7" -> "7"
    
    def get_chromosome(self, ac):
        """
        :param str ac: a protein ACcession number. Ex. 'P12345-1' or 'P12345'.
        (The no-isoform ACcession is preferred for speed).
        """
        if "-" in ac: # ACcession with isoform number:
            ac, _, _ = ac.partition('-') # 'P00001-2' -> 'P00001'
        return self.protac2chrm.get(ac, None)
    
    def _get_gene4protxml(self, prot_xml, ac):
        """
        Get the gene coding the protein from the protein xml data (`prot_xml`).

        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return Gene : return a :class:`Gene` instance with data fetched
        from the UniProtKB protein XML record.
        """
        # Get primary gene name (`gene_name`) and alternative names (`gene_metadata`):
        gene_name = 'Unknown'
        gene_metadata = defaultdict(list)
        for xml_name in prot_xml.findall('up:entry/up:gene/up:name',
                                         self.__PREFIX2XMLNAMESPC__):
            xml_name_type = xml_name.get('type', '')
            if xml_name_type == 'primary':
                gene_name = xml_name.text
            else:
                gene_metadata[xml_name_type].append(xml_name.text)
        # Get gene chromosome:
        chrm = self.get_chromosome(ac)
        # Get Gene instance from database or create a new one:
        gene, isnew = self._select_or_create(dbm.Gene, name=gene_name, chr=chrm)
        if isnew:
            #Add meta-data dictionary as gene :attr:`extradata`:
            gene.extradata = gene_metadata
        else:
            #Update gene :attr:`extradata` with meta-data dictionary:
            for key, new_values in gene_metadata.items():
                old_values = gene.extradata.get(key, list())
                if isinstance(old_values, list):
                    new_values.extend(old_values)
                else:
                    new_values.append(old_values)
                gene.extradata[key] = list( set(new_values) )
        return gene

    def get_gene(self, ac=None, ac_noisoform=None):
        # Get xml for protein with `ac` ACcession number:
        prot_xml = self._get_prot_xml(ac, ac_noisoform)
        # Return the protein gene:
        return self._get_gene4protxml(prot_xml, ac_noisoform or ac)

    def _get_functions4protxml(self, prot_xml): #TO-DO: replace with GO terms
        """
        Locates Protein function in a list of XML elements with comment
        information for a protein fetched from the UniProtKB and return this
        function.

        :param ElementTree prot_xml: an UniProtKB protein XML record.
        """
        # Set defaults:
        comments = prot_xml.findall('up:entry/up:comment',
                                    self.__PREFIX2XMLNAMESPC__)
        #
        function = None
        for xml_elem in comments:
            if xml_elem.get('type', '') == 'function':
                function = xml_elem.find('up:text', self.__PREFIX2XMLNAMESPC__).text
                break
        return function

    def _get_locations4protxml(self, prot_xml): #TO-DO: replace with GO terms?
        """
        Fetch Protein locations in a list of XML elements with comment
        information for a protein fetched from the UniProtKB and return those
        locations.

        :param ElementTree prot_xml: an UniProtKB protein XML record.
        """
        # Set defaults:
        locations = prot_xml.findall('.//up:subcellularLocation/up:location',
                                     self.__PREFIX2XMLNAMESPC__)
        #
        return sorted( {location.text for location in locations} )
    
    def _get_key2evidence4protxml(self, prot_xml):
        key2evidence = dict() # XML evidence Key -> :class:`Evidence` object
        evidences = prot_xml.iterfind('up:entry/up:evidence', 
                                      self.__PREFIX2XMLNAMESPC__)
        for xml_elem in evidences:
            # Get data from each UniProtKB XML evidence element:
            evidence_key = xml_elem.get('key')
            evidence_type = xml_elem.get('type')
            # Get or create the related :class:`Evidence` object with a GO
            # Evidence code/type:
            if evidence_type.startswith('ECO'): # New style of UniProtKB XML evidences (ECO codes):
                # - Try to convert from an Evidence and Conclusions Ontology 
                #   (ECO) code to a GO Evidence code/type:
                try:
                    evidence_type = self.oboeco2goevidence[evidence_type]
                except KeyError as _:
                    pass
            else: # Old style of UniProtKB XML evidences(GO Evidence codes/types):
                evidence_type = evidence_type.partition(':')[0]
            evidence, _ = self._select_or_create(dbm.Evidence,
                                                 type=evidence_type)
            # Add Evidence to the dictionary:
            key2evidence[evidence_key] = evidence
        #
        return key2evidence

    def _iter_ptms4protxml(self, prot_xml, reviewed=False):
        """
        Locates and yields the PTMs (post-translational modifications) found in
        an UniProtKB protein XML record.

        :param ElementTree prot_xml: an UniProtKB protein XML record.
        :param bool reviewed: indicates if the Protein has been reviewed (it's
        from Swiss-Prot) or not (it's from TrEMBL).

        :return generator : yields :class:`ProtModification` objects with PTM
        data fetched from the UniProtKB protein XML record.
        """
        #
        features = prot_xml.findall('.//up:feature', self.__PREFIX2XMLNAMESPC__)
        if features:
            key2evidence = self._get_key2evidence4protxml(prot_xml)
            protevidence_keys = key2evidence.keys()
        for xml_elem in features:
            mod_type = None
            feature_type = xml_elem.get('type', '')
            feature_desc = xml_elem.get('description', '')
            # - Phosphorilations:
            if ('Phosphoserine' in feature_desc or
                'Phosphotyrosine' in feature_desc or
                'Phosphothreonine' in feature_desc):
                mod_type = self.phosphorylation
            # - Acetylations:
            elif feature_type == 'modified residue' and 'acetyl' in feature_desc:
                mod_type = self.acetylation
            # - Ubiquitinations:
            elif feature_type == 'cross-link' and 'ubiquitin' in feature_desc:
                mod_type = self.ubiquitination
            #
            if mod_type is not None: # It's a PTM of interest:
                try: #Avoid UniProt XML errors in position annotation (no position):
                    pos = int( xml_elem.find('up:location/up:position',
                                             self.__PREFIX2XMLNAMESPC__)\
                                            .get('position') )
                except (AttributeError, ValueError) as _:
                    self.logger.put_with_key( 'PTM Error', 
                                              "%s : No %s Position"%(self._last_ac, mod_type.name) )
                    continue
                if pos: #Avoid UniProt XML errors in position annotation (position=0):
                    # Get  UniProt Evidences for Protein Modification:
                    pos_evidence_keys = xml_elem.get('evidence', '').split(' ')
                    evidences = {key2evidence[key] for key in pos_evidence_keys
                                 if key in protevidence_keys} #Avoid UniPort lack of evidences.
#                     X-NOTE: UniProt Modifications are not significant for 
#                     LymPHOS-UB-AC database, because we can not track if the 
#                     modifications appears or not in lymphoocytes.
#                     # Only Protein Modifications from a `reviewed` Protein and
#                     # with experimental/strong Evidences will be marked as
#                     # significant:
#                     signif = reviewed and any( (evidence.type in 
#                                                 evidence.SIGNIFICANT_TYPEKEYS) 
#                                                for evidence in evidences )
                    # Create and yield the Protein Modification:
                    yield dbm.ProtModification(mod_type=mod_type, position=pos,
                                               source=self.repository, 
                                               evidences=evidences,
#                                                significant= signif or None)
                                               significant=None) #X-NOTE: UniProt Modifications are not significant for LymPHOS-UB-AC database
                else:
                    self.logger.put_with_key( 'PTM Error', 
                                              "%s : %s Position=0"%(self._last_ac, mod_type.name) )

    def iter_ptms(self, ac=None, ac_noisoform=None):
        """
        Locates and yields the PTMs (post-translational modifications) found
        for a protein ACcession number in UniProt repository
        (:attr:`repo_file` or Internet).

        :param str ac: a protein ACcession number. Ex. 'P12345-1'
        :param str ac_noisoform: a protein ACcession number without isoform 
        information. Ex. 'P12345' (no "-1")
        
        :caution: only one parameter :param:`ac` or :param:`ac_noisoform` should 
        be provided. If both are provided, :method:`_get_prot_xml` gives 
        preference to :param:`ac`.
        
        :return generator : yields :class:`ProtModification` objects with PTM
        data fetched from UniProt repository (:attr:`repo_file` or Internet).
        """
        # Get xml for protein with `ac` ACcession number:
        prot_xml = self._get_prot_xml(ac, ac_noisoform)
        # Return the ptms generator:
        return self._iter_ptms4protxml(prot_xml)

    def get_ptms(self, ac=None, ac_noisoform=None):
        """
        Locates and returns the PTMs (post-translational modifications) found
        for a protein ACcession number in UniProt repository
        (:attr:`repo_file` or Internet).

        :param str ac: a protein ACcession number. Ex. 'P12345-1'
        :param str ac_noisoform: a protein ACcession number without isoform 
        information. Ex. 'P12345' (no "-1")
        
        :caution: only one parameter :param:`ac` or :param:`ac_noisoform` should 
        be provided. If both are provided, :method:`_get_prot_xml` gives 
        preference to :param:`ac`.
        
        :return set: a set of :class:`ProtModification` objects with PTM data
        fetched from UniProt repository (``self.repo_file`` or Internet).
        """
        return set( self.iter_ptms(ac, ac_noisoform) )

    def get_sequence(self, ac):
        """
        Get a protein sequence from a UniProtKB FASTA file to avoid processing
        xml for isoforms (slow).

        :param str ac: protein ACcession number to get te sequence from
        :attr:`repo_file` or Internet.

        :return str: protein aminoacid sequence.
        """
        # Open the FASTA file corresponding to the protein ACcession number:
        seq_io = self._open_from_repository(ac + '.fasta')
        # Get a cleaned sequence:
        if seq_io:
            seq_io.readline() #Discard FASTA first line (sequence information).
            sequence = u"".join( seq_io.readlines() ).translate( {32: None,   #Coerce to unicode and
                                                                  9:  None,   #clean sequence from spaces, ...
                                                                  10: None,
                                                                  45: None,
                                                                  13: None} )
            seq_io.close()
        else:
            sequence = u''
        #
        return sequence

    def _iter_goannotations4protxml(self, prot_xml):
        """
        Locates and yields the Gene Ontology Annotations found in an UniProtKB
        protein XML record, along with its evidences.

        :param ElementTree prot_xml: an UniProtKB protein XML record.

        :return generator : yields :class:`GOAnnotation` objects (along with
        its :class:`Evidence` associated objects).
        """
        # Set defaults:
        xml_goterms = prot_xml.iterfind(".//up:dbReference[@type='GO']",
                                        self.__PREFIX2XMLNAMESPC__)
        #
        for xml_goterm in xml_goterms:
            # Get the GOTerm from a database session or a OBO file:
            go, _ = self._get_or_obogoterm( xml_goterm.get("id") )
            # Get the evidence object and source information:
            xml_evidence = xml_goterm.find("up:property[@type='evidence']",
                                           self.__PREFIX2XMLNAMESPC__)
            evidence_value = xml_evidence.get("value")
            if evidence_value.startswith('ECO'): # New and more complex style of UniProtKB XML Evidences:
                # - Try to convert from Evidence and Conclusions Ontology code
                #   to GO Evidence code/type:
                try:
                    evidence_type = self.oboeco2goevidence[evidence_value]
                except KeyError as _:
                    evidence_type = evidence_value
                # - Get the source:
                xml_source = xml_goterm.find("up:property[@type='project']",
                                             self.__PREFIX2XMLNAMESPC__)
                original_source = xml_source.get("value")
            else: # Old style of UniProtKB XML Evidences:
                evidence_type, _, original_source = evidence_value.partition(':')
            # - Get or create the related :class:`Evidence` object:
            evidence, _ = self._select_or_create(dbm.Evidence,
                                                 type=evidence_type)
            # Create and yield the new :class:`GOAnnotation` object:
            yield dbm.GOAnnotation(go=go, original_source=original_source, 
                                   evidence=evidence, source=self.repository)

    def iter_goannotations(self, ac=None, ac_noisoform=None):
        """
        Locates and yields the Gene Ontology Annotations found for an UniProtKB
        ACcession number.

        :param str ac: an UniProtKB ACcession number. Ex. 'P12345-1'
        :param str ac_noisoform: a protein ACcession number without isoform 
        information. Ex. 'P12345' (no "-1")
        
        :caution: only one parameter :param:`ac` or :param:`ac_noisoform` should 
        be provided. If both are provided, :method:`_get_prot_xml` gives 
        preference to :param:`ac`.
        
        :return generator : yields :class:`GOAnnotation` objects (along with
        its :class:`Evidence` associated objects).
        """
        # Get xml for protein with `ac` ACcession number:
        prot_xml = self._get_prot_xml(ac, ac_noisoform)
        # Return the ptms generator:
        return self._iter_goannotations4protxml(prot_xml)

    def get_goannotations(self, ac=None, ac_noisoform=None):
        return set( self.iter_goannotations(ac, ac_noisoform) )

    def fetch_protein(self, protein):
        """
        Fetch protein information data from UniProt repository
        (:attr:`repo_file` or the Internet) and then store it into a
        :class:`Protein` object (optionally passed as the `protein` parameter
        instead of an accession number).

        :param object protein: a :class:`Protein` instance or an ACcession
        number string.

        :return Protein: a :class:`Protein` object with data fetched from
        UniProt repository (``self.repo_file`` or Internet).
        """
        db_session = self.db_session
        # Get a :class:`Protein` instance, if not supplied:
        if isinstance(protein, str):
            # `protein` parameter is an ACcession number so we need to create a
            # :class:`Protein` object:
            protein, _ = self._select_or_create( dbm.Protein, ac=protein,                 # Create a :class:`Protein` instance from an ACcession number
                                                 ac_noisoform=protein.partition('-')[0] ) #
        self._last_ac = protein.ac # For log purposes.
        #
        # Only process UniProt XML data for real proteins, not for decoys:
        if 'decoy' in protein.ac:
            # Decoys can have sequence inside the repository file:
            protein.seq = self.get_sequence(protein.ac)
            if protein.seq:
                protein.is_decoy = True
                return protein #No more information will be available from UniProt for a decoy protein.
            else:
                if db_session:
                    db_session.expunge(protein)
                return None # No protein information found in repository file!
        protein.is_decoy = False
        #
        # Get UniProt XML Tree object and stop if there is no UniProtKB XML
        # data to fetch more protein information:
        prot_xml = self._get_prot_xml(protein.ac_noisoform) #X_NOTE: :method:`_get_prot_xml` 'prefers' the ACcession name without isoform information
        if not prot_xml:
            if db_session:
                db_session.expunge(protein)
            return None # No protein information found in repository file!
        #
        # Assign UniProtKB data to attributes/fields of protein object, if such
        # data is available:
        # - TrEMBL (reviewed=False) or Swiss-Prot (reviewed=True) dataset origin:
        protein.reviewed = self._get_dataset4protxml(prot_xml) == 'Swiss-Prot'
        if self.only_reviewed and not protein.reviewed:
            if db_session:
                db_session.expunge(protein)
            return None
        # - current UniProtKB as source_repository:
        protein.source_repository = self.repository
        # - id_in_source_repository:
        uniprot_id = self._get_uniprotid4protxml(prot_xml)
        protein.id_in_source_repository = uniprot_id
        protein.linked_repositories[uniprot_id] = self.repository
        # - name:
        protein.name = self._get_name4protxml(prot_xml)
        # - sequence and length:
        protein.seq = self.get_sequence(protein.ac) #X_NOTE: :method:`get_sequence` needs the full ACcession name with isoform information
        protein.length = len(protein.seq)
        # - organism:
        protein.organism = self._get_organism4protxml(prot_xml)
        # - gene and chromosome where the protein is encoded:
        protein.gene = self._get_gene4protxml(prot_xml, protein.ac_noisoform)
        # - linked_repositories:
        #TO_DO: IMPLEMENT!
        # - function:
        protein.function = self._get_functions4protxml(prot_xml)
        # - locations:
        protein.locations = self._get_locations4protxml(prot_xml)
        # - modifications:
        ptms = set( self._iter_ptms4protxml(prot_xml, protein.reviewed) )
        ptms = self._clean_isoform_ptms(protein.ac, ptms, prot_xml) #Clean/Re-arrange the PTMs according to this isoform definition in the XML.
        protein._db_modifications = ptms #X_NOTE: used this instead of :method:``protein.add_modification`` for speed reasons.
        protein.mod_aas = len( {ptm.position for ptm in ptms if ptm.significant} ) #Update number of Significant modified aminoacids.
        # - GO Annotations:
        protein.goannotations = set( self._iter_goannotations4protxml(prot_xml) )
        #
        return protein

    def iter_fetch_proteins(self, proteins):
        """
        Accessory method wrapping :method:`fetch_protein` for fetching
        protein information data from UniProt repository (:attr:`repo_file` or
        the Internet) for multiple proteins.
        
        :param iterable proteins: an iterable object of :class:`Protein`
        instances and/or ACcession number strings.
        
        :return generator : yields :class:`Protein` objects with data fetched
        from UniProt repository (:attr:`repo_file` or Internet).
        """
        for protein in proteins:
            up_protein = self.fetch_protein(protein)
            if up_protein is not None:
                yield up_protein

    def fetch_proteins(self, proteins):
        """
        Accessory method wrapping :method:`iter_fetch_proteins` for
        fetching protein information data from UniProt repository
        (:attr:`repo_file` or the Internet) for multiple proteins.

        :param iterable proteins: an iterable object containing/yielding
        :class:`Protein` instances and/or ACcession number strings.

        :return set : a set containing :class:`Protein` objects with data
        fetched from UniProt repository (:attr:`repo_file` or Internet).
        """
        return set( self.iter_fetch_protein(proteins) )
    
    def iter_all_proteins(self):
        """
        Fetchs protein information data for ALL the proteins in a UniProt
        repository formated Zip file (:attr:`repo_file`).

        :return generator : yields all :class:`Protein` objects with data
        fetched from a UniProt repository formated Zip file (:attr:`repo_file`).
        """
        for zfilen in self.repo_file.namelist():
            if zfilen.upper().endswith('.FASTA'):
                protein_ac = os.path.splitext( os.path.split(zfilen)[1] )[0] # '/path/name.ext' -> 'name.ext' -> 'name'
                up_protein = self.fetch_protein(protein_ac)
                if up_protein is not None:
                    yield up_protein
    
    def all_proteins(self):
        """
        Accessory method wrapping :method:`iter_all_proteins` for
        fetching protein information data for ALL the proteins in a UniProt
        repository file (:attr:`repo_file`).

        :return set : a set containing all :class:`Protein` objects with data
        fetched from a UniProt repository file (:attr:`repo_file`).
        """
        return set( self.iter_all_proteins() )


class QuickGOAWebFetcher(CommonGOFetcher):
    """
    Class to get protein Gene Ontology Annotations found by EBI QuickGO.
    """
    __WEB_SERVICE__ = 'http://www.ebi.ac.uk/QuickGO/GAnnotation'
    
    _DEFAULT_REPO_TYPES = (ST_GO,)

    def _get_dataio4ac(self, ac, format='tsv', **kwargs):
        """
        Get a response from QuickGO Web Service using a GET request
        """
        # Set parameters with defaults:
        kwargs.setdefault('protein', ac)
        kwargs.setdefault('format', format)
        minimun_cols = {'goID','qualifier','goName','aspect','evidence','ref','with', 'from'}
        cols = kwargs.get( 'col', list() )
        if isinstance(cols, str): # `col` argument can be passed as an iterable of strings, a or as a string of comma separated names:
            cols = [ col.strip() for col in cols.split(',') ]
        cols.extend( minimun_cols.difference(cols) ) #Add the elements from minimun_cols not present in cols
        kwargs['col'] = ','.join(cols)
        # Build the URL:
#        request_url = self.__WEB_SERVICE__ + '?' + '&'.join( [key+'='+value for key, value in kwargs] )
        request_url = self.__WEB_SERVICE__ + '?' + urllib.urlencode(kwargs)
        # Get a response input-output object:
        response = urllib2.urlopen(request_url)
        return response

    def _iter_goannotations4qgoanns(self, quickgo_annotations):
        """
        Yields Gene Ontology Annotations from a iterable of EBI QuickGO records.

        :param iterable quickgo_annotations: an iterable of dictionaries, each
        one containing QuickGO Annotation information.

        :return generator : yields :class:`GOAnnotations` objects.
        """
        for qgo_ann in quickgo_annotations:
            # Get the GOTerm:
            go = self._get_or_obogoterm( qgo_ann['GO ID'] )[0]
            # Get the evidence information:
            evidence = dbm.Evidence()
            evidence.type = qgo_ann['Evidence']
            evidence.linked_ids = self._unpack_qgostrdict( qgo_ann['Reference'] )
            # Get other information:
            if qgo_ann['Qualifier'] != '-':
                qualifier = qgo_ann['Qualifier'].upper()
            else:
                qualifier = None
            if qgo_ann['With'] != '-':
                with_from = self._unpack_qgostrdict( qgo_ann['With'] )
            else:
                with_from = None
            original_source = qgo_ann['Source']
            #
            yield dbm.GOAnnotation(go=go, evidence=evidence, 
                                   original_source=original_source,
                                   qualifier=qualifier, with_from=with_from,
                                   source=self.repository)

    def iter_goannotations(self, ac):
        """
        Yields Gene Ontology Annotations found by EBI QuickGO
        (http://www.ebi.ac.uk/QuickGO/) for an UniProtKB ACcession number.

        :param str ac: an UniProtKB ACcession number.

        :return generator : yields :class:`GOAnnotations` objects.
        """
        tsv_data = self._get_dataio4ac(ac)
        go_annotations = tuple( csv.DictReader(tsv_data, delimiter='\t') )
        return self._iter_goannotations4qgoanns(go_annotations)

    def goannotations(self, ac):
        """
        Get Gene Ontology Annotations found by EBI QuickGO
        (http://www.ebi.ac.uk/QuickGO/) for an UniProtKB ACcession number.

        :param str ac: an UniProtKB ACcession number.

        :return set : a set containing :class:`GOAnnotations` objects.
        """
        return set( self.iter_goannotations(ac) )


class UniProtGOAFetcher(RepoFileFetcherMixin, CommonGOFetcher):
    """
    Class to get Protein Gene Ontology Annotations from an UniProt gaf-version
    2.0 gene_association.goa_<species> file.

    :CAUTION: ACcession numbers in those files doesn't include isoforms!
    """
    _DEFAULT_REPO_DATA = UNIPROTGOA
    _DEFAULT_REPO_TYPES = (ST_GO,)
    
    def __init__(self, repo_file, db_session=None, obofilefetcher=None):
        """
        :param object repo_file: zip file containing an UniProt gaf-version 2.0
        gene_association.goa_<species> file. It should be either a string
        containing the path to the file, or a file-like object.
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to fetch stored data.
        :param OBOGOTermFetcher obofilefetcher: optionally supplied
        :class:`OBOGOTermFetcher` instance to use for fetching GOTerms. If none
        is supplied, a new one will be created using `OBO_GO_FILE` as the
        source OBO file.
        """
        super(UniProtGOAFetcher, self).__init__(repo_file,
                                                db_session=db_session,
                                                obofilefetcher=obofilefetcher)

    def _get_repo_file_version(self):
        repo_file_ctime = None
        # Try to get generation date-time from inside repository file:
        with self._repo_file.open(self._repo_file.namelist()[0], 'rU') as io_file:
            for line in io_file:
                if line.startswith('!Generated:'): #Line contains date of file generation:
                    generated = line.lstrip('!Generated:').strip()
                    try:
                        repo_file_ctime = datetime.datetime.strptime(generated,
                                                                     '%Y-%m-%d %H:%M')
                    except ValueError as _:
                        pass
                    break
                elif not line.startswith('!'): #No more meta-data:
                    break
        # Otherwise get filesystem creation date-time from repository file:
        if repo_file_ctime is None:
            repo_file_ctime = datetime.datetime.fromtimestamp( 
                                        os.path.getctime(self.repo_filename) )
        #
        return repo_file_ctime.strftime('%Y.%m') #Version as a string 'year.month'.
    
    def _iter_repofile2qgoanns(self):
        headers = ('DB', 'AC', 'DB_Object_Symbol', 'Qualifier', 'GO ID',
                   'Reference', 'Evidence', 'With', 'Aspect', 'DB_Object_Name',
                   'DB_Object_Synonym', 'DB_Object_Type', 'Taxon', 'Date',
                   'Source', 'Annotation_Extension', 'Gene_Product_Form_ID')
        with self.repo_file.open(self.repo_file.namelist()[0], 'rU') as io_file:
            for line in io_file:
                if line.startswith('!'): continue # Avoid meta-data comments.
                yield dict( zip( headers, line.strip('\n').split('\t') ) )

    def iter_all_goannotations(self):
        """
        Yields Gene Ontology Annotations from a iterable of EBI QuickGO records.

        :param iterable quickgo_annotations: an iterable of dictionaries, each
        one containing QuickGO Annotation information.

        :return generator : yields :class:`GOAnnotations` objects.
        """
        for qgo_ann in self._iter_repofile2qgoanns():
            # Only process UniProt entries:
            if qgo_ann['DB'] != 'UniProtKB': continue
            #
            # Get associated GOTerm:
            go = self._get_or_obogoterm( qgo_ann['GO ID'] )[0]
            # Get associated Evidence information:
            evidence = dbm.Evidence()
            evidence.type = qgo_ann['Evidence']
            evidence.linked_ids = self._unpack_qgostrdict( qgo_ann['Reference'] )
            # Get other information:
            if qgo_ann['With'] != '':
                with_from = self._unpack_qgostrdict( qgo_ann['With'] )
            else:
                with_from = None
            #
            yield dbm.GOAnnotation(protein_ac=qgo_ann['AC'], go=go, #CAUTION: Those ACcession numbers doesn't include isoforms (UniProt gaf-version 2.0 gene_association.goa_<species> file)
                                   evidence=evidence, 
                                   original_source=qgo_ann['Source'],
                                   qualifier=qgo_ann['Qualifier'].upper(),
                                   with_from=with_from, 
                                   source=self.repository)


class ExperimentsFetcher(RepoFileFetcherMixin, Fetcher):
    """
    Class to get Experiments and SupraExperiments information from TSV files.
    """
    _repofile_id2experiment = None # Cache dictionary for Experiments file data: {'ABC-1': Experiment(), ...}
    _repofile_id2supraexperiment = None # Cache dictionary for SupraExperiments file data: {'ABC': SupraExperiment(), ...}
    
    def __init__(self, repo_file, db_session=None, caching=True, 
                 cache_limit=None, sources=REPOSITORIES):
        """
        :param object repo_file: zip file containing Experiments and 
        SupraExperiments information as TSV files. It should be either a string
        containing the path to the file, or a file-like object.
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to get stored data and put some of the new created data. 
        Defaults to None (no database Session).
        :param bool caching: indicates if the converter should use a caching
        strategy for some created objects. Defaults to True.
        :param int cache_limit: optional, maximum number of objects allowed in
        the cache; or None (no limit). Defaults to None (no limit).
        :param iterable sources: an iterable of dictionaries containing data 
        about Sources/Repositories of information. Defaults to the global 
        constant `REPOSITORIES`.
        """
        self.organism = None
        super(ExperimentsFetcher, self).__init__(repo_file, 
                                                 db_session=db_session, 
                                                 caching=caching, 
                                                 cache_limit=cache_limit)
        #
        self._name2sourcedata = {source_data['name']: source_data 
                                 for source_data in sources}
    
    # ``db_session`` property setter override:
    @Fetcher.db_session.setter
    def db_session(self, session):
        Fetcher.db_session.fset(self, session) #Call :method:`fset` of the superclass :property:`db_session`.
        # Organism for the cell lines model objects:
        self.organism, _ = self._select_or_create(**HUMAN)

    def _get_source4name_version(self, source_name, source_version=None):
        """
        Get a :class:`Source` instance using the repository name and,
        optionally, it's desired version.
        
        :param str source_name: the repository name.
        :param object source_version: optionally, the repository desired
        version as a string. Defaults to None (use the version value from the
        predefined repository Global, if available).
        """
        # Try to get as much source data as possible from the `source_name`
        # using a name to source data dictionary (:attr:`_name2sourcedata`). 
        # If none is available, generate the minimal source data dictionary
        # needed:
        source_data = self._name2sourcedata.get( source_name, 
                                                 {'ModelCls': dbm.Source, 
                                                  'name': source_name} )
        # If a desired version has been provided, use it as part of the source
        # data:
        if source_version:
            source_data = source_data.copy() #Avoid modification of original dictionary values in :attr:`_name2sourcedata`.
            source_data['version'] = source_version
        # Get Source/Repository instance from DataBase, or create it: 
        source, _ = self._select_or_create(**source_data)
        return source
    
    def _get_cellline4name(self, cellline_name):
        """
        Get a :class:`CellLine` instance using only the cell line name.
        
        :param str cellline_name: the cell line name.
        """
        # Get CellLine instance from DataBase, or create it:
        cellline, _ = self._select_or_create(dbm.CellLine, name=cellline_name, 
                                             organism=self.organism)
        return cellline
        
    def _iter_repofile_supraexperiments(self):
        with self.repo_file.open('supraexperiments.tsv', 'rU') as io_file:
            csvdr = csv.DictReader(io_file, delimiter='\t')
            for row in csvdr:
                # Create SupraExperiment instance from row data:
                supexp = dbm.SupraExperiment( id=row['supraexp_id'], 
                                              name=row['name'] )
                # Link to a Source and a CellLine instances:
                supexp.source = self._get_source4name_version( row['original_data_source'], 
                                                               row['data_source_version'] )
                supexp.cell_line = self._get_cellline4name( row['cell_type'] )
                yield supexp
    
    @property
    def repofile_id2supraexperiment(self):
        """
        Property with Lazy initialization from a zipped experiments TSV file.
        """
        if self._repofile_id2supraexperiment is None:
            # Lazy loading of SupraExperiments from a zipped TSV file:
            print('\nLoading SupraExperiments from file...')
            self._repofile_id2supraexperiment = { supexp.id: supexp for supexp in 
                                                  self._iter_repofile_supraexperiments() }
            print('Finished loading SupraExperiments from file.')
        return self._repofile_id2supraexperiment
    
    def _iter_repofile_experiments(self):
        with self.repo_file.open('experiments.tsv', 'rU') as io_file:
            csvdr = csv.DictReader(io_file, delimiter='\t')
            for row in csvdr:
                # Extract Experiment 'optional' data from row dictionary:
                extradata={ 'description': row['description'], 
                            'user': row['user'], 
                            'lc_conditions': row['LC_conditions'] }
                if row['quantitative_labeling']:
                    exp_quant_labeling = row['quantitative_labeling'].split('|')
                else:
                    exp_quant_labeling = None
                if row['quantitative_conditions']:
                    exp_quant_conditions = row['quantitative_conditions'].split('|')
                else:
                    exp_quant_conditions = None
                exp_date = None
                if row['date']:
                    try:
                        exp_date = datetime.datetime.strptime(row['date'], 
                                                              "%Y/%m/%d")
                    except ValueError as _:
                        pass
                # Create Experiment instance from row data:
                exp = dbm.Experiment(id=row['experiment_id'], 
                                     target_proteome=row['target_proteome'], 
                                     name=row['name'], 
                                     ms_instrument=row['MS_instrument'], 
                                     quant_type=row['quantitative_type'], 
                                     quant_labeling=exp_quant_labeling, 
                                     quant_conditions=exp_quant_conditions, 
                                     date=exp_date, 
                                     extradata=extradata)
                # Link to a SupraExperiment instance:
                exp.supraexp = self.get_supraexperiment( row['supraexp_id'] )
                yield exp
    
    @property
    def repofile_id2experiment(self):
        """
        Property with Lazy initialization from a zipped experiments TSV file.
        """
        if self._repofile_id2experiment is None:
            # Lazy loading of Experiments from a zipped TSV file:
            print('\nLoading Experiments from file...')
            self._repofile_id2experiment = { exp.id: exp for exp in 
                                             self._iter_repofile_experiments() }
            print('Finished loading Experiments from file.')
        return self._repofile_id2experiment
    
    def iter_repofile_supraexperiments(self):
        return self.repofile_id2supraexperiment.itervalues()
    
    def iter_repofile_experiments(self):
        return self.repofile_id2experiment.itervalues()
    
    def _multi_get_model_obj(self, obj_id, ModelCls, id2modelobj, default=None):
        """
        Try to get a model object from multiple sources using its unique ID.
        
        :param object obj_id: an ID value for the desired model object.
        :param class ModelCls: a SQLAlchemy model class. Model object should be 
        an instance of this class.
        :param dict id2modelobj: {ID value: model object, ...}
        :param object default: default object to return if the ID is not found 
        in any source. Defaults to None.
        """
        if self.db_session:
            # First, try to get model object from the DataBase:
            model_obj = self.db_session.query(ModelCls).get(obj_id)
            if not model_obj:
                # If not in DataBase try to get it from the supplied repository
                # dictionary (`id2modelobj`):
                model_obj = id2modelobj.get(obj_id, default)
                if model_obj: self.db_session.add(model_obj)
        else:
            # Try to get model object directly from the supplied repository
            # dictionary (`id2modelobj`):
            model_obj = id2modelobj.get(obj_id, default)
        return model_obj
    
    def get_supraexperiment(self, supexpid, default=None):
        return self._multi_get_model_obj(supexpid, dbm.SupraExperiment, 
                                         self.repofile_id2supraexperiment, 
                                         default)
    
    def get_experiment(self, expid, default=None):
        return self._multi_get_model_obj(expid, dbm.Experiment, 
                                         self.repofile_id2experiment, 
                                         default)


class LymPHOSFetcher(Fetcher):
    """
    Class to get Peptide and Protein infromation from LymPHOS2 MySQL database.
    """
    _DEFAULT_REPO_DATA = LYMPHOS2
    _DEFAULT_REPO_TYPES = (ST_MSDATA,)
    
    ASCORE_THRESHOLD = 19.0 #Ascore Threshold for the main PTM handled by the fetcher class.
    CELLLINE = TCELLS #A dictionary with params/data needed by :func:`dbm.select_or_create` (or :func:`dbm.get_or_create`) for a :class:`dbm.CellLine` instance.

    def __init__(self, lymphosdb_uri, db_session=None, caching=True,
                 cache_limit=100000, min_peplength=MIN_PEPLENGTH):
        """
        :param str lymphosdb_uri: the LymPHOS2 DB URI to connect to.
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to fetch stored data.
        :param bool caching: indicates if the converter should use a caching
        strategy form some created objects. Defaults to True.
        :param int cache_limit: maximum number of objects allowed in the cache,
        or None (no limit). Defaults to 100000 (try to avoid high memory use).
        :param int min_peplength: the minimum length a MS Peptide should have 
        (shorter ones will not be created/imported/returned). Defaults to the 
        value of the global `MIN_PEPLENGTH`.
        """
        super(LymPHOSFetcher, self).__init__(db_session, caching=caching,
                                             cache_limit=cache_limit)
        self._repository = None
        #
        self.min_peplength = min_peplength #Minimum peptide objects sequence length.
        self.ldb_engine = sqlalchemy.create_engine(lymphosdb_uri)
        self.ldb_new_session = sessionmaker(bind=self.ldb_engine)
        # Auto-Map the LymPHOS2 DB tables to SQLAlchemy ORM classes:
        self.ldb_Base = automap_base()
        self.ldb_Base.prepare(self.ldb_engine, reflect=True)
        # -Fix problems in SQLAlchemy reflection mechanism where MySQL DOUBLE
        #  fields get mapped to Python `Decimal` instead of `float`
        #  (X_NOTE: http://stackoverflow.com/q/19387882):
        for table in self.ldb_Base.metadata.tables.values():
            for column in table.columns.values():
                if isinstance(column.type, sqlalchemy.Numeric):
                    column.type.asdecimal = False #Don't threat this field as :class:`Decimal` but as :class:`float`.
        # -Make mapped class models more accessible:
        self.ldb_models = self.ldb_Base.classes

    # ``db_session`` property setter override:
    @Fetcher.db_session.setter
    def db_session(self, session):
        Fetcher.db_session.fset(self, session) #Call :method:`fset` of the superclass :property:`db_session`.
        # Set default PTM attribute:
        self.phosphorylation, isnew = self._select_or_create(**PHOSPHORYLATION)
        if isnew:
            self.phosphorylation.extradata = PHOSPHORYLATION_EXTRADATA
        # Set default :attr:`organism` and :attr:`cellline`:
        self.organism, _ = self._select_or_create(**HUMAN)
        self.cellline, _ = self._select_or_create(organism=self.organism,
                                                  **self.CELLLINE)
    
    def _get_default_repository(self):
        """
        Method to get/create a default repository, when none is provided, 
        using version information from LymPHOS DB.
        This method overwrites :class:`Fetcher` method to add functionality.
        
        :return object : a default :class:`Source` instance from data contained
        in :attr:`_DEFAULT_REPO_DATA` dictionary and version information from 
        LymPHOS DB; or None.
        """
        # Get last version data from LymPHOS DB:
        with self.ldb_session() as ldb_session:
            try:
                ldb_version = ldb_session.query(self.ldb_models.lymphos_version)[-1]
                version = ldb_version.db_version
            except IndexError as _:
                version = None
        # Create the LymPHOS repository object:
        repository, _ = self._select_or_create(version=version, 
                                               **self._DEFAULT_REPO_DATA)
        # Update Source.source_types:
        source_types = set()
        for dflt_repotype in self._DEFAULT_REPO_TYPES: 
            src_type, _ = self._select_or_create(**dflt_repotype)
            source_types.add(src_type)
        if source_types.difference(repository.source_types):
            repository.source_types.update(source_types)
        #
        if not repository.ptmid2threshold: #X-NOTE: This happens when the repository is new, or has no data in the database for this field. But the database may contain not updated information...
            repository.ptmid2threshold = {
                              PHOSPHORYLATION['id']: self.ASCORE_THRESHOLD
                                          }
        return repository
    
    @contextlib.contextmanager
    def ldb_session(self, *args, **kwargs):
        """
        A LymPHOS2 DB session context manager.
        """
        session = self.ldb_new_session(*args, **kwargs)
        try:
            yield session
        except Exception as _:
            raise
        finally:
            session.expunge_all() #Remove all object instances from this session so they are available as normal python objects.
            session.rollback() #Ensure no changes are commited to LymPHOS DB (just in case).
            session.close()

    def _get_ldb_protein(self, ac, ldb_session):
        """
        Use a provided LymPHOS2 DB session to get a Protein object from its
        ACcession number.

        :param str ac: an UniProtKB ACcession number.
        :param Session ldb_session: an active LymPHOS DB session.

        :return Protein : a LymPHOS2 DB protein object 
        (a :class:`self.ldb_models.lymphos_proteins` instance), or None if the 
        Protein is not found.
        """
        return ldb_session.query(self.ldb_models.lymphos_proteins).get(ac)

    def get_ldb_protein(self, ac):
        """
        Use a new LymPHOS2 DB session to get a Protein object from its
        ACcession number.

        :param str ac: an UniProtKB ACcession number.

        :return Protein : a LymPHOS2 DB protein object 
        (a :class:`self.ldb_models.lymphos_proteins` instance), or None if the 
        Protein is not found.
        """
        with self.ldb_session() as ldb_session:
            ldb_protein = self._get_ldb_protein(ac, ldb_session)
        return ldb_protein

    def _get_locations4ldbprot(self, ldb_protein):
        return ldb_protein.location.split(', ')

    def get_locations(self, ac):
        """
        :param str ac: an UniProtKB ACcession number.

        :return list : the list of Protein locations, or None if the protein is
        not found.
        """
        ldb_protein = self.get_ldb_protein(ac)
        if ldb_protein is not None:
            return self._get_locations4ldbprot(ldb_protein)
        else:
            return None #No protein found
    
    def _get_supraexperiment4ldbexpgroup(self, ldb_expgroup):
        """
        Get a SupraExperiment for a LymPHOS2 DB expgroup record.

        :param object ldb_expgroup: a LymPHOS2 DB expgroup object (this is, 
        a :class:`self.ldb_models.lymphos_expgroup` instance).
        
        :return SupraExperiment : a :class:`SupraExperiment` object.
        """
        supraexp, isnew = self._get_or_create(dbm.SupraExperiment, 
                                              "LymPHOS2-%s"%ldb_expgroup.id)
        if isnew:
            # Get SupraExperiment data:
            supraexp.name = ldb_expgroup.name
            supraexp.source = self.repository
            supraexp.cell_line = self.cellline
            supraexp.extradata['description'] = ldb_expgroup.description
        return supraexp
    
    def _get_experiment4ldbexp(self, ldb_exp):
        """
        Get a Experiment for a LymPHOS2 DB experiments record.

        :param object ldb_exp: a LymPHOS2 DB experiments object (this is, 
        a :class:`self.ldb_models.lymphos_experiments` instance).
        
        :return Experiment : a :class:`Experiment` object.
        """
        experiment, isnew = self._get_or_create( dbm.Experiment, 
                                                 "LymPHOS2-" + str(ldb_exp.id) )
        if isnew:
            # Set a target proteome and get the name:
            experiment.target_proteome = "Phosphorylation"
            experiment.name = ldb_exp.name
            # Get quantitative data:
            ldb_exp_label = ldb_exp.label
            if ldb_exp_label:
                experiment.quant_type = ldb_exp_label
                if ldb_exp_label == 'iTRAQ':
                    ions = range(114, 118)
                else:
                    ions = range(126, 132)
                lbl_tmplt = ldb_exp_label + "-%s"
                experiment.quant_labeling = [lbl_tmplt%ion for ion in ions]
                experiment.quant_conditions = ldb_exp.quant_cond.split('|')
            # Get extra data:
            experiment.extradata = {'description': ldb_exp.description, 
                                    'user': '', 'lc_conditions': ''}
            # Get the SupraExperiment:
            experiment.supraexp = self._get_supraexperiment4ldbexpgroup(ldb_exp.lymphos_expgroup)
        return experiment

    def _get_mspepregulation4ldbpep(self, ldb_pep, ldb_expgroups, ldb_session): #TEST: permissive filter
        """
        :return dict : {experiment name: {statistic indicating regulation: {condition: 1 (Up), 0 (no-change) or -1 (Down), ...}, ...}, ...} ;
        or None.
        """
        # Search the ``lymphos_quantpepexpcond`` records (which contain 
        # statistic quantitative information for each condition/time of a 
        # supra-experiment) that are related to a MS Peptide:
        quantpepexpcond = self.ldb_models.lymphos_quantpepexpcond
        ldb_quantpepexpconds = ldb_session.query(quantpepexpcond)\
                                          .filter(quantpepexpcond.unique_pep==ldb_pep.peptide)\
                                          .all()
        # Filter for regulation changes according to any of the LymPHOS2 applied 
        # statistics methods (trends/"arrows", t tests and DanteR) and format 
        # them:
        expid2expname = {ldb_expgroup.id: ldb_expgroup.name #Supra-experiment ID -> Supra-experiment name.
                         for ldb_expgroup in ldb_expgroups}
        regulation = defaultdict(dict) #{experiment name: {statistic indicating regulation: {condition: 1 (Up), 0 (no-change) or -1 (Down), ...}, ...}, ...}
        for ldb_quantpepexpcond in ldb_quantpepexpconds:
            exp_name = expid2expname[ldb_quantpepexpcond.expgroup_id] #Supra-experiment name.
            conds = ldb_quantpepexpcond.dballquanttimes.split('|') #Supra-experiment conditions/times.
            # - For trend statistic:
            spectraregs4conds = [ map( int, spectraregs.split('^') ) #Number of spectra up-regulated(position 0), without changes (position 1) and down-regulated (position 2); for each condition/time.
                                  for spectraregs in 
                                  ldb_quantpepexpcond.dbavgregulation.split('|') ]
            reg_vals = list()
            for spectraregs in spectraregs4conds:
                total = float( sum(spectraregs) )
                if total > 1: #More than 1 spectrum:
                    up_or_down = spectraregs[2] - spectraregs[0] #upregulated - downregulated.
                    abs_up_or_down = abs(up_or_down)
                    if up_or_down != 0: #down (-1) or up (1) regulation:
                        reg_val = -1 if up_or_down < 0 else 1
                    else: #No-regulation (0):
                        reg_val = 0
                        if spectraregs[1]:
                            abs_up_or_down = spectraregs[1]
                        else:
                            abs_up_or_down = total / 2 #This is to have a reg_fit of 0.5 (50%).
                    reg_fit = abs_up_or_down / total #Fit index: the goodness of the election.
                else: #Only 1 spectrum:
                    reg_val = spectraregs.index(1) - 1
                    reg_fit = -1
                if reg_fit > 0.20: #Only if trend threshold (very weak trend) passed add the trend change: 
                    reg_vals.append(reg_val)
                else:
                    reg_vals.append(0)
            if any(reg_vals): #Only if any value indicating a trend in regulation:
                regulation[exp_name]['trend>0.20'] = { cond: reg_val 
                                                       for cond, reg_val in
                                                       zip(conds, reg_vals) }
            # - For t test statistic:
            reg_signs = [ 1 if float(ratio) >= 0.0 else -1 #1: Up-regulated, -1: Down-regulated, according to PQuantifier average normalized log2(ratios); for each condition/time.
                          for ratio in ldb_quantpepexpcond.dbavgratios.split('|') ]
            for ttest, ttest_field in ( ('ttest95', ldb_quantpepexpcond.dbttestpassed95), 
                                        ('ttest99', ldb_quantpepexpcond.dbttestpassed99) ):
                if not ttest_field: #No t test data -> skip processing:
                    continue
                ttestpassed = map( int, ttest_field.split('|') ) #1: t test passed, 0: not-passed; for each condition/time.
                if any(ttestpassed): #Only if any value indicating significant regulation changes:
                    regulation[exp_name][ttest] = { cond: ttest_val * sign 
                                                    for cond, ttest_val, sign in
                                                    zip(conds, ttestpassed, reg_signs) }
            # - For DanteR statistic:
            if ldb_quantpepexpcond.dbdanteradjpvalues: #Only if any DanteR data:
                avgratios = map( float, ldb_quantpepexpcond.dbdanteravgratios.split('|') ) #Average log2(ratios); for each condition/time.
                adjpvalues = map( float, ldb_quantpepexpcond.dbdanteradjpvalues.split('|') ) #Adjusted p-Values; for each condition/time.
                danterpassed = list()
                for adjpvalue, avgratio in zip(adjpvalues, avgratios):
                    fc = 2 ** avgratio
                    if (fc > 1.5 or fc < 1/1.5) and adjpvalue < 0.05: #Only if Fold Change and Adjusted p-Value thresholds passed add the change:
                        danterpassed.append(1)
                    else:
                        danterpassed.append(0)
                if any(danterpassed): #Only if any value indicating significant regulation changes:
                    reg_signs = [1 if avgratio >= 0.0 else -1 #1: Up-regulated, -1: Down-regulated.
                                 for avgratio in avgratios]
                    regulation[exp_name]['danter'] = { cond: danter_val * sign 
                                                       for cond, danter_val, sign in
                                                       zip(conds, danterpassed, reg_signs) }
        #
        return regulation
        
    def _get_mspepregttest954ldbpep(self, ldb_pep, ldb_expgroups, ldb_session): #TEST: moderate filter
        """
        :return dict : {experiment name: {condition: 1 (Up), 0 (no-change) or -1 (Down), ...}, ...} ;
        or None.
        """
        # Search MS Peptide related records indicating significant (t test 95%) 
        # regulation changes:
        quantpepexpcond = self.ldb_models.lymphos_quantpepexpcond
        ldb_quantpepexpconds = ldb_session.query(quantpepexpcond)\
                                          .filter(quantpepexpcond.unique_pep==ldb_pep.peptide,
                                                  quantpepexpcond.dbttestpassed95!='',
                                                  quantpepexpcond.dbttestpassed95!='0',
                                                  quantpepexpcond.dbttestpassed95!='0|0')\
                                          .all()
        if ldb_quantpepexpconds: # Only if any records indicating significant regulation changes:
            # - Format the significant (t test 95%) regulation changes:
            expid2expname = {ldb_expgroup.id: ldb_expgroup.name
                             for ldb_expgroup in ldb_expgroups}
            regulation = dict() #{experiment name: {condition: 1 (Up), 0 (no-change) or -1 (Down), ...}, ...}
            for ldb_quantpepexpcond in ldb_quantpepexpconds:
                exp_name = expid2expname[ldb_quantpepexpcond.expgroup_id]
                conds = ldb_quantpepexpcond.dballquanttimes.split('|')
                ttestpassed95 = map( int, ldb_quantpepexpcond.dbttestpassed95.split('|') )
                reg_signs = [ 1 if float(ratio) >= 0.0 else -1 #1: Up-regulated, -1: Down-regulated
                              for ratio in ldb_quantpepexpcond.dbavgratios.split('|') ]
                regulation[exp_name] = { cond: ttest*sign for cond, ttest, sign in
                                         zip(conds, ttestpassed95, reg_signs) }
            # - Return formated regulation information for the MS peptide:
            return regulation
        else:
            return None
        
    def _get_mspeptide4ldbpep(self, ldb_pep, ldb_session, link2ldb_prots=False):
        """
        Get a Mass Spectrometry Peptide for a LymPHOS2 DB peptidecache record,
        along with their associated modifications (phosphorilations).
        :caution: This method doesn't check sequence length of the supplied 
        `ldb_pep` (although :attr:`min_peplength' has been set), so check it 
        before!
        
        :param object ldb_pep: a LymPHOS2 DB peptidecache object (this is, 
        a :class:`self.ldb_models.lymphos_peptidecache` instance).
        :param Session ldb_session: an active LymPHOS DB session.
        :param bool link2ldb_prots: if True, LymPHOS protein references will be
        linked with the resulting MS Peptide (as :class:`Protein` objects);
        but those proteins, if not in the target database, won't have any data
        apart from the ACcession number. Defaults to False.

        :return MSPeptide : a MS Peptides (:class:`MSPeptide` instance), with
        their associated modifications (:class:`MSPepModification` objects),
        related experiments (:class:`Experiment` objects), and linked proteins
        (:class:`Protein` objects).
        """
        seq_w_allmods = "".join( aa.upper()+"(21)" if aa.islower() else aa #'AAAsAA' -> 'AAAS(21)AA'
                                 for aa in ldb_pep.peptide )               #
        mspep, isnew = self._select_or_create(dbm.MSPeptide,
                                              _db_seq_w_allmods=seq_w_allmods,
                                              source=self.repository)
        if isnew:
            # Add sequence without modifications:
            mspep.seq = ldb_pep.peptide.upper() #'AAAsAA' -> 'AAASAA' (X-NOTE:this is faster for this case than :method:`MSPeptide.seq_w_mods2seq`)
            # Add p-sites as MSPepModifications:
            if ldb_pep.phospho:
                psitepos_ascore = zip( map(int, ldb_pep.dbpsites.split('|')),
                                       map(float, ldb_pep.dbascores.split('|')) )
                modifications = set()
                for psitepos, ascore in psitepos_ascore:
                    modification = dbm.MSPepModification( mod_type=self.phosphorylation,
                                                          position=psitepos + 1, #In LymPHOS2 DB p-site position is in python units (start at 0)
                                                          ascore=ascore,
                                                          significant=(ascore>=self.ASCORE_THRESHOLD) )
                    modifications.add(modification)
                mspep._db_modifications = modifications
            mspep.refresh_seq_w_mods() #Update :attr:`_db_seq_w_mods` with the sequence annotated with Significant and clear Physiological Modification IDs.
            mspep.refresh_mod_aas() #Update :attr:`_db_mod_aas` with number of Significant and clear Physiologically modified AAs.
            mspep.refresh_allphysiomods_signif() #Update :attr:`_db_allnophysiomods_signif`.
            # Add related LymPHOS2 Experiments and SupraExperiments:
            data = self.ldb_models.lymphos_data
            experiments = self.ldb_models.lymphos_experiments
            ldb_expgroups = ldb_session.query(self.ldb_models.lymphos_expgroup)\
                                       .join(experiments)\
                                       .join(data)\
                                       .filter(data.peptide==ldb_pep.peptide)\
                                       .distinct().all()
            ldb_exps = ldb_session.query(experiments)\
                                  .join(data)\
                                  .filter(data.peptide==ldb_pep.peptide)\
                                  .distinct().all()
            mspep.experiments = {self._get_experiment4ldbexp(ldb_exp)
                                 for ldb_exp in ldb_exps}
            # Store protein ACs from original source:
            prots_acs = [ ac_name.split('^')[0] #'AC^protein description' -> 'AC'
                          for ac_name in ldb_pep.dbproteins.split('|') ] #'AC^protein description|AC^protein description' -> ['AC^protein description', 'AC^protein description']
            mspep.prots_from_source = prots_acs
            # Add regulation information:
            if ldb_pep.quantitative: # Only if quantitative MS Peptide:
                    mspep.regulation = self._get_mspepregulation4ldbpep(ldb_pep, ldb_expgroups, ldb_session) #TEST
            # Add or Link related :class:`Proteins` objects according to ACs
            # from original source:
            if link2ldb_prots:
                # X-NOTE: If proteins are not already in the database
                # (typically decoys or proteins deleted from UniProt) they will
                # be created as proteins with an ACcession number, but with NO
                # sequence or other data.
                mspep.proteins = {self._select_or_create(dbm.Protein, ac=ac)[0]
                                  for ac in prots_acs}
        return mspep

    def iter_all_mspeptides(self, ldb_session, link2ldb_prots=False,
                            include_decoys=False):
        """
        Get all mass spectrometry peptides from LymPHOS2 DB (from table
        ``lymphos_peptidecache``), along with their associated modifications
        (phosphorylations).

        :param Session ldb_session: an active LymPHOS2 DB session.
        :param bool link2ldb_prots: if True, LymPHOS protein references will be
        linked with the resulting MS Peptide; but those proteins, if not in the
        target database, won't have any data apart from the ACcession number.
        Defaults to False.
        :param bool include_decoys: if True, MS Peptides pointing to any decoy
        will also be returned. Defaults to False.

        :return generator : yields MS Peptides (:class:`MSPeptide` instances)
        with their associated modifications (:class:`MSPepModification`
        objects), related experiments (:class:`Experiment` objects), and linked
        proteins (:class:`Protein` objects).
        """
        peptidecache = self.ldb_models.lymphos_peptidecache
        min_peplength = self.min_peplength
        #
        ldb_peptides = ldb_session.query(peptidecache)
        if not include_decoys: # Filter decoys:
            ldb_peptides = ldb_peptides.filter( ~peptidecache.dbproteins.like('%decoy%') )
        for ldb_pep in ldb_peptides.all():
            # Filter-out short peptides:
            if len(ldb_pep.peptide) < min_peplength:
                continue #Skip this short ldb_pep
            yield self._get_mspeptide4ldbpep(ldb_pep, ldb_session, link2ldb_prots)

    def _iter_mspeptides4ldbprot(self, ldb_protein, ldb_session,
                                 link2ldb_prots=False, include_decoys=False):
        """
        Get mass spectrometry peptides from LymPHOS DB (from table
        ``lymphos_peptidecache``), along with their associated modifications
        (phosphorylations), for a given LymPHOS DB protein.
        
        :param object ldb_protein: a LymPHOS2 DB protein object (this is, 
        a :class:`self.ldb_models.lymphos_proteins` instance).
        :param Session ldb_session: an active LymPHOS2 DB session.
        :param bool link2ldb_prots: if True, LymPHOS protein references will be
        linked with the resulting MS Peptide; but those proteins, if not in the
        target database, won't have any data apart from the ACcession number.
        Defaults to False.
        :param bool include_decoys: if True, MS Peptides pointing to any decoy
        will also be returned. Defaults to False.
        
        :return generator : yields MS Peptides (:class:`MSPeptide` instances)
        with their associated modifications (:class:`MSPepModification`
        objects), related experiments (:class:`Experiment` objects), and linked
        proteins (:class:`Protein` objects).
        """
        peptidecache = self.ldb_models.lymphos_peptidecache
        min_peplength = self.min_peplength
        #
        if not min_peplength:
            pepnames = sorted( {each.lymphos_data.peptide for each in
                                ldb_protein.lymphos_data_proteins_collection} )
        else: #Filter-out short peptides:
            pepnames = sorted( {each.lymphos_data.peptide for each in
                                ldb_protein.lymphos_data_proteins_collection
                                if len(each.lymphos_data.peptide)>=min_peplength} )            
        ldb_peptides = ldb_session.query(peptidecache)\
                                  .filter( peptidecache.peptide.in_(pepnames) )
        if not include_decoys: # Filter decoys:
            ldb_peptides = ldb_peptides.filter( ~peptidecache.dbproteins.like('%decoy%') )
        for ldb_pep in ldb_peptides.all():
            yield self._get_mspeptide4ldbpep(ldb_pep, ldb_session, link2ldb_prots)

    def get_mspeptides(self, ac, link2ldb_prots=False, include_decoys=False):
        """
        Get mass spectrometry peptides from LymPHOS2 DB (from table
        ``lymphos_peptidecache``), along with their associated modifications
        (phosphorylations), for a given protein ACcession number.

        :param string ac: a protein ACcession number.
        :param bool link2ldb_prots: if True, LymPHOS protein references will be
        linked with the resulting MS Peptide; but those proteins, if not in the
        target database, won't have any data apart from the ACcession number.
        Defaults to False.
        :param bool include_decoys: if True, MS Peptides pointing to any decoy
        will also be returned. Defaults to False.

        :return set : a set of MS Peptides (:class:`MSPeptide` instances) with
        their associated modifications (:class:`MSPepModification` objects),
        related experiments (:class:`Experiment` objects), and linked proteins
        (:class:`Protein` objects); or None if the protein is not found.
        """
        with self.ldb_session() as ldb_session:
            ldb_protein = self._get_ldb_protein(ac, ldb_session)
            if ldb_protein:
                mspeptides = set( self._iter_mspeptides4ldbprot(ldb_protein,
                                                                ldb_session,
                                                                link2ldb_prots,
                                                                include_decoys) )
            else:
                mspeptides = None # No protein found.
        return mspeptides

    def _get_spectrum4ldbdata(self, ldb_data, ldb_session, experiment=None):
        """
        Get a mass spectrometry spectrum for a LymPHOS2 DB data record.

        :param object ldb_data: a LymPHOS2 DB data object (this is, 
        a :class:`self.ldb_models.lymphos_data` instance).
        :param Session ldb_session: an active LymPHOS2 DB session.
        :param experiment: a :class:`Experiment` instance to
        associate this spectrum with. If None, the Experiment will be fetched
        from LymPHOS `expgroup` DB column associated with `ldb_data`. Defaults
        to None.

        :return Spectrum : a Spectrum (:class:`Spectrum` instance), with its
        related experiment (:class:`Experiment` object).
        """
        # Get a LymPHOS2 Spectrum:
        ldb_spectrum = ldb_data.lymphos_spectra_collection[0]
        # Get a LymPHOS2 experiment:
        if experiment is None:
            experiment = self._get_experiment4ldbexp(ldb_data.lymphos_experiments)
        #
        spectrum = dbm.Spectrum(ms_n = ldb_data.ms_n,
                                has_mzs_intensities = True,
                                masses = map( float,
                                              ldb_spectrum.mass.split('|') ),
                                intensities = map( float,
                                                   ldb_spectrum.intensity.split('|') ),
                                raw_file = ldb_data.raw_file,
                                scan_i = ldb_data.scan_i,
                                scan_f = ldb_data.scan_f,
                                rt = ldb_data.rt,
                                charge = ldb_data.charge,
                                parent_mass = ldb_data.mass,
                                experiment = experiment)
        #
        return spectrum

    def _get_psm4ldbdata(self, ldb_data, ldb_session, link2ldb_prots=False,
                         include_decoys=False):
        """
        Get a PSM (peptide spectrum match) from a LymPHOS2 DB data record,
        along with their associated modifications, Spectrum and MSPeptide.

        :param ldb_data: a LymPHOS2 DB data object (this is, 
        a :class:`self.ldb_models.lymphos_data` instance).
        :param Session ldb_session: an active LymPHOS2 DB session.
        :param bool link2ldb_prots: if True, LymPHOS protein references will be
        linked with the resulting MS Peptide; but those proteins, if not in the
        target database, won't have any data apart from the ACcession number.
        Defaults to False.
        :param bool include_decoys: if True, PSM pointing to any decoy will
        also be returned. If False, None will be returned instead. Defaults to
        False.

        :return PSM or None : a :class:`PSM` instance with their associated
        modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        # Filter-out short peptides:
        if len(ldb_data.peptide) < self.min_peplength:
            return None
        # Get mspeptide data:
        ldb_pep = ldb_session.query(self.ldb_models.lymphos_peptidecache)\
                             .filter_by(peptide = ldb_data.peptide)\
                             .first()
        # -Deal with decoys:
        if not include_decoys and 'decoy' in ldb_pep.dbproteins:
            return None #Discarded PSM because of decoys.
        # -Get a :class:``MSPeptide`` instance from its LymPHOS `ldb_pep` counterpart:
        mspep = self._get_mspeptide4ldbpep(ldb_pep, ldb_session, link2ldb_prots)
        # Get experiment:
        experiment = self._get_experiment4ldbexp(ldb_data.lymphos_experiments)
        # Get spectrum data:
        spectrum = self._get_spectrum4ldbdata(ldb_data, ldb_session, experiment)
        # Create the PSM:
        psm = dbm.PSM(spectrum=spectrum, mspeptide=mspep, experiment=experiment)
        # Get search engines scores:
        psm.scores = {'sequest_xcorr': float(ldb_data.xcorr),
                      'sequest_deltacn': float(ldb_data.deltacn),
                      'sequest_d': float(ldb_data.d),
                      'omssa_pvalue': float(ldb_data.omssa),
                      'phenyx_zscore': float(ldb_data.phenyx),
                      'easyprot_zscore': float(ldb_data.easyprot),
                      'peaks_logp': float(ldb_data.peaks),
                      }
        # Get p-sites as PSM Modifications:
        if ldb_data.phospho:
            pep_psites2ascores = zip( map(int, ldb_data.dbpsites.split('|')),
                                      map(float, ldb_data.dbascores.split('|')) )
            modifications = set()
            for psite_pos, ascore in pep_psites2ascores:
                modification = dbm.PSMModification( mod_type=self.phosphorylation,
                                                    position=psite_pos + 1, #In LymPHOS2 DB p-site position is in python units (start at 0)
                                                    ascore=ascore, 
                                                    significant=(ascore>=self.ASCORE_THRESHOLD) )
                modifications.add(modification)
            psm._db_modifications = modifications
        #
        return psm

    def iter_all_psms(self, link2ldb_prots=False, include_decoys=False):
        """
        Iteratively get all PSMs (peptide spectrum matches) from LymPHOS2 DB
        (from table ``lymphos_data``), along with their associated modifications,
        Spectrum and MS Peptide.

        :param bool link2ldb_prots: if True, LymPHOS protein references will be
        linked with the resulting MS Peptide; but those proteins, if not in the
        target database, won't have any data apart from the ACcession number.
        Defaults to False.
        :param bool include_decoys: if True, PSM pointing to any decoy will
        also be returned. Defaults to False.

        :return generator : yields PSMs (:class:`PSM` instances) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        data = self.ldb_models.lymphos_data
        #
        with self.ldb_session() as ldb_session:
            ldb_datas = ldb_session.query(data).all()
            for ldb_data in ldb_datas:
                psm = self._get_psm4ldbdata(ldb_data, ldb_session,
                                            link2ldb_prots, include_decoys)
                if psm is not None: #Avoid decoys and short peptides
                    yield psm

    def _iter_psms4ldbprot(self, ldb_protein, ldb_session, link2ldb_prots=False,
                           include_decoys=False):
        """
        Get PSMs (peptide spectrum matches) from LymPHOS2 DB, along with their
        associated modifications, Spectrum and MSPeptide, for a given LymPHOS2
        DB protein.

        :param object ldb_protein: a LymPHOS2 DB protein object (this is, 
        a :class:`self.ldb_models.lymphos_proteins` instance).
        :param Session ldb_session: an active LymPHOS2 DB session.
        :param bool link2ldb_prots: if True, LymPHOS2 protein references will be
        linked with the resulting MS Peptide; but those proteins, if not in the
        target database, won't have any data apart from the ACcession number.
        Defaults to False.
        :param bool include_decoys: if True, PSM pointing to any decoy will
        also be returned. Defaults to False.

        :return generator : yields PSMs (:class:`PSM` instances) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        ldb_datas = {each.lymphos_data for each in
                     ldb_protein.lymphos_data_proteins_collection}
        for ldb_data in ldb_datas:
            psm = self._get_psm4ldbdata(ldb_data, ldb_session, link2ldb_prots,
                                        include_decoys)
            if psm is not None:
                yield psm

    def get_psms(self, ac, link2ldb_prots=False, include_decoys=False):
        """
        Get PSMs (Peptide Spectrum Matches) from LymPHOS2 DB, along with their
        associated modifications, Spectrum and MSPeptide, for a given protein
        ACcession number.

        :param string ac: a protein ACcession number.
        :param bool link2ldb_prots: if True, LymPHOS2 protein references will be
        linked with the resulting MS Peptide; but those proteins, if not in the
        target database, won't have any data apart from the ACcession number.
        Defaults to False.
        :param bool include_decoys: if True, PSM pointing to any decoy will
        also be returned. Defaults to False.

        :return: a set of PSMs (:class:`PSM` instances) with their associated
        modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects); or None if
        the protein is not found.
        """
        with self.ldb_session() as ldb_session:
            ldb_protein = self._get_ldb_protein(ac, ldb_session)
            if ldb_protein:
                psms = set( self._iter_psms4ldbprot(ldb_protein, ldb_session,
                                                    link2ldb_prots) )
            else:
                psms = None # No protein found.
        return psms

    def _get_psites4ldbprot(self, ldb_protein, ldb_session):
        # Get related phosphopeptides:
        min_peplength = self.min_peplength
        if not min_peplength:
            pepnames = sorted( {each.lymphos_data.peptide for each in
                                ldb_protein.lymphos_data_proteins_collection} )
        else: #Filter-out short peptides:
            pepnames = sorted( {each.lymphos_data.peptide for each in
                                ldb_protein.lymphos_data_proteins_collection
                                if len(each.lymphos_data.peptide)>=min_peplength} )            
        peptidecache = self.ldb_models.lymphos_peptidecache
        phosphopeptides = ldb_session.query(peptidecache).filter(
                                       peptidecache.peptide.in_(pepnames),
                                       peptidecache.phospho == True ).all()
        # Get p-sites information from related phosphopeptides:
        prot_seq = ldb_protein.seq
        prot_psites = defaultdict(list)
        for peptide in phosphopeptides:
            pep_sseq = ppf.formatseq4search(peptide.peptide.upper(), 
                                            ppf.AA2REGEX_PROTEOMICS) #X-NOTE: Use a proteomics substitution dictionary.
            psites_pos_ascores = zip( map(int, peptide.dbpsites.split('|')),
                                      map(float, peptide.dbascores.split('|')) )
            for match in re.finditer(pep_sseq, prot_seq):
                for ppos, pascore in psites_pos_ascores:
                    prot_psites[match.start() + 1 + ppos].append(pascore)
        # Summarize p-sites information in :class:`dbm.ProtModification` instances:
        modifications = set()
        evidence = dbm.Evidence(type='IDA', #'Inferred from Direct Assay'.
                                linked_ids={"LymPHOS" + self.repository.version: ldb_protein.ac} )
        for ppos, ascores in prot_psites.items():
            maxascore = max(ascores)
            modification = dbm.ProtModification(mod_type=self.phosphorylation,
                                                position=ppos + 1, #X-NOTE: In LymPHOS2 DB p-site position is in python units (start at 0)
                                                ascore=maxascore, 
                                                significant= (maxascore>=self.ASCORE_THRESHOLD), 
                                                experimental=True,
                                                cell_lines={self.cellline},
                                                evidences={evidence},
                                                source=self.repository)
            modifications.add(modification)
        #
        return modifications

    def get_psites(self, ac):
        """
        :param str ac: an UniProtKB ACcession number.

        :return set : a set of protein modifications corresponding to the protein
        p-sites, or None if the protein is not found.
        """
        with self.ldb_session() as ldb_session:
            ldb_protein = self._get_ldb_protein(ac, ldb_session)
            if ldb_protein:
                psites = self._get_psites4ldbprot(ldb_protein, ldb_session)
            else:
                psites = None # No protein found.
        return psites

    def fetch_protein(self, protein):
        """
        Fetch protein information data from LymPHOS2 DB and then store it into 
        a :class:`Protein` object (optionally passed as the `protein` parameter
        instead of an accession number).

        :param object protein: a :class:`Protein` instance or an ACcession
        number string.

        :return Protein : a :class:`Protein` object with data fetched from
        LymPHO2S DB, or None if the protein is not found.
        """
        # Get a :class:`Protein` instance, if not supplied:
        if isinstance(protein, str):
            # `protein` parameter is an ACcession number so we need to create a
            # :class:`Protein` object:
            protein, _ = self._select_or_create(dbm.Protein, ac=protein) #Create a :class:`Protein` instance from an ACcession number
        self._last_ac = protein.ac #For logging purposes
        #
        with self.ldb_session() as ldb_session:
            # Get LymPHOS DB protein:
            ldb_protein = self._get_ldb_protein(protein.ac, ldb_session)
            if ldb_protein is None:
                return None # No protein found.
            # Assign sequence (decoys can have sequence data):
            protein.seq = ldb_protein.seq
            #
            # Only process other data for real proteins, not for decoys:
            if ldb_protein.is_decoy:
                protein.is_decoy = True
                return protein
            else:
                protein.is_decoy = False
            #
            # Assign current LymPHOS DB as source_repository:
            protein.source_repository = self.repository
            #
            # Assign LymPHOS DB data to attributes/fields of protein object:
            # - id_in_source_repository:
            protein.id_in_source_repository = ldb_protein.ac
            protein.linked_repositories[ldb_protein.ac] = self.repository
            # - linked_repositories:
            # - name:
            protein.name = ldb_protein.name
            # - organism:
            protein.organism = self.organism
            # - chromosome:
            protein.chr = ldb_protein.chr
            # - function:
            protein.function = ldb_protein.function
            # - locations:
            protein.locations = self._get_locations4ldbprot(ldb_protein)
            # - modifications:
            protein._db_modifications = self._get_psites4ldbprot(ldb_protein, ldb_session) #X_NOTE: used this form instead of protein.add_modification() for speed.
            protein.mod_aas = len( protein.pos2modifications.keys() ) #Update the number of modified aminoacids of the protein.
        #
        return protein


class TSVMSDataFetcher(RepoFileFetcherMixin, Fetcher):
    """
    Abstract Class to get MS Peptides and their Modification information (as
    well as PSM and their Modifications, available Spectrum data, ...) from
    article supplementary information tables, as a TSV (or CSV) files.
    """
    ASCORE_THRESHOLD = 0.9 #(A)Score Threshold for the main PTM handled by the fetcher class.
    PTMID2THRESHOLD = None #A Dictionary of PTM IDs to its corresponding (A)Score Threshold. Ex.: {21: 19, 121: 0.75}
    CELLLINE = JURKAT #A dictionary with params/data needed by :func:`dbm.select_or_create` (or :func:`dbm.get_or_create`) for a :class:`dbm.CellLine` instance.
    
    _DEFAULT_REPO_TYPES = (ST_MSDATA,) # Those Fetchers generally only contain Mass Spectrometry data.
    
    def __init__(self, repo_file, db_session=None, caching=True,
                 cache_limit=100000, experiments=EXPERIMENTS_FILE, 
                 min_peplength=MIN_PEPLENGTH):
        """
        :param object repo_file: zip file containing supplementary information 
        tables as TSVs. It should be either a string containing the path to the 
        file, or a file-like object.
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to fetch stored data.
        :param bool caching: indicates if the converter should use a caching
        strategy form some created objects. Defaults to True.
        :param int cache_limit: maximum number of objects allowed in the cache,
        or None (no limit). Defaults to 100000 (try to avoid high memory use).
        :param object experiments: a :class:`ExperimentFetcher` instance, or 
        either a string containing the path to a file, or a file-like object, 
        of a zip file containing Experiments and SupraExperiments information 
        as TSV files. Defaults to global ``EXPERIMENTS_FILE``.
        :param int min_peplength: the minimum length a MS Peptide should have 
        (shorter ones will not be created/imported/returned). Defaults to the 
        value of the global `MIN_PEPLENGTH`.
        """
        #Set :attr:`expfetcher` with a :class:`ExperimentFetcher` instance:
        if not isinstance(experiments, ExperimentsFetcher):
            experiments = ExperimentsFetcher(experiments)
        self.expfetcher = experiments
        #
        super(TSVMSDataFetcher, self).__init__(repo_file,
                                               db_session=db_session,
                                               caching=caching,
                                               cache_limit=cache_limit)
        #
        self.min_peplength = min_peplength #Minimum peptide objects sequence length.
        # Defaults for Coded MODification -> MODification ATTRibute:
        self.CMOD2MODATTR = {'gl': 'ubiquitination',
                             'ac': 'acetylation',
                             'ox': 'oxidation',
                             'ph': 'phosphorylation', 
#                             '#' : 'phosphorylation',
                             }
    
    def _get_default_repository(self):
        """
        Method to get/create a default repository, when none is provided, but a 
        repository file is available.
        Overwrites :superclass:`RepoFileFetcherMixin` method.
        
        :return object : a default :class:`Source` instance from data contained
        in :attr:`_DEFAULT_REPO_DATA` dictionary, repository file version as 
        returned by :method:`_get_repo_file_version`, and also dictionary data 
        contained in :attr:`PTMID2THRESHOLD`; or None.
        """
        repository = super(TSVMSDataFetcher, self)._get_default_repository()
        if repository and not repository.ptmid2threshold: #X-NOTE: This happens when the repository is new, or has no data in the database for this field. But the database may contain not updated information.
            repository.ptmid2threshold = self.PTMID2THRESHOLD
        return repository
    
    # ``db_session`` property setter override:
    @Fetcher.db_session.setter
    def db_session(self, session): #TO_DO: Move this, or parts, to Fetcher class???
        Fetcher.db_session.fset(self, session) #Call :method:`fset` of the superclass :property:`db_session`.
        # Set :attr:`expfetcher.db_session`:
        self.expfetcher.db_session = session
        # Set default PTMs attributes:
        for attr, ptm_modeldata, ptm_modelextradata in (
                ('phosphorylation', PHOSPHORYLATION, PHOSPHORYLATION_EXTRADATA),
                ('acetylation', ACETYLATION, ACETYLATION_EXTRADATA), 
                ('ubiquitination', UBIQUITINATION, UBIQUITINATION_EXTRADATA), 
                ('oxidation', OXIDATION, OXIDATION_EXTRADATA)
                                                         ):
            ptm_modelobj, isnew = self._select_or_create(**ptm_modeldata)
            if isnew:
                ptm_modelobj.extradata = ptm_modelextradata
            setattr(self, attr, ptm_modelobj)
        # Set default :attr:`organism` and :attr:`cellline`:
        self.organism, _ = self._select_or_create(**HUMAN)
        self.cellline, _ = self._select_or_create(organism = self.organism,
                                                  **self.CELLLINE)

    def _get_pos2psmmods4row(self, row, modified_seq_field,
                             probabilities_seq_field, main_modification):
        """
        Get Positions -> PSM Modifications for a `row` dictionary from a TSV (or
        CSV) file.
        
        :param dict row: a row dictionary from a TSV file.
        :param str modified_seq_field: the field/key, in the row dictionary,
        corresponding to the modified sequence identified by the search engine.
        :param str probabilities_seq_field: the field/key, in the row dictionary,
        corresponding to the probabilities for the modifications in the
        `modified_seq_field`.
        :param str main_modification: the studied coded modification in the 
        article source data.
        
        :return tuple : { position: {PSMModification(), ...} }, clean aminoacid
        sequence.
        """
        modified_seq = row[modified_seq_field]
        # Get modifications probabilities grouped by position from field 
        # `probabilities_seq_field`:
        _, pos2mainmod_prob = ppf.split_seq_and_things( 
                                                 row[probabilities_seq_field], 
                                                 thing_func=float )
        # Get PSM Modifications grouped by position, and clean sequence, from
        # `modified_sequence` field:
        modified_seq = modified_seq.strip('_') #'_(ac)ADKPDM(ox)GEIASFDK(gl)AK_' -> '(ac)M(ox)ADKPDM(ox)GEIASFDK(gl)AK'
        if modified_seq.startswith('('): # Deal with different nomenclature for N-term PTMs (usually in N-term acetylations):
            modified_seq = modified_seq[4] + modified_seq[0:4] + modified_seq[5:] #'(ac)M(ox)ADKPDM(ox)GEIASFDK(gl)AK' -> 'M(ac)(ox)ADKPDM(ox)GEIASFDK(gl)AK'
        clean_seq, pos2c_mods = ppf.split_seq_and_things(modified_seq) #Get clean sequence ('ADKPDMGEIASFDKAK') and coded modifications grouped by position.
        pos2psmmods = defaultdict(set)
        for pos, c_mods in pos2c_mods.items(): #Get PSM Modifications grouped by position from coded modifications and modifications probabilities grouped by position:
            for c_mod in c_mods:
                mod_type = getattr( self, self.CMOD2MODATTR[c_mod] ) #'coded modification' -> self.modification_type
                psmmod = dbm.PSMModification(mod_type=mod_type, position=pos)
                if c_mod == main_modification: # Only the studied modification in the article' table would have ascore information:
                    ascore = max( pos2mainmod_prob.get( pos, [None] ) ) #Max. ascore for the modification position, or None.
                    if ascore is not None:
                        psmmod.ascore = ascore
                        psmmod.significant = (ascore>=self.ASCORE_THRESHOLD)
                pos2psmmods[pos].add(psmmod)
        #
        return pos2psmmods, clean_seq

    def _get_mspeptide4data(self, row, seq, pos2psmmods, psmmods, experiment,
                            evidence, prots_from_source):
        """
        Get the MS Peptide with their associated modifications from multiple
        data variables (obtained basically from a single PSM).
        :caution: This method doesn't check sequence length of the supplied 
        `seq` (although :attr:`min_peplength' has been set), so check it 
        before!
        
        :param dict row: a row dictionary from a TSV (or CSV) file.
        :param str seq: the MS Peptide aminoacid sequence.
        :param dict pos2psmmods: { position: {PSMModification(), ...} }
        :param iterable psmmods: an iterable of :class:`PSMModification` 
        instances corresponding to the associated PSM. Normally a set.
        :param Experiment experiment: a :class:`Experiment` instance
        corresponding to the associated PSM.
        :param Evidence evidence: a :class:`Evidence` instance corresponding to
        the associated PSM.
        :param list prots_from_source: a list of matching UniProt ACs from the
        original source data.

        :return MSPeptide : a :class:`MSPeptide` instance (a new one, or a
        matching one obtained from the database and updated with new data),
        along with their associated :class:`MSPeptideModification` instances.
        """
        seq_w_allmods = dbm.MSPeptide.seq2seq_w_mods(seq, pos2psmmods) #Get sequence annotated with ALL (significant or not, physiological or not) Modification IDs: 'M(35)S(21)K(121)AK(1)AR'
        mspep, isnew = self._select_or_create(dbm.MSPeptide,
                                              _db_seq_w_allmods=seq_w_allmods,
                                              source=self.repository)
        if isnew:
            # A new one -> Add initial information from the PSM:
            mspep.seq = seq
            mspep._db_modifications = {dbm.MSPepModification( mod_type=mod.mod_type, 
                                                              position=mod.position, 
                                                              ascore=mod.ascore, 
                                                              significant=mod.significant, 
                                                              evidences={evidence} )
                                       for mod in psmmods}
            mspep.experiments = {experiment}
            mspep.prots_from_source = sorted(prots_from_source)
        else:
            # A matching one obtained from the Database/Cache -> Update with
            # information from the PSM: 
            # - Update modification ascores with the maximum one, so also
            #   modification significance, and modification evidences:
            get_pos_modtype = attrgetter('position', 'mod_type.id')
            s_mspepmods = sorted(mspep.modifications, key=get_pos_modtype) # Align MS Peptide and PSM Modifications
            s_psmmods = sorted(psmmods, key=get_pos_modtype)               #
            for mspepmod, psmmod in zip(s_mspepmods, s_psmmods):
#                 assert(mspepmod.position == psmmod.position and    #DEBUG
#                        mspepmod.mod_type.id == psmmod.mod_type.id) #
                ascore = max(mspepmod.ascore, psmmod.ascore)
                if ascore is not None:
                    mspepmod.ascore = ascore # Update ascore
                    mspepmod.significant = (ascore >= self.ASCORE_THRESHOLD) # Update significance
                mspepmod.evidences.add(evidence) # Update evidences
            # - Update experimentss:
            mspep.experiments.add(experiment)
            # - Update proteins obtained from source data:
            mspep.prots_from_source = sorted( set(mspep.prots_from_source)\
                                              .union(prots_from_source) )
        # Set/Update MSPeptide database-cached attributes:
        mspep.refresh_seq_w_mods() #For :attr:`_db_seq_w_mods`, with the sequence annotated with Significant and clear Physiological Modification IDs.
        mspep.refresh_mod_aas() #For :attr:`_db_mod_aas`, with number of Significant and clear Physiologically modified AAs.
        mspep.refresh_allphysiomods_signif() #For :attr:`_db_allnophysiomods_signif`.
        #
        return mspep

    def _get_spectrum4row(self, row, experiment):
        """
        Get the Spectrum for a `row` dictionary from TSV (or CSV) data.

        :caution: Should be implemented in sub-classes!
        """
        raise NotImplementedError('Sub-class responsibility!')
        # return dbm.Spectrum()
    
    def tsv_namelist(self):
        """
        Returns/filters only the TSV file names (with .TSV extension) inside
        :attr:``repo_file``, so only TSV files are imported/processed.
        """
        if self.repo_file:
            return [tsv_filen for tsv_filen in self.repo_file.namelist() 
                    if os.path.splitext(tsv_filen)[1].upper() == '.TSV']
        else:
            return list()
    
    def iter_all_psms(self):
        """
        Iteratively get all PSMs from supplementary information tables, along
        with their associated modifications.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).

        :caution: Should be implemented in sub-classes! And because it usually
        calls :method:`_get_mspeptide4data`, check first sequence length in
        sub-class implementations.
        """
        raise NotImplementedError('Sub-class responsibility!')
        # for tsv_filen in self.tsv_namelist(): #Process only TSV files:
        #     #Check sequence length before calling :method:`_get_mspeptide4data`
        #     yield dbm.PSM()


class Choudhary09AcFetcher(TSVMSDataFetcher):
    """
    Class to get MS peptides and Acetylation information from Choudhary et al.
    2009 supplementary information tables as TSV files.

    X-NOTE: Those tables have some data organization problems:
    - Duplicates that can refer to different possible acetylation points (a) or
      different PSMs with different acetylation probabilities (b):
      (a) Sequence Window    Modified Sequence              Acetyl (K) Probabilities
          AGGKAGKDSGKAK      _(ac)AGGKAGK(ac)DSGK(ac)AK_    AGGKAGK(1)DSGK(0.996)AK(0.004)
          AGKDSGKAKTKAV      _(ac)AGGKAGK(ac)DSGK(ac)AK_    AGGKAGK(1)DSGK(0.996)AK(0.004)
      (b) Sequence Window    Modified Sequence              Acetyl (K) Probabilities
          TYCDLGKAAKDVF      _(ac)CNTPTYCDLGK(ac)AAK_       CNTPTYCDLGK(0.781)AAK(0.219)
          DLGKAAKDVFNKG      _(ac)CNTPTYCDLGK(ac)AAK_       CNTPTYCDLGK(0.5)AAK(0.5)
    - No prob. for N-term acetylations:
        Sequence Window    Modified Sequence              Acetyl (K) Probabilities
        ELSLLEKSLGLSK      _(ac)AAAAELSLLEK(ac)SLGLSK_    AAAAELSLLEK(1)SLGLSK
    - When all prob.<0.9, the first option/s of the higest is/are alwais choosen:
        Sequence Window    Modified Sequence                Acetyl (K) Probabilities
        ASFDKAKLKKTET      _(ac)ADKPDM(ox)GEIASFDK(ac)AK_   ADKPDMGEIASFDK(0.5)AK(0.5)
    """
    _DEFAULT_REPO_DATA = CHOUDHARY
    
    ASCORE_THRESHOLD = 0.9
    PTMID2THRESHOLD = {ACETYLATION['id']: ASCORE_THRESHOLD}
    CELLLINE = JURKAT
    
    def _get_repo_file_version(self):
        return '2009'
    
    def _get_pos2psmmods4row(self, row):
        """
        Get PSM Modifications for a `row` dictionary from a TSV file.
        """
        return super(Choudhary09AcFetcher, self)._get_pos2psmmods4row(row,
                                                   'Modified Sequence',
                                                   'Acetyl (K) Probabilities',
                                                   'ac')

    def _get_spectrum4row(self, row, experiment):
        """
        Get the Spectrum for a `row` dictionary from TSV data.
        """
        charge = int( row['Charge'] )
        return dbm.Spectrum(charge = charge,
                            has_mzs_intensities = False,
                            parent_mass = float( row['m/z'] ) * charge,
                            experiment = experiment)

    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Choudhary et al. 2009 supplementary
        information tables, along with their associated modifications.
        This method tries to solve data organization problems found in source
        TSV files by treating each row of the tables as a PSM not as a MS
        Peptide.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        for tsv_filen in self.tsv_namelist(): #Process only TSV files:
            exp_subid = os.path.splitext(os.path.basename(tsv_filen))[0].split('_')[-1].upper() #'Jurkat_A549_MS275.tsv' -> 'MS275'
            ev_linkedid_key = "Choudhary09-" + exp_subid
            experiment = self.expfetcher.get_experiment("CHO09-" + exp_subid)
            jurkat_ratio_row = "Ratio Jurkat {0}/Ctrl.".format(exp_subid)
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    # Filter-Out rows without Jurkat results:
                    if not row[jurkat_ratio_row].strip():
                        continue
                    #
                    evidence = dbm.Evidence( type='IDA', #'Inferred from Direct Assay'
                                             linked_ids={ev_linkedid_key: 
                                                         row['Modified Sequence']} )
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row)
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their modifications:
                    prots_from_source = [ ac for ac in row['Uniprot'].split(';')
                                          if ac.strip() ]
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    # Get the void Spectrum:
                    spectrum = self._get_spectrum4row(row, experiment)
                    # Create the PSM:
                    yield dbm.PSM( spectrum=spectrum, mspeptide=mspep,
                                   experiment=experiment,
                                   _db_modifications=psmmods,
                                   scores={ 'mascot_score': float(row['Mascot Score']) } )


class Svinkina15AcFetcher(TSVMSDataFetcher):
    """
    Class to get MS peptides and Acetylation information from Svinkina et al.
    2015 supplementary information tables as TSV files.

    X-NOTE: Those tables have some data organization problems:
    - Duplicates that can refer to different possible acetylation points (a) or
      different PSMs with different acetylation probabilities (b):
      (a) Sequence Window    Modified Sequence              Acetyl (K) Probabilities
          AGGKAGKDSGKAK      _(ac)AGGKAGK(ac)DSGK(ac)AK_    AGGKAGK(1)DSGK(0.996)AK(0.004)
          AGKDSGKAKTKAV      _(ac)AGGKAGK(ac)DSGK(ac)AK_    AGGKAGK(1)DSGK(0.996)AK(0.004)
      (b) Sequence Window    Modified Sequence              Acetyl (K) Probabilities
          TYCDLGKAAKDVF      _(ac)CNTPTYCDLGK(ac)AAK_       CNTPTYCDLGK(0.781)AAK(0.219)
          DLGKAAKDVFNKG      _(ac)CNTPTYCDLGK(ac)AAK_       CNTPTYCDLGK(0.5)AAK(0.5)
    - No prob. for N-term acetylations:
        Sequence Window    Modified Sequence              Acetyl (K) Probabilities
        ELSLLEKSLGLSK      _(ac)AAAAELSLLEK(ac)SLGLSK_    AAAAELSLLEK(1)SLGLSK
    - When all prob.<0.9, the first option/s of the higest is/are alwais choosen:
        Sequence Window    Modified Sequence                Acetyl (K) Probabilities
        ASFDKAKLKKTET      _(ac)ADKPDM(ox)GEIASFDK(ac)AK_   ADKPDMGEIASFDK(0.5)AK(0.5)
    """
    _DEFAULT_REPO_DATA = SVINKINA
    
    ASCORE_THRESHOLD = 0.75
    PTMID2THRESHOLD = {ACETYLATION['id']: ASCORE_THRESHOLD}
    CELLLINE = JURKAT_E6_1
    EXPSUBID2DESC = {'bRPFRAC': 'SILAC experiment with basic Reverse Phase fractionation', 
                     'NOFRAC': 'SILAC experiment without fractionation'}

    def _get_repo_file_version(self):
        return '2015'
    
    def _get_pos2psmmods4row(self, row):
        """
        Get PSM Modifications for a `row` dictionary from a TSV file.
        """
        return super(Svinkina15AcFetcher, self)._get_pos2psmmods4row(row,
                                                   'Modified sequence',
                                                   'Acetyl (K) Probabilities',
                                                   'ac')

    def _get_spectrum4row(self, row, experiment):
        """
        Get the Spectrum for a `row` dictionary from TSV data.
        """
        charge = int( row['Charge'] )
        parent_mass = float( row['Mass'] )
        ms2_scan = int( row['MS/MS Scan Number'] )
        return dbm.Spectrum(ms_n = 2,
                            charge = charge,
                            has_mzs_intensities = False,
                            raw_file = row['Raw file'], 
                            scan_i = ms2_scan,
                            scan_f = ms2_scan, 
                            parent_mass = parent_mass,
                            experiment = experiment)

    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Svinkina et al. 2015 supplementary
        information tables, along with their associated modifications.
        This method tries to solve data organization problems found in source
        TSV files by treating each row of the tables as a PSM not as a MS
        Peptide.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        min_peplength = self.min_peplength
        for tsv_filen in self.tsv_namelist(): #Process only TSV files:
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    exp_replicate = row['Experiment']
                    exp_bioreplicate = exp_replicate[-1]
                    if exp_replicate.startswith('nfl'):
                        exp_id = "SVI15-NOFRAC-" + exp_bioreplicate
                    else:
                        exp_id = "SVI15-bRPFRAC-" + exp_bioreplicate
                    experiment = self.expfetcher.get_experiment(exp_id)
                    evidence = dbm.Evidence( type='IDA', #'Inferred from Direct Assay'
                                             linked_ids={ 'Svinkina15-ID': row['id'], 
                                                          exp_id: row['Modified sequence'] },
                                             extradata={'bio_replicate': exp_bioreplicate} )
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row)
                    if len(seq) < min_peplength:
                        continue # Skip/filter-out short peptides. 
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their modifications:
                    prots_from_source = [ ac for ac in row['Proteins'].split(';')
                                          if ac.strip() ]
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    # Get the void Spectrum:
                    spectrum = self._get_spectrum4row(row, experiment)
                    # Create the PSM:
                    yield dbm.PSM( spectrum=spectrum, mspeptide=mspep,
                                   experiment=experiment,
                                   _db_modifications=psmmods,
                                   scores={ 'maxquant_score': float(row['Score']) } )


class Udeshi13UbFetcher(TSVMSDataFetcher):
    """
    Class to get MS peptides and Ubiquitination information from Udeshi et al.
    2013 supplementary information tables as TSV files.

    X-NOTE: Those tables have some data organization problems.
    """
    _DEFAULT_REPO_DATA = UDESHI
    
    ASCORE_THRESHOLD = 0.75
    PTMID2THRESHOLD = {UBIQUITINATION['id']: ASCORE_THRESHOLD}
    CELLLINE = JURKAT_E6_1

    def _get_repo_file_version(self):
        return '2013'
    
    def _get_pos2psmmods4row(self, row):
        """
        Get PSM Modifications for a `row` dictionary from a TSV file.
        """
        return super(Udeshi13UbFetcher, self)._get_pos2psmmods4row(row,
                                                    'Modified sequence',
                                                    'GlyGly (K) Probabilities',
                                                    'gl')

    def _get_spectrum4row(self, row, experiment):
        """
        Get the Spectrum for a `row` dictionary from TSV data.
        """
        charge = int( row['Charge'] )
        scan_n = int( row['Best localization scan number'] )
        return dbm.Spectrum(charge = charge,
                            has_mzs_intensities = False,
                            parent_mass = float( row['MaxQuant Light m/z'] ) * charge,
                            raw_file = row['Best localization raw file'],
                            scan_i = scan_n,
                            scan_f = scan_n,
                            experiment = experiment)
    
    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Udeshi et al. 2013 supplementary
        information tables, along with their associated modifications.
        This method tries to solve data organization problems found in source
        TSV files by treating each row of the tables as a PSM not as a MS
        Peptide.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        min_peplength = self.min_peplength
        for tsv_filen in self.tsv_namelist(): #Process only TSV files:
            experiment = self.expfetcher.get_experiment('UDE13')
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    evidence = dbm.Evidence(type='IDA', #'Inferred from Direct Assay'
                                            linked_ids={'Udeshi13': row['Modified sequence']} )
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row)
                    if len(seq) < min_peplength:
                        continue # Skip/filter-out short peptides. 
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their modifications:
                    prots_from_source = [ ac for ac in row['Proteins'].split(';')
                                          if ac.strip() ]
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    # Get the void Spectrum for the 'Best localization' data:
                    spectrum = self._get_spectrum4row(row, experiment)
                    # Create the PSM:
                    # -Get the scores from the 3 replicates:
                    ublocprob2score = defaultdict(list)
                    for replicate in range(1,3):
                        try:
                            ublocprob = float( row['Localization prob Rep0%d'%replicate] )
                            score = float( row['Score Rep0%d'%replicate] )
                        except ValueError as _:
                            ublocprob = score = 0.0
                        ublocprob2score[ublocprob].append(score)
                    # -Get the score of the 'best localization' replicate:
                    max_ublocprob = max( ublocprob2score.keys() )
                    psm_score = max( ublocprob2score[max_ublocprob] )
                    # -Create and yield the PSM:
                    yield dbm.PSM( spectrum=spectrum, mspeptide=mspep, 
                                   experiment=experiment,
                                   _db_modifications=psmmods,
                                   scores={'maxquant_score': psm_score} )


class PhosTyrFetcher(TSVMSDataFetcher):
    """
    Subclass of :class:`TSVMSDataFetcher` designed to get MS peptides and
    tyrosine phosphorylation (pTyr) information from papers tables as TSV files.
    """
    ASCORE_THRESHOLD = None
    PTMID2THRESHOLD = {PHOSPHORYLATION['id']: ASCORE_THRESHOLD}
    CELLLINE = JURKAT
    
    EXP_ID = '' # Experiment ID.
    SEQWMODS_FIELD = '' # Name of the Field, in the TSV files, containing the identified sequence with modifications/phosphorylations.
    PHOS_CHAR = 'p' # Character used in field to mark a phosphorylated AA.
    PHOSCHAR_POS_SHIFT = 1 # Value added to the position of the PHOS_CHAR marker to get the real position of the phosphorylation: 0 when the phosphorylation is located before the PHOS_CHAR or 1 if it's located after it.
    PROTID_FIELD = '' # Name of the Field, in the TSV files, containing the protein IDs.
    PROTID_TYPE = "NCBI GI" # Type of the old protein IDs (NCBI GI by default) in the TSV files.
    PROTID2AC_FILE = 'conversion.tsv.ncbi2ac' # File name of a file (by default a TSV file) containing information to convert from some obsolete Protein ID to an UniProtKB ACcession number.  
    
    _protid2ac = None # {old_protein_id: UniProtKB_AC}
    
    def _get_protid2ac4iofile(self, io_file):
        """
        This default implementation generates a conversion dictionary from NCBI
        GI to UniProtKB ACcession numbers, using files generated by UniProt ID
        mapping webapp (http://www.uniprot.org/uploadlists/).
        Overwrite in subclasses as needed.
        
        :param file io_file: a file object to a TSV file generated by UniProt
        ID mapping web-app.
        
        :return dict : {NCBI_GI: UniProtKB_AC}
        """
        protid2ac = dict()
        csvr = csv.reader(io_file, delimiter='\t')
        _ = csvr.next() #Discard first row containing field names.
        for row in csvr:
            prot_ac = row[2]
            for prot_id in row[0].split(','):
                prot_id = prot_id.strip()
                protid2ac[prot_id] = prot_ac
        return protid2ac
        
    @property
    def protid2ac(self):
        """
        Memoized, lazy-loading, read-only property.
        A conversion dictionary from different old protein IDs to UniProtKB 
        ACcession numbers: {old_protein_id: UniProtKB_AC}
        """
        if self._protid2ac is None: # :attr:`_protid2ac` is not initialized:
            # Create the conversion dictionary from a NCBI GI -> UniProtKB AC, 
            # if the corresponding file (:attr:`PROTID2AC_FILE`) is available 
            # inside the zipped :attr:`repo_file`:
            if self.PROTID2AC_FILE and self.repo_file:
                with self.repo_file.open(self.PROTID2AC_FILE, 'rU') as io_file:
                    self._protid2ac = self._get_protid2ac4iofile(io_file)
            else:
                self._protid2ac = dict()
        #
        return self._protid2ac
    
    def _get_pos2psmmods4row(self, row):
        """
        Get PSM Modifications (phosphorylations) for a `row` dictionary from a 
        TSV file.
        Overwrites :super-class:`TSVMSDataFetcher` method.
        
        :param dict row: a `row` dictionary from a TSV file.

        :return tuple : { position: {PSMModification(), ...} }, clean aminoacid
        sequence.
        """
        # Get phosphorylations, "searching" for :attr:`PHOS_CHAR` in the `row` 
        # field/key corresponding to :attr:`SEQWMODS_FIELD`:
        seq_frags = row[self.SEQWMODS_FIELD].split(self.PHOS_CHAR)
        pos2psmmods = defaultdict(set)
        seqpos = 0
        phoschar_pos_shift = self.PHOSCHAR_POS_SHIFT
        for seq_frag in seq_frags[:-1]:
            seqpos += len(seq_frag)
            modpos = seqpos + phoschar_pos_shift
            psmmod = dbm.PSMModification(mod_type=self.phosphorylation,
                                         position=modpos, 
                                         significant=True)
            pos2psmmods[modpos].add(psmmod)
        return pos2psmmods, "".join(seq_frags)
    
    def _get_protsfromsource4row(self, row):
        """
        :param dict row: a `row` dictionary from a TSV file.
        
        :return list : 
        """
        prot_id = row[self.PROTID_FIELD].strip()
        return [ self.protid2ac.get( prot_id, 
                                     "%s (%s)"%(prot_id, self.PROTID_TYPE) ) ]
    
    def _get_psmscores4row(self, row):
        """
        Get the PSM score/s.
        :caution: This default implementation only returns None; so, if possible, 
        overwrite it in sub-classes.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return : None.
        """
        return None
    
    def iter_all_psms(self, filterout_row=None, preprocess_row=None):
        """
        Generic method to iteratively get all PSMs from pTry paper tables, along 
        with their associated modifications. This method treats each row of the 
        tables as a PSM not as a MS Peptide.
        Implements :super-class:`TSVMSDataFetcher` method.
        
        :param func or None filterout_row: a callable that gets the current row
        and returns True if the row should be skipped/filtered-out. Defaults to
        None (no filtering out).
        :param func or None preprocess_row: a callable that gets the current row, 
        pre-processes it, and returns a list of modified rows. Defaults to None 
        (no pre-processing).
        
        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        min_peplength = self.min_peplength
        experiment = self.expfetcher.get_experiment(self.EXP_ID)
        exp_name = experiment.name
        seqwmods_field = self.SEQWMODS_FIELD
        #
        for tsv_filen in self.tsv_namelist(): #Process only TSV files:
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    # Filter-out/skip rows:
                    if filterout_row and filterout_row(row):
                        continue #Filter-out/skip.
                    # Evidence linked to original sequence with modifications:
                    evidence = dbm.Evidence(type='IDA', #'Inferred from Direct Assay'
                                            linked_ids={exp_name: row[seqwmods_field]} )
                    # Pre-process row data to get pre-processed rows:
                    if preprocess_row:
                        prepro_rows = preprocess_row(row)
                    else:
                        prepro_rows = [row]
                    # Process every possible pre-processed row to get PSMs:
                    for pprow in prepro_rows:
                        # - Get PSM Modifications and a clean sequence:
                        pos2psmmods, seq = self._get_pos2psmmods4row(pprow)
                        if len(seq) < min_peplength:
                            continue # Skip/filter-out short peptides. 
                        psmmods = {mod for mods in pos2psmmods.values()
                                       for mod in mods}
                        # - Get the MS Peptide with their modifications:
                        prots_from_source = self._get_protsfromsource4row(pprow)
                        mspep = self._get_mspeptide4data(pprow, seq, 
                                                         pos2psmmods, psmmods, 
                                                         experiment, evidence, 
                                                         prots_from_source)
                        # - Get the Spectrum:
                        spectrum = self._get_spectrum4row(pprow, experiment)
                        # - Get the PSM score/s:
                        scores = self._get_psmscores4row(pprow)
                        # - Create and yield the PSM:
                        yield dbm.PSM(spectrum=spectrum, mspeptide=mspep, 
                                      experiment=experiment,
                                      _db_modifications=psmmods,
                                      scores=scores)


class Watts94PhosFetcher(PhosTyrFetcher):
    """
    Class to get MS peptides and phosphorylation information from Watts et al.
    1994 table 1 as TSV files.
    """
    _DEFAULT_REPO_DATA = WATTS
    
    EXP_ID = 'WAT94' # Experiment ID.
    SEQWMODS_FIELD = 'Inferred peptide sequence (c)' # Name of the Field, in the TSV files, containing the identified sequence with modifications/phosphorylations.
    
    def _get_repo_file_version(self):
        """
        Overwrites :superclass:`RepoFileFetcherMixin` method.
        """
        return '1994'
    
    def _get_protsfromsource4row(self, row):
        """
        Overwrites :super-class:`PhosTyrFetcher` method.
        """
        return ['P43403'] # All peptides in Watts et al. 1994 belong to Tyrosine-protein kinase ZAP-70 (http://www.uniprot.org/uniprot/P43403)
    
    def _get_spectrum4row(self, row, experiment):
        """
        Get some Spectrum data from a `row` dictionary from a TSV file.
        Implements :super-class:`TSVMSDataFetcher` method.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return Spectrum : a new model :class:`Spectrum` instance.
        """
        parent_mass = float( row['Observed phosphopeptide mass (a)'].partition(' ')[0] )
        return dbm.Spectrum(has_mzs_intensities = False,
                            parent_mass = parent_mass, 
                            experiment = experiment)
    
    def _filterout_row(self, row):
        """
        Mark ambiguous sequences to filter-out/skip them.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return bool : True if row must be filtered-out/skipped, otherwise False.
        """
        if ' or ' in row[self.SEQWMODS_FIELD]:
            return True
        else:
            return False
    
    def _preprocess_row(self, row):
        """
        Pre-process row dictionary "in-situ" to clean sequences.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return list of dict : a list containing the only pre-processed row
        dictionary.
        """
        original_seq = row[self.SEQWMODS_FIELD]
        row[self.SEQWMODS_FIELD] = original_seq.split(' ')[1] # '(R) DpYVR (Q)' -> 'DpYVR'
        return [row]
    
    def iter_all_psms(self):
        """
        Overwrites :super-class:`PhosTyrFetcher` method, to avoid yielding
        ambiguous sequences and clean the remaining ones.
        """
        return super(Watts94PhosFetcher, self).iter_all_psms(self._filterout_row, 
                                                             self._preprocess_row)


class Salomon03PhosFetcher(PhosTyrFetcher):
    """
    Class to get MS peptides and phosphorylation information from Salomon et al.
    2003 table 1 as TSV files.
    """
    _DEFAULT_REPO_DATA = SALOMON
    
    CELLLINE = JURKAT_E6_1
        
    EXP_ID = 'SAL03' # Experiment ID.
    SEQWMODS_FIELD = 'Peptide sequence' # Name of the Field, in the TSV files, containing the identified sequence with modifications/phosphorylations.
    PROTID_FIELD = 'NCBI GI no.' # Name of the Field, in the TSV files, containing the protein IDs.
    PROTID_TYPE = "NCBI GI" # Type of the old protein IDs (NCBI GI by default) in the TSV files.
    PROTID2AC_FILE = 'Salomon 2003 Table 1 pTyr - NCBI GI to ACcession.tsv.ncbi2ac' # File name of a file (by default a TSV file) containing information to convert from some obsolete Protein ID to an UniProtKB ACcession number.  
    
    def _get_repo_file_version(self):
        """
        Overwrites :superclass:`RepoFileFetcherMixin` method.
        """
        return '2003'
    
    def _get_spectrum4row(self, row, experiment):
        """
        Generates a minimum Spectrum object with almost no data (nothing useful 
        is available in the paper supplied tables).
        Implements :super-class:`TSVMSDataFetcher` method.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return Spectrum : a new model :class:`Spectrum` instance.
        """
        return dbm.Spectrum(has_mzs_intensities = False,
                            experiment = experiment)


class Ficarro05PhosFetcher(PhosTyrFetcher):
    """
    Class to get MS peptides and phosphorylation information from Ficarro et al.
    2005 table 1 as TSV files.
    """
    _DEFAULT_REPO_DATA = FICARRO
    
    CELLLINE = JURKAT_E6_1
        
    EXP_ID = 'FIC05' # Experiment ID.
    SEQWMODS_FIELD = 'Sequence (a)' # Name of the Field, in the TSV files, containing the identified sequence with modifications/phosphorylations.
    PROTID_FIELD = 'Accession (NCBI GI no spaces)' # Name of the Field, in the TSV files, containing the protein IDs.
    PROTID_TYPE = "NCBI GI" # Type of the old protein IDs (NCBI GI by default) in the TSV files.
    PROTID2AC_FILE = 'Ficarro 2005 Table 3 pTyr - NCBI GI to ACcession.tsv.ncbi2ac' # File name of a file (by default a TSV file) containing information to convert from some obsolete Protein ID to an UniProtKB ACcession number.  
    
    def _get_repo_file_version(self):
        """
        Overwrites :superclass:`RepoFileFetcherMixin` method.
        """
        return '2005'
    
    def _get_spectrum4row(self, row, experiment):
        """
        Generates a minimum Spectrum object with almost no data.
        Implements :super-class:`TSVMSDataFetcher` method.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return Spectrum : a new model :class:`Spectrum` instance.
        """
        return dbm.Spectrum(has_mzs_intensities = False, 
                            rt = int(round(float( row['RT'] ))), 
                            experiment = experiment)
    
    def _filterout_row(self, row):
        """
        Mark ambiguous sequences to filter-out/skip them.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return bool : True if row must be filtered-out/skipped, otherwise False.
        """
        if '(' in row[self.SEQWMODS_FIELD]:
            return True
        else:
            return False
    
    def _preprocess_row(self, row):
        """
        Pre-process row dictionary to generate multiple PSM rows from it.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return list of dict : a list containing the needed pre-processed row
        dictionaries.
        """
        # Get the replicates IDs in the row:
        replicates = list()
        if row['MS/MS, 1 (f)'] == '*':
            replicates.append('1')
        if row['MS/MS, 2 (f)'] == '*':
            replicates.append('2')
        # Generate a new pre-processed row for each replicate:
        prepro_rows = list() #Pre-processed rows.
        for replicate in replicates:
            prepro_row = row.copy()
            prepro_row['RT'] = row["RT analysis %s (c)"%replicate]
            prepro_row['Area'] = row["Area analysis %s (d)"%replicate]
            prepro_rows.append(prepro_row)
        return prepro_rows
    
    def iter_all_psms(self):
        """
        Overwrites :super-class:`PhosTyrFetcher` method, to avoid yielding
        ambiguous sequences and generate all the needed PSM rows for each TSV
        row.
        """
        return super(Ficarro05PhosFetcher, self).iter_all_psms(self._filterout_row, 
                                                               self._preprocess_row)


class Tao05PhosFetcher(PhosTyrFetcher):
    """
    Class to get MS peptides and phosphorylation information from Tao et al.
    2005 supplementary information table 1 as TSV files.
    """
    _DEFAULT_REPO_DATA = TAO
    
    CELLLINE = JURKAT
        
    EXP_ID = 'TAO05' # Experiment ID.
    SEQWMODS_FIELD = 'Peptide sequence ' # Name of the Field, in the TSV files, containing the identified sequence with modifications/phosphorylations.
    PHOS_CHAR = '*' # Character used in field to mark a posphorylated AA.
    PHOSCHAR_POS_SHIFT = 0 # Value added to the position of the PHOS_CHAR marker to get the real position of the phosphorylation: 0 when the phosphorylation is located before the PHOS_CHAR or 1 if it's located after it.
    PROTID_FIELD = 'IPI Entry ' # Name of the Field, in the TSV files, containing the protein IDs.
    PROTID_TYPE = "IPI" # Type of the old protein IDs in the TSV files.
    PROTID2AC_FILE = 'Tao 2005 Supplementary Table 1 pTyr - IPI to ACcession.csv.ipi2ac' # File name of a file containing information to convert from some obsolete Protein ID to an UniProtKB ACcession number.  
    
    def _get_protid2ac4iofile(self, io_file):
        """
        Generates a conversion dictionary from IPI to UniProtKB ACcession 
        numbers, using files generated by the Protein Identifier Cross-Reference 
        service (https://www.ebi.ac.uk/Tools/picr/).
        Overwrites :super-class:`PhosTyrFetcher` method
        
        :param file io_file: a file object to a CSV file generated by the 
        Protein Identifier Cross-Reference service.
        
        :return dict : {IPI: UniProtKB_AC}
        """
        # Read data from CSV file and store it in a temporal multi-level 
        # dictionary:
        csvdr = csv.DictReader(io_file, delimiter=',')
        protid2db2acs = defaultdict( lambda: defaultdict(set) ) # {IPI: {'SWISSPROT': {AC, ...}, 'TREMBL': {AC, ...}} }
        for row in csvdr:
            if row['Status'] == 'identical': # Only identical records are collected:
                prot_id = row['Input Accession'].strip()
                prot_ac = row['Mapped Accession'].strip()
                database = row['Database'].strip()
                protid2db2acs[prot_id][database].add(prot_ac)
        # Select the best UniProtKB_AC for every IPI from the temporal 
        # multi-level dictionary to create the final conversion dictionary:
        protid2ac = dict() # {IPI: UniProtKB_AC}
        for prot_id, db2acs in protid2db2acs.items():
            best_acs = db2acs.get( 'SWISSPROT', db2acs['TREMBL'] ) #Get the best available ACs for the current IPI (so, prefers SWISSPROT ACs over TREMBL ones).
            selected_ac = best_acs.pop()
            if '.' in selected_ac or best_acs:
                selected_ac = selected_ac[:-2] # 'P00001.2' or 'P00001-3' -> 'P00001'
            protid2ac[prot_id] = selected_ac
        return protid2ac
    
    def _get_repo_file_version(self):
        """
        Overwrites :superclass:`RepoFileFetcherMixin` method.
        """
        return '2005'
    
    def _get_psmscores4row(self, row):
        """
        Get the PSM scores from row fields 'Prophet ' and 'Xcorr ' (if data is 
        available for them).
        Overwrites :super-class:`PhosTyrFetcher` method.
                
        :param dict row: a `row` dictionary from a TSV file.
        
        :return dict : {score name: score value}.
        """
        scores = dict()
        for row_field, score_name in ( ('Prophet ', 'peptide-prophet_probability'), 
                                       ('Xcorr ', 'sequest_xcorr') ):
            try:
                score = float( row[row_field].strip() )
            except ValueError as _:
                continue # A void or invalid field value (skip it).
            scores[score_name] = score
        return scores
    
    def _get_spectrum4row(self, row, experiment):
        """
        Generates a minimum Spectrum object with almost no data.
        Implements :super-class:`TSVMSDataFetcher` method.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return Spectrum : a new model :class:`Spectrum` instance.
        """
        return dbm.Spectrum(has_mzs_intensities = False, 
                            experiment = experiment)
    
    def _preprocess_row(self, row):
        """
        Pre-process row dictionary "in-situ" to clean sequences.
        
        :param dict row: a `row` dictionary from a TSV file.
        
        :return list of dict : a list containing the only pre-processed row
        dictionary.
        """
        original_seq = row[self.SEQWMODS_FIELD]
        row[self.SEQWMODS_FIELD] = original_seq.split('.')[1] # 'R.DY*VR.Q' -> 'DY*VR'
        return [row]
    
    def iter_all_psms(self):
        """
        Process rows in TSV files to obtain all PSMs and associated data. 
        Uses :method:`_preprocess_row` to clean the sequence in each TSV row.
        Overwrites :super-class:`PhosTyrFetcher` method.
        """
        return super(Tao05PhosFetcher, self).iter_all_psms(None, 
                                                           self._preprocess_row)


class Mayya09PhosFetcher(TSVMSDataFetcher):
    """
    Class to get MS peptides and phosphorylation information from Mayya et al.
    2009 supplementary information tables as TSV files.
    """
    _DEFAULT_REPO_DATA = MAYYA
    
    ASCORE_THRESHOLD = 13.0
    PTMID2THRESHOLD = {PHOSPHORYLATION['id']: ASCORE_THRESHOLD}
    CELLLINE = JURKAT

    def _get_repo_file_version(self):
        return '2009'
    
    def _get_pos2psmmods4row(self, row):
        """
        Get PSM Modifications (phosphorylations) for a `row` dictionary from a 
        TSV file.
        
        :param dict row: a `row` dictionary from a TSV file.
        """
        # Get phosphorylations, "searching" for '#' in the'peptide_sequence' 
        # field/key:
        seq_frags = row['peptide_sequence'].split('#')
        pos2psmmods = defaultdict(set)
        pos = 0
        for index, seq_frag in enumerate(seq_frags[:-1], start=1):
            pos += len(seq_frag)
            ascore = row[ "ascore_%d"%(index) ] #Get ascore from 'ascore_X' field:
            try:
                ascore = float(ascore)
            except ValueError as _: #Ascore value is N/A or not present (X-NOTE from original table notes: Sites with no Ascore entry or with a 'N/A' entry did not need Ascore analysis as they were the only phosphorylable residue(s) in the respective phosphopeptides.)
                ascore = 1000 #As in LymPHOS and LymPHOS2.
            psmmod = dbm.PSMModification( mod_type=self.phosphorylation,
                                          position=pos,
                                          ascore=ascore, 
                                          significant=(ascore>=self.ASCORE_THRESHOLD) )
            pos2psmmods[pos].add(psmmod)
        return pos2psmmods, "".join(seq_frags)

    def _get_spectrum4row(self, row, experiment):
        """
        Get the Spectrum for a `row` dictionary from TSV data.
        """
        scan_n = int( row['scan#'] )
        return dbm.Spectrum(charge = int( row['chg'] ),
                            has_mzs_intensities = False,
                            parent_mass = float( row['mass'] ),
                            raw_file = row['path'],
                            scan_i = scan_n,
                            scan_f = scan_n,
                            experiment = experiment)

    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Mayya et al. 2009 supplementary
        information tables, along with their associated modifications.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        min_peplength = self.min_peplength
        experiment = self.expfetcher.get_experiment('MAY09')
        #
        for tsv_filen in self.tsv_namelist(): #Process only TSV files:
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    evidence = dbm.Evidence(type='IDA', #'Inferred from Direct Assay'
                                            linked_ids={'Mayya09': row['peptide_sequence']} )
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row)
                    if len(seq) < min_peplength:
                        continue # Skip/filter-out short peptides. 
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their modifications:
                    prots_from_source = [ row['Uniprot ID'].upper() ] if row['Uniprot ID'].strip() else []
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    # Get the void Spectrum:
                    spectrum = self._get_spectrum4row(row, experiment)
                    # Create the PSM:
                    scores = { 'sequest_xcorr': float( row['Xcorr'] ) }
                    deltacn = row["dCn'"]
                    if '*' in deltacn:
                        deltacn_type = 'sequest_villen07-deltacn'
                        deltacn = deltacn.strip('*')
                    else:
                        deltacn_type = 'sequest_deltacn'
                    try:
                        scores[deltacn_type] = float(deltacn)
                    except ValueError as _:
                        pass
                    try:
                        scores['peptide-prophet_probability'] = float( 
                                          row['Peptide-Prophet Probability'] )
                    except ValueError as _:
                        pass
                    yield dbm.PSM(spectrum=spectrum, mspeptide=mspep, #Create and yield the PSM
                                  experiment=experiment,
                                  _db_modifications=psmmods,
                                  scores=scores)


class Nguyen16PhosFetcher(TSVMSDataFetcher):
    """
    Class to get MS peptides and phosphorylation information from Nguyen et al.
    2015 supplementary information tables as TSV files (Integrator new format).
    """
    _DEFAULT_REPO_DATA = NGUYEN
    
    ASCORE_THRESHOLD = 19.0
    PTMID2THRESHOLD = {PHOSPHORYLATION['id']: ASCORE_THRESHOLD}
    CELLLINE = JURKAT_E6_1
    
    def __init__(self, repo_file, db_session=None, caching=True,
                 cache_limit=100000, experiments=EXPERIMENTS_FILE, 
                 min_peplength=MIN_PEPLENGTH):
        """
        :param object repo_file: zip file containing supplementary information 
        tables as TSVs. It should be either a string containing the path to the 
        file, or a file-like object.
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to fetch stored data.
        :param bool caching: indicates if the converter should use a caching
        strategy form some created objects. Defaults to True.
        :param int cache_limit: maximum number of objects allowed in the cache,
        or None (no limit). Defaults to 100000 (try to avoid high memory use).
        :param object experiments: a :class:`ExperimentFetcher` instance, or a
        either a string containing the path to a file, or a file-like object, 
        of a zip file containing Experiments and SupraExperiments information 
        as TSV files. Defaults to global ``EXPERIMENTS_FILE``.
        :param int min_peplength: the minimum length a MS Peptide should have 
        (shorter ones will not be created/imported/returned). Defaults to the 
        value of the global `MIN_PEPLENGTH`.
        """
        super(Nguyen16PhosFetcher, self).__init__(repo_file,
                                                  db_session=db_session,
                                                  caching=caching,
                                                  cache_limit=cache_limit, 
                                                  experiments=experiments, 
                                                  min_peplength=min_peplength)
        # Defaults for Coded MODification -> MODification ATTRibute:
        self.CMOD2MODATTR = {'121': 'ubiquitination',
                             '1': 'acetylation',
                             '35': 'oxidation',
                             '21' : 'phosphorylation',
                             '23' : 'phosphorylation', #X_NOTE: This is really a dehidratation
                             }
    
    def _get_repo_file_version(self):
        return '2016'
    
    def _get_pos2psmmods4row( self, row, main_modifications=('21', '23') ):
        """
        Get Positions -> PSM Modifications for a `row` dictionary from a TSV file.

        :param dict row: a row dictionary from a TSV file.
        :param iterable main_modifications: the studied coded modifications in 
        the article source data. Defaults to ('21', '23').

        :return tuple : { position: {PSMModification(), ...} }, clean aminoacid
        sequence.
        """
        # Get modified sequence from 'ascore revised peptide' or 'consensus modified peptide':
        modified_seq = row['ascore revised peptide'].strip('.')
        if not modified_seq:
            modified_seq = row['consensus modified peptide']
        # Get PSM Modifications grouped by position, and clean sequence, from
        # `modified_sequence` field:
        clean_seq, pos2c_mods = ppf.split_seq_and_things(modified_seq) #Get clean sequence (ADKPDMGEIASFDKAK) and coded modifications grouped by position.
        pos2psmmods = defaultdict(set)
        ascore_index = 0
        for pos, c_mods in pos2c_mods.items(): #Get PSM Modifications grouped by position from coded modifications and modifications probabilities grouped by position:
            for c_mod in c_mods:
                try:
                    mod_type = getattr( self, self.CMOD2MODATTR[c_mod] ) #coded modification -> self.modification_type
                except KeyError as _:
                    pass
                else: 
                    psmmod = dbm.PSMModification(mod_type=mod_type, position=pos)
                    if c_mod in main_modifications: # Only the studied modifications in the article whould have ascore information:
                        ascore_index += 1
                        try:
                            ascore = float( row[ "ascore%d"%(ascore_index) ] ) #Get ascore from 'ascoreX' field:
                        except ValueError as _:
                            ascore = 0.0 #No Q-Ascore value.
                        psmmod.ascore = ascore
                        psmmod.significant = (ascore>=self.ASCORE_THRESHOLD)
                    pos2psmmods[pos].add(psmmod)
        #
        return pos2psmmods, clean_seq
    
    def _get_mean_mass4row(self, row):
        """
        Get the Spectrum mean mass from different search engines for a `row`
        dictionary from TSV data.
        """
        masses = list()
        if row['Se'] == '1':
            masses.append(float( row['Sequest actual_mass'] ))
        if row['Om'] == '1':
            masses.append(float( row['Omssa calc_neutral_pep_mass'] ))
        if row['Ea'] == '1':
            masses.append( float( row['EasyProt mz'] ) * float( row['EasyProt z'] ) )            
        return sum(masses) / len(masses)
    
    def _get_spectrum4row(self, row, experiment):
        """
        Get the Spectrum for a `row` dictionary from TSV data.
        """
        return dbm.Spectrum(charge=int(float( row['consensus z'] )),
                            ms_n=int( row['MS'] ),
                            has_mzs_intensities=False,
                            parent_mass=self._get_mean_mass4row(row),
                            raw_file=row['file'] + ".RAW",
                            scan_i=int( row['first scan'] ),
                            scan_f=int( row['last scan'] ),
                            experiment=experiment)

    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Nguyen et al. 2015 supplementary
        information tables, along with their associated modifications.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        supra7_exps = ('201', '202', '203', '209', '210')
        supexp = "7"
        min_peplength = self.min_peplength
        #
        for tsv_filen in self.tsv_namelist(): #Process only TSV files:
            exp_number = os.path.basename(tsv_filen).split('_')[0] #'201_eos_Ab_0_15_30_60_120_1440_Fr00_extended_report.tsv' -> '201'.  #IMPROVE?
            if exp_number not in supra7_exps: #Only process Supra-Experiment 7
                continue
            exp_id = "NGT16-" +  exp_number
            experiment = self.expfetcher.get_experiment(exp_id)
            ev_linkedid_key = "NGT-{0}-{1}".format(supexp, exp_number)
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                # Build headers from first two lines (only for new Integrator TSV output format):
                line1 = io_file.readline().split('\t')
                line2 = io_file.readline().split('\t')
                line1.extend( [''] * ( len(line2) - len(line1) ) )
                headers = map( " ".join, zip(line1, line2) )
                headers = map(str.strip, headers)
                # Read and process data:
                csvdr = csv.DictReader(io_file, fieldnames=headers, delimiter='\t')
                for row in csvdr:
                    # Avoid peptides with no matching proteins or only decoy ones:
                    prots_from_source = [prot_ac for prot_ac 
                                         in row['consensus proteins'].split('|') 
                                         if prot_ac and "decoy" not in prot_ac]
                    if not prots_from_source:
                        continue
                    # Define an 'IDA' evidence linked to this row:
                    ev_linkedid_value = "{0} Scans: {1}-{2}".format( row['file'], 
                                                                     row['first scan'], 
                                                                     row['last scan'] )
                    evidence = dbm.Evidence( type='IDA', #'Inferred from Direct Assay'
                                             linked_ids={ev_linkedid_key: ev_linkedid_value} )
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row)
                    if len(seq) < min_peplength:
                        continue # Skip/filter-out short peptides. 
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their modifications:
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    # Get the void Spectrum data:
                    spectrum = self._get_spectrum4row(row, experiment)
                    # Get PSM scores according to different search engines:
                    scores = dict()
                    try:
                        scores['sequest_xcorr'] = float( row['Sequest xcorr'] )
                    except ValueError as _:
                        pass
                    try:
                        scores['omssa_pvalue'] = float( row['Omssa pvalue'] )
                    except ValueError as _:
                        pass
                    try:
                        scores['easyprot_zscore'] = float( row['EasyProt zscore'] )
                    except ValueError as _:
                        pass
                    # Create the PSM:
                    yield dbm.PSM(spectrum=spectrum, mspeptide=mspep, #Create and yield the PSM
                                  experiment=experiment,
                                  _db_modifications=psmmods,
                                  scores=scores)


class Jouy15PhosFetcher(TSVMSDataFetcher):
    """
    Class to get MS Peptides and Phosphorylations information from Jouy et al.
    2015 supplementary information tables as TSV files.
    """
    _DEFAULT_REPO_DATA = JOUY
    
    ASCORE_THRESHOLD = 0.95
    PTMID2THRESHOLD = {PHOSPHORYLATION['id']: ASCORE_THRESHOLD}
    CELLLINE = JURKAT_E6_1

    def _get_repo_file_version(self):
        return '2015'
    
    def _get_pos2psmmods4row(self, row):
        """
        Get Positions -> PSM Modifications for a `row` dictionary from a TSV (or
        CSV) file.
        
        :param dict row: a row dictionary from a TSV file.
        
        :return tuple : { position: {PSMModification(), ...} }, clean aminoacid
        sequence.
        """
        return super(Jouy15PhosFetcher, self)._get_pos2psmmods4row(row, 
                                                                   'Modified sequence', 
                                                                   'Phospho (STY) Probabilities', 
                                                                   'ph')
    
    def _get_spectrum4row(self, row, experiment, charge, parent_mass, scan_n):
        """
        Get the Spectrum for a `row` dictionary from TSV data.
        """
        return dbm.Spectrum(charge = charge,
                            has_mzs_intensities = False,
                            parent_mass = parent_mass,
                            scan_i = scan_n,
                            scan_f = scan_n,
                            experiment = experiment)
    
    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Jouy et al. 2015 supplementary
        information tables, along with their associated modifications.
        
        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        min_peplength = self.min_peplength
        for tsv_filen in self.tsv_namelist(): #Process only TSV files:
            exp_name = os.path.basename(tsv_filen).split('_')[0]
            exp_id = "JOU15-" + exp_name
            experiment = self.expfetcher.get_experiment(exp_id)
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    evidence = dbm.Evidence(type='IDA', #'Inferred from Direct Assay'
                                            linked_ids={exp_id + "_seqwcmods": row['Modified sequence'], 
                                                        exp_id + "_PsiteID": int( row['id'] ),
                                                        exp_id + "_PeptideIDs": map( int, row['Peptide IDs'].split(';') ), 
                                                        exp_id + "_ModPeptideIDs": map( int, row['Mod, peptide IDs'].split(';') ), 
                                                        exp_id + "_EvidenceIDs": map( int, row['Evidence IDs'].split(';') ), 
                                                        })
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row)
                    if len(seq) < min_peplength:
                        continue # Skip/filter-out short peptides. 
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their modifications:
                    prots_from_source = [ row['Protein'] ]
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    # -Update MS Peptide regulations 
                    #  {experiment ID: { 'ratios':        [ratio0, ratio1, ...], 
                    #                    'ratios_ttests': [ttest0, ttest1, ...] }, ...} :
                    exp_regultaion = mspep.regulation.get( exp_id, dict() )
                    exp_regultaion['ratios'] = exp_regultaion.get( 'ratios', list() ) + [ float( row['Ratio (Activ/Control)'] ) ]
                    exp_regultaion['ratios_ttests'] = exp_regultaion.get( 'ratios_ttests', list() ) + [ float( row['T-test'] ) ]
                    mspep.regulation[exp_id] = exp_regultaion
                    # Create the PSMs:
                    # - Get the scores:
                    scores = { 'maxquant_score': float( row['Score'] ), 
                               'maxquant_PEP': row['PEP'] }
                    # - Get/Calc data for spectra:
                    mz = float( row['m/z'] )
                    charge = int(round(ppf.pep_w_mods_mass(mspep) / mz)) # Infer charge from theoretical mass and experimental m/z. 
                    parent_mass = ppf.get_mass4mz_charge(mz, charge) # Get experimental parent mass.
                    # - Create a PSM for each scan number ('MS/MS IDs' column):
                    for scan_n in map( int, row['MS/MS IDs'].split(';') ):
                        # -Get a new void Spectrum:
                        spectrum = self._get_spectrum4row(row, experiment, 
                                                          charge, parent_mass, 
                                                          scan_n)
                        # -Create a new copy of each PSM Modification:
                        psmmods_copy = {dbm.PSMModification(mod_type=mod.mod_type, 
                                                            position=mod.position, 
                                                            ascore=mod.ascore, 
                                                            significant=mod.significant)
                                        for mod in psmmods}
                        # -Create and yield a PSMs:
                        yield dbm.PSM(spectrum=spectrum, mspeptide=mspep, 
                                      experiment=experiment,
                                      _db_modifications=psmmods_copy,
                                      scores=scores)


class Beltejar17PhosFetcher(TSVMSDataFetcher):
    """
    Class to get MS peptides and Phosphorylation information from Beltejar et al.
    2017 supplementary dataset, raw sheet, as TSV files.

    X-NOTE: MaxQuant data with no sequence with modifications and no parent mass.
    """
    _DEFAULT_REPO_DATA = BELTEJAR
    
    ASCORE_THRESHOLD = 0.75 # X-NOTE: inferred from other MaxQuant papers, not mentioned in current paper or supplementary information.
    PTMID2THRESHOLD = {PHOSPHORYLATION['id']: ASCORE_THRESHOLD}
    CELLLINE = JURKAT_E6_1

    def _get_repo_file_version(self):
        return '2017'
    
    def _get_pos2psmmods4row(self, row): #TEST #DEBUG
        """
        Get Positions -> PSM Modifications for a `row` dictionary from a TSV (or
        CSV) file.
        
        :param dict row: a row dictionary from a TSV file.
        
        :return tuple : { position: {PSMModification(), ...} }, clean aminoacid
        sequence.
        """
        # Get modifications probabilities grouped by position from field 
        # `Phospho (STY) Probabilities`:
        clean_seq, pos2phos_probs = ppf.split_seq_and_things( 
                                           row['Phospho (STY) Probabilities'], 
                                           thing_func=float )
        # Get PSM Modifications grouped by position:
        mod_type = self.phosphorylation
        ascore_threshold = self.ASCORE_THRESHOLD
        pos2psmmods = defaultdict(set)
        for pos, phos_probs in pos2phos_probs.items():
            ascore = max(phos_probs) #Max. ascore for the modification position.
            if ascore >= ascore_threshold: #Only store significant phosphorylations:
                psmmod = dbm.PSMModification(mod_type=mod_type, position=pos, 
                                             ascore=ascore, significant=True)
                pos2psmmods[pos].add(psmmod)
        #
        return pos2psmmods, clean_seq
    
    def _get_spectrum4row(self, row, experiment):
        """
        Get the Spectrum for a `row` dictionary from TSV data.
        """
        charge = int( row['Charge'] )
        return dbm.Spectrum(ms_n = 2,
                            charge = charge,
                            has_mzs_intensities = False,
                            experiment = experiment)

    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Beltejar et al. 2017 supplementary
        dataset, along with their associated modifications.
        This method treats each row of the tables as a PSM not as a MS
        Peptide.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object) and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        min_peplength = self.min_peplength
        exp_id = "BEL17" 
        experiment = self.expfetcher.get_experiment(exp_id)
        for tsv_filen in self.tsv_namelist(): #Process only TSV files:
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    evidence = dbm.Evidence( type='IDA', #'Inferred from Direct Assay'
                                             linked_ids={ 'Beltejar17-ID': row['Unique identifier'], 
                                                          exp_id: row['Phospho (STY) Probabilities'] },
                                             extradata={'Mod_peptide_IDs': 
                                                        row['Mod. peptide IDs'].split(';')} )
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row)
                    if len(seq) < min_peplength:
                        continue # Skip/filter-out short peptides. 
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their modifications:
                    prots_from_source = [ ac for ac in row['Proteins'].split(';')
                                          if ac.strip() ]
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    # Get the void Spectrum:
                    spectrum = self._get_spectrum4row(row, experiment)
                    # Create the PSM:
                    # - Get the scores:
                    scores = { 'maxquant_score': float( row['Score'] ), 
                               'maxquant_PEP': row['PEP'] }
                    yield dbm.PSM(spectrum=spectrum, mspeptide=mspep,
                                  experiment=experiment,
                                  _db_modifications=psmmods, scores=scores)


class Mertins13Fetcher(TSVMSDataFetcher):
    """
    Class to get MS data with PTMs information from Mertins et al. 2013  
    Supplementary Table 1 as TSV files.
    """
    _DEFAULT_REPO_DATA = MERTINS
    
    ASCORE_THRESHOLD = 0.75
    PTMID2THRESHOLD = {'default': ASCORE_THRESHOLD}
    CELLLINE = JURKAT
        
    def __init__(self, repo_file, db_session=None, caching=True,
                 cache_limit=100000, needed_oneof_cmods=None, 
                 min_peplength=MIN_PEPLENGTH):
        """
        :param object repo_file: ZIP file containing TSV files. It should be 
        either a string containing the path to the file, or a file-like object.
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to fetch stored data.
        :param bool caching: indicates if the converter should use a caching
        strategy form some created objects. Defaults to True.
        :param cache_limit: maximum number of objects allowed in the cache,
        or None (no limit). Defaults to 100000 (try to avoid high memory use).                
        :param object needed_oneof_cmods: iterable of PD modifications that
        must be present (at least one), or 'all' for all :attr:`VALID_CMODS`
        (all recognized modifications), or None for no need of any modification.
        :param int min_peplength: the minimum length a MS Peptide should have 
        (shorter ones will not be created/imported/returned). Defaults to the 
        value of the global `MIN_PEPLENGTH`.
        """
        super(Mertins13Fetcher, self).__init__(repo_file, db_session=db_session, 
                                               caching=caching,
                                               cache_limit=cache_limit, 
                                               min_peplength=min_peplength)
        # Probability Header in TSV -> Coded Main MODification:
        self.PROBHEADER2CMMOD = {'GlyGly (K) Probabilities': 'gl', 
                                 'Phospho (STY) Probabilities': 'ph', 
                                 'Acetyl (K) Probabilities': 'ac'}
        self.VALID_CMODS = frozenset( self.CMOD2MODATTR.keys() ) # Valid/Recognized coded modifications.
        if (isinstance(needed_oneof_cmods, str) and 
            needed_oneof_cmods.lower() == 'all'):
            self.needed_oneof_cmods = self.VALID_CMODS
        elif ( needed_oneof_cmods is None or 
               self.VALID_CMODS.issuperset(needed_oneof_cmods) ):
            self.needed_oneof_cmods = needed_oneof_cmods
        else:
            raise ValueError("'needed_oneof_cmods' must be a subset of %r, "\
                             "'all' or None."%self.VALID_CMODS)
    
    def _get_repo_file_version(self):
        return '2013'
    
    def _get_pos2psmmods4row(self, row, probabilities_seq_field, main_modification): #TO_DO: CHANGE?
        """
        Get PSM Modifications for a `row` dictionary from a TSV file.
        """
        return super(Mertins13Fetcher, self)._get_pos2psmmods4row(row,
                                                   'Modified sequence',
                                                   probabilities_seq_field,
                                                   main_modification)
    
    def _get_spectrum4row(self, row, experiment, nrow):
        """
        Get the Spectrum for a row dictionary from TSV data.
        """
        charge = int( row['Charge'] )
        scan = int( row['Best localization scan number'] )
        return dbm.Spectrum(charge=charge,
                            has_mzs_intensities=False, 
                            parent_mass=float( row['m/z'] )*charge,
                            raw_file=nrow+" "+row['Best localization raw file'],
                            scan_i=scan,
                            scan_f=scan,
                            experiment=experiment)
    
    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Proteome Discoverer exported PSMs as TSV
        files, along with their associated modifications. This method treats
        each row of the tables as a PSM.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object), and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        if self.needed_oneof_cmods:
            any_of_cmods = lambda cmods_cell: any(cmod in cmods_cell for cmod 
                                                  in self.needed_oneof_cmods)
        else:
            any_of_cmods = lambda cmods_cell: True
        #
        min_peplength = self.min_peplength
        # Process only TSV files:
        for tsv_filen in self.tsv_namelist():
            exp_shortname, _ = os.path.splitext( os.path.basename(tsv_filen) ) #IMPROVE!
            exp_id = "MER13-" + exp_shortname
            experiment = self.expfetcher.get_experiment(exp_id) #Get an experiment from the Experiment Fetcher...
            if experiment is None: #If None found, try with the Cache (DB search has failed in the previous step), or create a new one:
                experiment, _ = self._select_or_create( dbm.Experiment, #The :method:`_select_or_create` ensures searching first in the Cache, if cache is available
                                                        id=exp_id, 
                                                        name="Mertins2013. "+exp_shortname.rpartition('-')[2] )
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                probabilities_seq_field = csvdr.fieldnames[8]
                main_modification = self.PROBHEADER2CMMOD.get(probabilities_seq_field, None)
                # Go to next TSV if there is none of the needed PTMs in the TSV:
                if not any_of_cmods(main_modification):
                    continue
                # Process the TSV file row by row:
                for nrow, row in enumerate(csvdr, start=2):
                    nrow = str(nrow) #Can allow to differentiate between PSMs with same sequence reported with same 'best' spectrum and 'best' raw file
                    # Set an Evidence instance for the PSM:
                    evidence = dbm.Evidence(type='IDA', #'Inferred from Direct Assay'
                                            linked_ids={ exp_id: nrow + " " +
                                                                 row['Modified sequence'] } )
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row, 
                                                                 probabilities_seq_field, 
                                                                 main_modification)
                    if len(seq) < min_peplength:
                        continue # Skip/filter-out short peptides. 
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their associated Modifications:
                    prots_from_source = [ ac for ac in row['Proteins'].split(';')
                                          if ac.strip() ]
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    # Get the Spectrum:
                    spectrum = self._get_spectrum4row(row, experiment, nrow)
                    # Get search engines' scores for the PSM:
                    scores = { 'maxquant_score': float( row['Score'] ) }
                    # Create and yield the PSM object with all its associated data:
                    yield dbm.PSM(spectrum=spectrum, mspeptide=mspep,
                                  experiment=experiment,
                                  _db_modifications=psmmods, scores=scores)


class PDTSVFetcher(TSVMSDataFetcher):
    """
    Class to get MS data with PTMs information from Proteome Discoverer 1.4 
    exported PSMs as TSV files.
    """
    _DEFAULT_REPO_DATA = CROSSTALK
    
    ASCORE_THRESHOLD = 75.0
    PTMID2THRESHOLD = {'default': ASCORE_THRESHOLD}
    CELLLINE = JURKAT_E6_1
    
    PTMRS_ROW = 'ptmRS [14]: Best Site Probabilities'
    
    def __init__(self, repo_file, db_session=None, caching=True,
                 cache_limit=100000, needed_oneof_cmods=None, 
                 min_peplength=MIN_PEPLENGTH):
        """
        :param object repo_file: ZIP file containing P.D 1.4 PSMs information 
        as TSVs. It should be either a string containing the path to the file, 
        or a file-like object.
        :param Session db_session: an optional SQLAlchemy :class:`Session`
        instance, used to fetch stored data.
        :param bool caching: indicates if the converter should use a caching
        strategy form some created objects. Defaults to True.
        :param cache_limit: maximum number of objects allowed in the cache,
        or None (no limit). Defaults to 100000 (try to avoid high memory use).                
        :param object needed_oneof_cmods: iteralbe of PD modifications that
        must be present (at least one), or 'all' for all `VALID_CMODS`
        (recognized modifications from PD 1.4), or None for no need for any
        modification.
        :param int min_peplength: the minimum length a MS Peptide should have 
        (shorter ones will not be created/imported/returned). Defaults to the 
        value of the global `MIN_PEPLENGTH`.
        """
        super(PDTSVFetcher, self).__init__(repo_file, db_session=db_session,
                                           caching=caching,
                                           cache_limit=cache_limit, 
                                           min_peplength=min_peplength)
        # Coded MODification -> MODification ATTRibute:
        self.CMOD2MODATTR = {'GlyGly': 'ubiquitination', 
                             'TMT6&GlyGly': 'ubiquitination', 
                             'Acetyl': 'acetylation', 
                             'Phospho': 'phosphorylation', 
                             'Oxidation': 'oxidation'}
        
        self.VALID_CMODS = frozenset( self.CMOD2MODATTR.keys() ) # Valid/Recognized coded modifications from PD 1.4.
        if (isinstance(needed_oneof_cmods, str) and 
            needed_oneof_cmods.lower() == 'all'):
            self.needed_oneof_cmods = self.VALID_CMODS
        elif ( needed_oneof_cmods is None or 
               self.VALID_CMODS.issuperset(needed_oneof_cmods) ):
            self.needed_oneof_cmods = needed_oneof_cmods
        else:
            raise ValueError("'needed_oneof_cmods' must be a subset of %r, "\
                             "'all' or None."%self.VALID_CMODS)
    
    def _get_repo_file_version(self):
        return '2017'
    
    @staticmethod
    def _decode_pos_cmod4pdcmod(pdcmod, seq_len, cmodinit=0):
        # Get PD1.4 modification position and coded modification type:
        aa_pos, _, pdcmodtype = pdcmod.partition('(') # 'K4(GlyGly)' -> 'K4', '(', 'GlyGly)'
        if not 'Term' in aa_pos:
            pos = int( aa_pos[1:] ) #Get position: 'K4' -> 4 .
        elif aa_pos.startswith('N-Term'):
            pos = 1
        elif aa_pos.startswith('C-Term'):
            pos = seq_len
        cmod = pdcmodtype[cmodinit:-1] #Get PD1.4 coded modification type: 'GlyGly)' -> 'GlyGly'.
        return pos, cmod
    
    def _get_pos2psmmods4row(self, row):
        """
        Get Position -> PSM Modifications for a row dictionary from a Proteome
        Discoverer 1.4 exported TSV file.

        :param dict row: a row dictionary from a PD1.4 TSV file; with the 
        needed fields 'Modifications', 'Sequence' and 
        'ptmRS [14]: Best Site Probabilities'.

        :return tuple : { position: {PSMModification(), ...} }, 'clean aminoacid
        sequence'.
        """
        # Get clean aminoacid sequence from row field 'Sequence':
        clean_seq = row['Sequence'].strip().upper()
        clean_seq_len = len(clean_seq)
        # Get the position of each PTM found by the Sequest search engine from row field 'Modifications':
        sequestcmods = row['Modifications'].split('; ') # 'K4(GlyGly); C5(Carbamidomethyl); M26(Oxidation)' -> ['K4(GlyGly)', 'C5(Carbamidomethyl)', 'M26(Oxidation)']
        cmod2sequestposs = defaultdict(set) # { 'coded modification': {sequence positions} }
        for pdcmod in iter_strs_with_strs_in(self.VALID_CMODS, sequestcmods): #Avoid processing modifications without any of the accepted/recognized modifications:
            pos, cmod = self._decode_pos_cmod4pdcmod(pdcmod, clean_seq_len) #Get position and PD1.4 coded modification type
            cmod2sequestposs[cmod].add(pos)
        # Get the position and score of each PTM evaluated by the ptmRS node from row field 'ptmRS [14]: Best Site Probabilities':
        ptmrscmods = row[self.PTMRS_ROW].split('; ') # 'K4(1xGlyGly): 98; M26(1xOxidation): 100' -> ['K4(1xGlyGly): 98', 'M26(1xOxidation): 100']
        cmod2scoreposs = defaultdict(set) # { 'coded modification': { (PTM score, seq. positions according to ptmRS) } }
        for pdcmod in iter_strs_with_strs_in(self.VALID_CMODS, ptmrscmods): #Avoid processing modifications without any of the accepted/recognized modifications:
            # Split position+PD1.4 coded modification type, and score: 'K4(1xGlyGly): 98' -> 'K4(1xGlyGly)', ': ', 98.0 :
            aapos_pdcmod, _, score = pdcmod.partition(': ') # 'K4(1xGlyGly): 98' -> 'K4(1xGlyGly)', ': ', '98'
            score = float(score)
            # Get position and PD1.4 coded modification type:
            pos, cmod = self._decode_pos_cmod4pdcmod(aapos_pdcmod, clean_seq_len, 2) #Get position and PD1.4 coded modification type
            cmod2scoreposs[cmod].add( (score, pos) )
        # PTM position Check and Re-assignment. Create and store PSM Modifications:
        pos2psmmods = defaultdict(set) # { position: {PSMModification(), ...} }
        for cmod, sequestposs in cmod2sequestposs.iteritems():
            score_poss = cmod2scoreposs.get( cmod, 
                                            { (None, pos) for pos in sequestposs } )
            num_modpos = len(sequestposs) #Number of modified positions for this modification type.
            # PTM position Re-assignment (correct modification positions according 
            # to ptmRS scores):
            if len(score_poss) > num_modpos:
                score_insequest_pos = [ (score, (pos in sequestposs), pos) #Added a ponderation towards ptmRS positions matching Sequest positions 
                                         for score, pos in score_poss ]
                score_insequest_pos.sort( key=lambda t: (1/t[0], not t[1], t[2]) ) #This `key` makes the sorting reverse/descending for the first parameter(score) and the second(ponderation), and direct/ascending for the third(position)
                score_poss = [ (score, pos) for score, _, pos 
                              in score_insequest_pos[:num_modpos] ]
            # Create and store new PSM Modifications:
            mod_type = getattr( self, self.CMOD2MODATTR[cmod] ) #Get existing :class:``ModificationType`` instance corresponding to the PD1.4 coded modification type.
            for score, pos in score_poss:
                psmmod = dbm.PSMModification( mod_type=mod_type, 
                                              position=pos, 
                                              ascore=score, 
                                              significant=(score>=self.ASCORE_THRESHOLD) )
                pos2psmmods[pos].add(psmmod)
        #
        return pos2psmmods, clean_seq
    
    def _get_spectrum4row(self, row, experiment):
        """
        Get the Spectrum for a row dictionary from TSV data.
        """
        return dbm.Spectrum(ms_n=int( row['MS Order'][-1] ), # 'MS2' -> 2 .
                            charge=int( row['Charge'] ),
                            has_mzs_intensities=False, #TO_DO: Add m/z spectra from MGF.
                            parent_mass=float( row['MH+ [Da]'] ),
                            raw_file=row['Spectrum File'],
                            scan_i=int( row['First Scan'] ),
                            scan_f=int( row['Last Scan'] ),
                            rt=float( row['RT [min]'] ),
                            experiment=experiment)
    
    def iter_all_psms(self):
        """
        Iteratively get all PSMs from Proteome Discoverer exported PSMs as TSV
        files, along with their associated modifications. This method treats
        each row of the tables as a PSM.

        :return generator : yields PSM (:class:`PSM` objects) with their
        associated modifications (:class:`PSMModification` objects), Spectrum
        (:class:`Spectrum` object), and MS Peptide (a :class:`MSPeptide` object
        with their associated :class:`MSPepModification` objects).
        """
        if self.needed_oneof_cmods:
            any_of_cmods = lambda cmods_cell: any(cmod in cmods_cell for cmod 
                                                  in self.needed_oneof_cmods)
        else:
            any_of_cmods = lambda cmods_cell: True
        #
        min_peplength = self.min_peplength
        # Process only TSV files:
        for tsv_filen in self.tsv_namelist():
            exp_name, _ = os.path.splitext( os.path.basename(tsv_filen) ) #IMPROVE!
#             exp_subid = exp_name.split('_')[1] #Dades 2016-10-17  #IMPROVE!
            exp_subid = exp_name.split('_')[2] #Dades 2017-11-24  #IMPROVE!
            exp_id = "TCXTALKDB-" + exp_subid
            ev_linkedid_key = "PD1.4-" + exp_subid
            experiment = self.expfetcher.get_experiment(exp_id) #Get an experiment from the Experiment Fetcher...
            if experiment is None: #If None found, try with the Cache (DB search has failed in the previous step) or create a new one:
                experiment, _ = self._select_or_create(dbm.Experiment, #The :method:`_select_or_create` ensures search in the Cache, if available
                                                       id=exp_id, 
                                                       name=exp_name)
            # Process the TSV file row by row:
            with self.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    # Go to next PSM if there is none of the needed PD 
                    # modifications in the PSM's 'Modifications' cell:
                    if not any_of_cmods( row['Modifications'] ):
                        continue
                    # Set an Evidence instance for the PSM:
                    evidence = dbm.Evidence(type='IDA', #'Inferred from Direct Assay'
                                            linked_ids={ ev_linkedid_key: 
                                                         row['Sequence'] } )
                    # Get PSM Modifications:
                    pos2psmmods, seq = self._get_pos2psmmods4row(row)
                    if len(seq) < min_peplength:
                        continue # Skip/filter-out short peptides. 
                    psmmods = {mod for mods in pos2psmmods.values()
                                   for mod in mods}
                    # Get the MS Peptide with their associated Modifications:
                    prots_from_source = [ ac for ac in 
                                          row['Protein Group Accessions'].split(';')
                                          if ac.strip() ]
                    mspep = self._get_mspeptide4data(row, seq, pos2psmmods,
                                                     psmmods, experiment,
                                                     evidence, prots_from_source)
                    mspep.missing_cleavages = int( row['# Missed Cleavages'] )
                    # Get the Spectrum: #TO_DO: Add m/z spectra.
                    spectrum = self._get_spectrum4row(row, experiment)
                    # Get search engines' scores for the PSM:
                    scores = {'sequest_xcorr': float( row['XCorr'] ),
                              'sequest_deltacn': float( row['ΔCn'] ), #CAUTION with the delta character!
                              'percolator_qValue': float( row['q-Value'] ),
                              'percolator_PEP': float( row['PEP'] ),
#                              'sequest_d': float( row[''] ),
                              }
                    # Create and yield the PSM object with all its associated data:
                    yield dbm.PSM(spectrum=spectrum, mspeptide=mspep,
                                  experiment=experiment,
                                  _db_modifications=psmmods, scores=scores)


#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    prot_converter = UniProtRepositoryFetcher(repo_file=UNIPROT_FILE)
    protein = prot_converter.fetch_protein('P56524')
    print( prot_converter.logger() )
    print(protein)
    print(protein.organism)
    print(protein.organism.extradata)
    print(protein.seq)
    print(protein.pos2modifications)
    for modification in protein.modifications:
        print(modification)
        print(modification.aa)
    print(protein.goannotations)
    qgo_converter = QuickGOAWebFetcher()
    goannotations = qgo_converter.goannotations('P56524')
    for goann in goannotations:
        if goann.go.id == 'GO:0000122':
            print(goann.go.id, goann.with_from)
    goannotations = qgo_converter.summarize_goannotations_withfrom(goannotations)
    for goann in goannotations:
        if goann.go.id == 'GO:0000122':
            print(goann.go.id, goann.with_from)
