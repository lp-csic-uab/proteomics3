# -*- coding: utf-8 -*-

"""
:synopsis: Common Basic Functions for Proteomics with Peptides and Proteins (NEW version migrated to Python 3.x).

:created:    2015-03-17

:authors:    Óscar (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.4.3'
__UPDATED__ = '2019-09-17'

#===============================================================================
# Imports
#===============================================================================
import csv
import re
from collections import defaultdict

from commons3 import mass
from general3.basic_class import ObjectDict

from .unimod_globals import UNIMOD

#===============================================================================
# Global variables
#===============================================================================
AA2REGEX = {'B': '[BND]', 'Z': '[ZEQ]', 'J': '[JIL]', 'X': '.'}
AA2REGEX_PROTEOMICS  = {'B': '[BND]', 'Z': '[ZEQ]', 'J': '[JIL]', 'X': '.',
                        'I': '[JIL]', 'L': '[JIL]'}
AA2REGEX_PROTEOMICS_LOWRES  = {'B': '[BND]', 'Z': '[ZEQ]', 'J': '[JIL]', 'X': '.',
                               'I': '[JIL]', 'L': '[JIL]', 
                               'Q': '[QK]', 'K': '[QK]'} #X-NOTE: http://www.ionsource.com/tutorial/DeNovo/rules.htm


#===============================================================================
# Functions declarations
#===============================================================================
def new_protein_from_fasta(seq_header, seq_buffer):
    return ObjectDict( ac = seq_header.split('|')[1], 
                       name = seq_header, 
                       seq = ''.join(seq_buffer) )


def iter_prots_in_fasta( fasta_filen, filter_out=('REVERSE', 'DECOY') ):
    """
    Load data from a FASTA filename and yields it.
    
    :param str fasta_filen: input FASTA filename.
    :param iterable of str filter_out:
    
    :return generator : yields tuples of each protein's header and sequence.
    """
    do_filter_out = bool(filter_out)
    with open(fasta_filen, 'rU') as i_file:
        seq_header = None
        seq_buffer = list()
        for row in i_file:
            row = row.strip()
            if row[0] == '>': # New protein:
                if seq_header and seq_buffer:
                    yield seq_header, "".join(seq_buffer) # Yield previous protein.
                #
                seq_buffer = list()
                seq_header = row.lstrip("> ") # Get rid of ">" or "> "
                # Filter out decoys:
                if do_filter_out:
                    row_upper = row.upper()
                    for filter_str in filter_out:
                        if filter_str in row_upper:
                            seq_header = None
                            break
            elif seq_header: # New line with AAs from a protein:
                seq_buffer.append(row)
        # Yield the last protein:
        if seq_header and seq_buffer:
            yield seq_header, "".join(seq_buffer)


def load_fasta_data(fasta_filen):
    fasta_prots = dict()
    #
    for header, seq in iter_prots_in_fasta(fasta_filen, filter_out=False):
        prot = ObjectDict(ac=header.split('|')[1], name=header, seq=seq)
        fasta_prots[prot.ac] = prot
#     #
#     with open(fasta_filen, 'r') as io_file:
#         seq_header = ''
#         seq_buffer = list()
#         curr_prot = None
#         for row in io_file:
#             row = row.strip()
#             if row[0] == '>':
#                 if seq_header and seq_buffer:
#                     curr_prot = new_protein_from_fasta(seq_header, seq_buffer)
#                     fasta_prots[curr_prot.ac] = curr_prot
#                 seq_buffer = list()
#                 seq_header = row
#             else:
#                 seq_buffer.append(row)
#         if seq_header and seq_buffer:
#             curr_prot = new_protein_from_fasta(seq_header, seq_buffer)
#             fasta_prots[curr_prot.ac] = curr_prot
    #
    return fasta_prots


def proteins_by_ac(proteins):
    return {prot.ac: prot for prot in proteins}


def find_mods(pep_orig, unimods=None, fixed_mods=None):
    """
    Locates UNIMOD modifications, and returns a list of dictionaries
    containing location and UNIMOD information.
    
    #TODO: change to use :func:`split_seq_and_things`
    """
    if not unimods:
        unimods = UNIMOD
    if not fixed_mods: #TODO: make it more general not so case-specific
        # Find carbamidomethylations X-NOTE: default static modification:
        fixed_mods = { 'C': UNIMOD[4] }
    pos = 0
    mods = list()
    aas_and_mods = re.split('[\\)\\(, ]', pep_orig)
    for aas_or_mods in aas_and_mods:
        if aas_or_mods.isdigit(): # Mods numbers (unimods):
            mod = unimods[ int(aas_or_mods) ].copy()
            mod['pos'] = pos
            mods.append(mod)
        else: # AAs letters (fixed_mods):
            for residue, unimod in fixed_mods.items():
                sub_pos = 0
                c_pos = aas_or_mods.find(residue, sub_pos)
                while c_pos > -1:
                    sub_pos = c_pos + 1
                    mod = unimod.copy()
                    mod['pos'] = pos + sub_pos
                    mods.append(mod)
                    c_pos = aas_or_mods.find(residue, sub_pos)
            pos += len(aas_or_mods)
    return mods


def split_seq_and_things(seq_w_things, seps=("(", None, ")"), 
                         thing_func=lambda x:x):
    """
    Get the plain sequence from a 'decorated' sequence with 'things' enclosed
    in separators (such as brackets), and the positions in the sequence where
    those 'things' occur:
      'A(1, 35)BB(23)CCC' -> 'ABBCCC', { 1: ['1', '35'], 3: ['23'] }
      'A(1)(35)BB(23)CCC' -> 'ABBCCC', { 1: ['1', '35'], 3: ['23'] }
    
    :param str seq_w_things: a 'decorated' sequence with 'things' enclosed in 
    separators.
    :param tuple seps: open, middle and close separators as a tuple.
    Defaults to ( '(', None, ')' ).
    :param func thing_func: a function to apply to every thing found in the 
    sequence. Defaults to a function tha returns the same thing.
    
    :return tuple : the plain sequence, and a dictionary containing the 
    (bio)positions within the sequence where things occur, along with those 
    things.
    """
    osep, msep, csep = seps #Open, middle and close separators
    pos = 0
    pos2thing = defaultdict(list)
    seq = list()
    thing = list()
    is_thing = False
    for char in seq_w_things:
        if char == osep: #Open separator -> start a thing
            is_thing = True
            thing = list()
        elif char == msep: #Middle separator -> another thing
            pos2thing[pos].append( thing_func("".join(thing)) )
            thing = list()
        elif char == csep: #Close separator -> last thing
            pos2thing[pos].append( thing_func("".join(thing)) )
            is_thing = False
        elif is_thing: #Characters of a thing
            thing.append(char)
        else: #Sequence Characters
            pos += 1 #Only Sequence Characters stand for a sequence position
            seq.append(char)
    return "".join(seq), pos2thing


def merge_seq_and_things( seq_wo_things, pos2things, seps=("(", None, ")") ):
    """
    Get the 'decorated' sequence, with 'things'/modifications enclosed in
    separators (such as brackets),from a plain peptide sequence and the
    positions in the sequence along with 'things'/modifications at those
    positions. Ex.:
      'ABBCCC', { 1: ['21'], 3: ['23', '1'] }, seps=("(", ", ", ")") -> 'A(21)BB(23, 1)CCC'
      'ABBCCC', { 1: ['21'], 3: ['23', '1'] }, seps=("(", None, ")") -> 'A(21)BB(23)(1)CCC'
    
    :param str seq_wo_things: a plain peptide sequence; without 'things'
    enclosed in separators.    
    :param dict pos2things: a dictionary containing the positions within the 
    sequence where things occur, along with those things as a list.
    :param tuple seps: open, middle and close separators as a tuple.
    Defaults to ( "(", None, ")" ).
    
    :return str : the resulting 'decorated' sequence.
    """
    open_sep, mid_sep, close_sep = seps #Open, middle and close separators
    seq_aas = list(seq_wo_things)
    if mid_sep is None:
        thing_tmplt = open_sep + "{0}" + close_sep #Ensure `things` list will only contain strings (using also format() below)
        for pos, things in pos2things.items():
            pypos = pos - 1
            for thing in things:
                seq_aas[pypos] += thing_tmplt.format(thing)
    else:
        for pos, things in pos2things.items():
            things_as_strs = map(str, things) #Ensure `things` list only contains str.
            seq_aas[pos - 1] += open_sep + mid_sep.join(things_as_strs) + close_sep
    return "".join(seq_aas)


def formatseq4search(seq, aa_mapping=AA2REGEX, other_aa_tmplt='{0}', 
                     global_tmplt='(?=({0}))'):
    """
    Formating of original string according to a `aa_mapping` dictionary. 
    Ex.: AAABAAXAAJ -> AAA[BND]AA.AA[JIL]
    
    :param string seq: and aminoacid sequence.
    :param dict aa_mapping: a dictionary that maps aminoacids in `seq` to
    regular expressions, .... Ex.: { aa: regex }. Defaults to the global
    mapping dictionary `AA2REGEX`. X-NOTE: this default doesn't make
    substitutions for proteomics; use `AA2REGEX_PROTEOMICS` or
    `AA2GROUP_PROTEOMICS_LOWRES` instead.
    :param string other_aa_tmplt: a template to apply to aminoacids not in the 
    `aa_mapping` dictionary. Defaults to '{0}' (let the aa without change).
    :param string global_tmplt: a template to apply to the whole new sequence.
    Defaults to '(?=({0}))' to add a regular expression lookahead assertion 
    X-NOTE: this allows overlapping matches, but those matches are void
    (.group(0) = '') unless a capture group is specified in the pattern such as
    '(?=({0}))', and then the matching string should be accessed in .group(1) :
    http://stackoverflow.com/a/11430936)
    
    :return string : a new sequence from the original one according to the
    `aa_mapping` replacements.
    """
    aa_map_keys = aa_mapping.keys()
    new_seq = "".join( aa_mapping[aa] if aa in aa_map_keys 
                       else other_aa_tmplt.format(aa) for aa in seq )
    return global_tmplt.format(new_seq)


def overlaping_poss(str_a, str_b, min_overlap=5):
    """
    Find if two string sequences contain an overlapping region. If so, return 
    the overlapping region positions on both sequences.
    
    :param str str_a: a sequence string.
    :param str str_b: another sequence string.
    :parma int min_overlap: minimum length of the overlapping region to be 
    considered.
    
    :return dict of lists: 
    """
    # First check if both sequences are equal:
    len_a = len(str_a)
    if str_a == str_b:
        #
        return { str_a: [ (0, len_a) ] }
    # Second check if the smallest sequence is contained in the longest one:
    len_b = len(str_b)
    if len_a > len_b:
        #Swap input strings so str_a is the shortest and str_b the longest:
        str_b, str_a = str_a, str_b
        len_b, len_a = len_a, len_b
    if str_a in str_b:
        strb_matchposs = list()
        str_a_inb = str_b.find(str_a)
        while str_a_inb > -1:
            strb_matchposs.append( (str_a_inb, str_a_inb + len_a) )
            str_a_inb = str_b.find(str_a, str_a_inb + 1)
        #
        return { str_a: [ (0, len_a) ] * len(strb_matchposs), 
                 str_b: strb_matchposs }
    # Then, the overlap, is any, should be partial in one of the sequences extremes.
    len_maxoverlap = len_a - 1 #This is the maximum possible overlap length (the length of smaller str -1 to avoid the equivalent of str_a in str_b).
    # So, try to find a maximum partial overlap of the shortest sequence C-Term 
    # with the longest sequence N-term:
    cterm_a = str_a[-min_overlap:] #Shortest sequence C-Term.
    len_max_ctainb = 0
    pos_ctainb = str_b.rfind(cterm_a, 0, len_maxoverlap)
    while pos_ctainb > -1:
        len_max_ctainb = pos_ctainb + min_overlap
        if str_b[:pos_ctainb]==str_a[-(len_max_ctainb):-min_overlap]:
            #
            cta_in_b = { str_b: [ (0, len_max_ctainb) ], 
                         str_a: [ (len_a -(len_max_ctainb), len_a) ] }
            break
        pos_ctainb = str_b.rfind(cterm_a, 0, (pos_ctainb + min_overlap) - 1)
    # Finally, try to find a maximum partial overlap of the shortest sequence N-Term 
    # with the longest sequence C-term:
    nterm_a = str_a[:min_overlap]#Shortest sequence N-Term.
    len_max_ntainb = 0
    pos_ntainb = str_b.find(nterm_a, -len_maxoverlap)
    while pos_ntainb > -1:
        len_max_ntainb = len_b - pos_ntainb
        if str_b[pos_ntainb+min_overlap:]==str_a[min_overlap:len_max_ntainb]:
            #
            nta_in_b = { str_b: [ (pos_ntainb, len_b) ], 
                         str_a: [ (0, len_max_ntainb) ] }
            break
        pos_ntainb = str_b.find(nterm_a, pos_ntainb + 1)
    #
    if not (len_max_ctainb or len_max_ntainb):
        return False
    elif len_max_ctainb > len_max_ntainb:
        return cta_in_b
    else:
        return nta_in_b


def iterfind_pep_in_prots(pep, prots, aa_mapping=AA2REGEX):
    pep = formatseq4search(pep.upper(), aa_mapping)
    #
    for prot in prots:
        pep_pos = list()
        for match in re.finditer(pep, prot.seq):
            pep_pos.append( (match.start(1) + 1, match.end(1)) ) #X-NOTE: Biological sequence positions (start at 1), not python positions
        if pep_pos:
            yield prot, pep_pos


def itersimplefind_pep_in_prots(pep, prots):
    for prot in prots:
        if pep in prot.seq:
            yield prot


def protein_coverage(protein, peptides):
    covered_aa_pos = set() #Amino-acids positions covered by peptides
    for peptide in peptides.values():
        pep_prots = [protein]
        prots_w_peppos = iterfind_pep_in_prots(peptide['peptide'], pep_prots)
        for _, pep_poss in prots_w_peppos:
            for pep_pos in pep_poss:
                start, end = pep_pos
                covered_aa_pos.add(range(start, end+1))
    # Coverage calculus:
    return len(covered_aa_pos) / len(protein.seq) * 100


def pep_mod_mass(peptide, unimods=None, fixed_mods=None):
    """
    Calculate modified peptide mass. For dict-like peptide object.
    
    :param dict peptide: a dict-like peptide object, with keys 'peptide' (raw
    peptide sequence) and 'pep_orig' (peptide sequence with included
    modifications as UniMod IDs)
    """
    #
    pep_seq = peptide['peptide']
    pep_mass = mass.get_mass(pep_seq)
    #
    pep_orig = peptide['pep_orig']
    mods = find_mods(pep_orig, unimods, fixed_mods)
    for mod in mods:
        pep_mass += mod['delta_mass']
    return pep_mass

def pep_w_mods_mass(peptide, unimods=None, fixed_mods=None):
    """
    Calculate modified peptide mass. For objects of sub-classes of
    :class:`PepLikeMixin` (from db_models.py module)
    
    :param PepLikeMixin peptide: an instance form a sub-classes of
    :class:`PepLikeMixin`, with attributes :attr:`seq` (raw peptide sequence)
    and :attr:`seq_w_mods` (peptide sequence with included modifications as
    UniMod IDs)
    """
    pep_mass = mass.get_mass(peptide.seq) #Mass for the raw sequence
    for unimod in find_mods(peptide.seq_w_mods, unimods, fixed_mods): #Add mass for each PTM in seq_w_mods:
        pep_mass += unimod['delta_mass']
    return pep_mass

def get_mass4mz_charge(mz, charge):
    """Calc mass using a formula that takes into account the mass of the proton and the electron"""
    return (mz * charge) - ( charge * (1.00782503224 - 0.00054857990907) )

def get_mz(mass, charge):
    """Calc m/z using a formula that takes into account the mass of the proton and the electron"""
    return ( mass + charge * (1.00782503224 - 0.00054857990907) ) / charge


def save_prot_data(filen, prot_acs, all_proteins, data_headers = None):
    '''
    Generic function to save protein data from a iterable containing protein ACs into a filen CSV file:
    From those proteins, also put apart the uncharacterized ones.
    '''
    if not data_headers:
        data_headers = ['ac','name','gene','organism','evidence','database','revised']
    
    with open(filen + '.csv', 'wb') as io_file:
        with open(filen + '_uncharacterized.csv', 'wb') as io_file_unchard:
            csvdwriter = csv.DictWriter(io_file, data_headers, extrasaction='ignore', delimiter=';')
            csvdwriter_unchard = csv.DictWriter(io_file_unchard, data_headers, extrasaction='ignore', delimiter=';')
            csvdwriter.writer.writerow(data_headers)
            csvdwriter_unchard.writer.writerow(data_headers)
            curr_csvdwriter = None
            for prot_ac in sorted(prot_acs):
                if 'uncharacterized protein' not in all_proteins[prot_ac].name:
                    curr_csvdwriter = csvdwriter
                else:
                    curr_csvdwriter = csvdwriter_unchard
                curr_csvdwriter.writerow(all_proteins[prot_ac].__dict__)
    return

