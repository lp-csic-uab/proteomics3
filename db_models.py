# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

"""
:synopsis: Database Model Classes for Proteomics.

:created:    2014-10-21

:authors:    Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.16.4'
__UPDATED__ = '2018-08-30'

#===============================================================================
# Imports
#===============================================================================
import contextlib
import json
import re
from collections import defaultdict, OrderedDict
from datetime import datetime
from itertools import product

from sqlalchemy.orm import (relationship, backref, reconstructor, deferred, 
                            sessionmaker, validates, joinedload)
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.ext.mutable import Mutable, MutableDict
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy import (Column, Integer, Float, String, Text, Boolean, Date, 
                        Enum, Sequence, ForeignKey, Table, SmallInteger)
from sqlalchemy import create_engine, inspect, types, or_, event

import proteomics.pepprot_func as ppf
from general.basic_class import InitKwargs2AttrMixin, ReprMixin, Memoized
from general.basic_func import chunker, flatten_1lvl
# from math_extras import pystats #Used in :method:`DigestionPeptide.mean_styk_dist`


#===============================================================================
# Global variables
#===============================================================================
# X-NOTE: due the lack of 'J' in current UniProt Human proteins, and to
# increase the speed of :func:`search_seq_in_prot_db`, 'J', 'I', and 'L' are
# mapped to ('I','L') instead of ('J','I','L'), in the following mapping
# dictionaries:
AA2GROUP = {'B': ('B','N','D'), 'Z': ('Z','E','Q'), 'J': ('I','L'),
            'X': ('_', 'X')}
AA2GROUP_PROTEOMICS = {'B': ('B','N','D'), 'Z': ('Z','E','Q'), 'J': ('I','L'),
                       'X': ('_', 'X'), 'I': ('I','L'), 'L': ('I','L')}
AA2GROUP_PROTEOMICS_LOWRES = {'B': ('B','N','D'), 'Z': ('Z','E','Q'), 'J': ('I','L'),
                              'X': ('_', 'X'), 'I': ('I','L'), 'L': ('I','L'),
                              'Q': ('Q', 'K'), 'K': ('Q', 'K')}

# IDs (Unimod Accessions) of modifications WITHOUT a Clear Physiological Origin, 
# so they can be excluded from some operations (`seq_w_mods` and others):
NO_PHYSIO_MDDIDS = {4} #Cys Carbamidomethylation by default.


#===============================================================================
# Function definitions
#===============================================================================
def select_or_create(session, ModelCls, **kwargs):
    """
    Select a `ModelCls` class instance from a Database `session`, according to
    the keyword arguments passed as filters, if it already exists; otherwise
    creates the instance, and adds it to the `session`.

    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param class ModelCls: a SQLAlchemy model class.

    :return tuple : an instance of the supplied model class, and a boolean 
    indicating if this is a newly created instance (True) or not (False).
    """
    # Try to get an object from the DataBase `session` using the keyword
    # arguments passed as filters:
    model_obj = session.query(ModelCls).filter_by(**kwargs).first()
    if model_obj:
        # Return the retrieved DataBase object, indicating it's not a new one:
        return model_obj, False
    else:
        # Create a new :class:`ModelCls` object, using the keyword
        # arguments passed as its attributes:
        model_obj = ModelCls(**kwargs)
        session.add(model_obj) #Add new object to the DataBase `session`.
        return model_obj, True #Return new object indicating it's a new one.


def get_or_create(session, ModelCls, pk, **extrakwargs):
    """
    Get a `ModelCls` instance from a Database `session` using its Primary Key
    (`pk`), if it already exists; otherwise creates the instance with the
    supplied `pk` and optional extra keyword arguments (`extrakwargs`), and
    adds it to the `session`.

    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param class ModelCls: a SQLAlchemy model class.
    :param object pk: a Primary Key value for the specified `ModelCls`.

    :return tuple : an instance of the supplied model class, and a boolean 
    indicating if this is a newly created instance (True) or not (False).
    """
    # Try to get the object from the DataBase `session` using its Primary Key:
    model_obj = session.query(ModelCls).get(pk)
    if model_obj:
        # Return the retrieved DataBase object, indicating it's not a new one:
        return model_obj, False
    else:
        # Create a new :class:`ModelCls` object, using :param:`pk` value as its
        # Primary Key value:
        extrakwargs[inspect(ModelCls).primary_key[0].name] = pk #X-NOTE: http://stackoverflow.com/a/10594091
        model_obj = ModelCls(**extrakwargs)
        
        session.add(model_obj) #Add new object to the DataBase `session`.
        return model_obj, True #Return new object indicating it's a new one.


def validate_enum(obj, attr, value, allowed_values):
    if value not in allowed_values:
        print( "Warning!: {0} not in {1}, for attribute `{2}` of object {3}"\
               .format(value, allowed_values, attr, obj) )
        return None
    return value


def session_from_instance(model_obj):
    db_session = inspect(model_obj).session
    if not db_session:
        raise TypeError("If the model object is not associated with a "\
                        "`session`, a valid `session` should be provided.")
    return db_session


def search_seq_in_prot_db(seq, session, aa_mapping=AA2GROUP_PROTEOMICS,
                          global_tmplt='%{0}%'):
    """
    Search a sequence and all its possible variants (according to an amino acid
    mapping dictionary) in all the proteins of a database, using the backend
    query engine but avoiding the use of regular expression searches (not found
    in SQLAlchemy, and backend-specific).
    X_NOTE: This is supposed to be faster than doing it in pure Python (almost
    all the workload it's on the optimized SQL engine), but it's still slow
    (very long queries and big search space, and, additionally, if no
    threads/processes are used, only 2 CPU cores can be used: one for the
    Python process and another for the backend engine server).

    :param str seq: and amino acid sequence.
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param dict aa_mapping: a dictionary that maps amino acids in `seq` to
    groups of amino acid variants. Ex.: { aa: (aa, aa1, aa2, ... ), ... }. 
    Defaults to global dictionary AA2GROUP_PROTEOMICS.
    :param string global_tmplt: a template to apply to each new sequence
    variant before searching. Defaults to '%{0}%' for searching the sequence
    everywhere inside the protein ones.

    :return set : a set containing all matching :class:`Protein` instances
    found.
    """
    # Generate all the possible sequence combination variants:
    aa_map_keys = frozenset( aa_mapping.keys() )
    seq_groups = [aa_mapping[aa] if aa in aa_map_keys else (aa,) for aa in seq]
    seq_combs = list( product(*seq_groups) )
    # Search Proteins sequences:
    prots = set()
    for seq_combs_chunk in chunker(seq_combs, 500): #Split the combinations to search in chunks of 500 to avoid very long and complex SQL sentences:
        # Make a query with multiple 'OR LIKE' SQL stamens (one for each sequence
        # combination variant):
        ors = [Protein.seq.like( global_tmplt.format( "".join(aa_comb) ) )
               for aa_comb in seq_combs_chunk]
        prots.update( session.query(Protein).filter( or_(*ors) ) )
    return prots


# DEPRECATED: slower than :func:`search_seq_in_prot_db`
# def search_seq_in_prot_db_mysqlregex(seq, session, 
#                                      aa_mapping=ppf.AA2REGEX_PROTEOMICS,
#                                      global_tmplt='{0}'):
#     """
#     Version of :func:`search_seq_in_prot_db` but using regular expression
#     searches as in MySQL backend.
#     
#     :CAUTION: Deprecated!. Generally SLOWER on MySQL than using the 'LIKE' 
#     SQL stamen, so use instead the previous :func:`search_seq_in_prot_db`.
#     """
#     search_seq = ppf.formatseq4search(seq, aa_mapping, global_tmplt=global_tmplt)
#     prots = session.query(Protein).filter( Protein.seq.op('RLIKE')(search_seq) )
#     return set(prots)


def generate_srcid_ptmid2threshold(sources, ptm_ids=None, session=None):
    """
    Generate a dictionary of the 'ascore threshold' corresponding to each
    ModificationType ID for each Source ID:
        { (Source ID, ModificationType ID): ascore threshold, ...}
    
    :param iterable sources: an iterable of :class:`Source` instances.
    :param iterable ptm_ids: an iterable of ModificationType IDs numbers. If
    None is provided, :param:`session` will be used to get All the
    ModificationType IDs from the Database.
    :param Session session: an optional SQLAlchemy :class:`Session` instance.
    :caution: At least one of the two parameters :param:`ptm_ids` 
    or :param:`session` must be provided.

    :return dict : { (Source ID, ModificationType ID): ascore threshold, ...}.
    :caution: The value of 'ascore threshold' can be None (=not defined).    
    """
    if ptm_ids is None:
        if session is not None: # No `ptm_ids` but a `session`:
            # Get all the ModificationType IDs from the Database:
            ptm_ids = [ x for x, in session.query(ModificationType.id).all() ] #Query list of tuples -> flat list of IDs.
        else: # Error -> No `ptm_ids` and no `session`:
            raise TypeError("At least one of the two parameters `ptm_ids` or "
                            "`session` must be provided.")
    # Generate the dictionary { (Source ID, ModificationType ID): ascore threshold, ...}:
    srcid_ptmid2threshold = dict()
    for source in sources:
        source_id = source.id
        ptmid2threshold = source.ptmid2threshold
        default = ptmid2threshold.get('default', None) #Dictionary :attr:`ptmid2threshold` can have a 'default' threshold value for all other PTM IDs not in it; otherwise the default will be None.
        for ptm_id in ptm_ids:
            ascore_threshold = ptmid2threshold.get(ptm_id, default)
            srcid_ptmid2threshold[source_id, ptm_id] = ascore_threshold
    #
    return srcid_ptmid2threshold


# See :func:`create_proteases` at the end of this file.


#===============================================================================
# Database Basic Classes definitions
#===============================================================================
Base = declarative_base()


class DataBase(object):
    """
    A class to act as a wrapper to abstract the use of SQLAlchemy engine,
    sessions, ... from a single DataBase object.
    """
    def __init__(self, database_uri=None, engine=None, models_base=Base,
                 *args, **kwargs):
        self._models_base = models_base
        # Create an attribute that is a dictionary of the model classes, but
        # also allows those to be accessed as its own attributes:
        name2model = dict(models_base._decl_class_registry)
        self._models = type('Name2Model', (dict,), name2model)()
        self._models.update(name2model)
        #
        self.new_session = sessionmaker() #Callable instance returned by SQLAlchemy factory :class:``sessionmaker`` to create new DB sessions.
        self.connect(database_uri, engine, *args, **kwargs) #Binds :attr:`new_session` to a SQLAlchemy :class:`Engine` instance, if :param:`database_uri` or :param:`engine` are provided.
    
    @property #Read-only property
    def models_base(self):
        return self._models_base
    
    @property #Read-only property
    def models(self):
        return self._models
    
    @property #Read-only property
    def tables(self):
        return self._models_base.metadata.tables
    
    def connect(self, database_uri=None, engine=None, *args, **kwargs):
        """
        Binds a provided or a newly created SQLAlchemy :class:`Engine` instance
        to the callable :attr:`new_session`, if either :param:`database_uri`
        or :param:`engine` is provided.
        """
        self.db_uri = database_uri
        if not engine and database_uri:
            engine = create_engine(database_uri, pool_recycle=3600, *args, **kwargs) #Reset connections in the pool if they are 60 minutes old. X_NOTE: https://docs.sqlalchemy.org/en/rel_0_9/dialects/mysql.html?highlight=pool_recycle#connection-timeouts
        self.db_engine = engine
        self.new_session.configure(bind=engine) #Binds callable :attr:`new_session` to the new engine.
    
    def close(self):
        """
        Close ALL connections to the database management system (DBMS).
        It may help avoiding "Too many connections" errors with the DBMS.
        """
        self.db_engine.dispose() #Close ALL connections in the Engine connection pool.
    
    def drop_tables(self, tables=None, checkfirst=True):
        self._models_base.metadata.drop_all(self.db_engine, tables=tables,
                                            checkfirst=checkfirst)
        return self.tables
    
    def create_tables(self, force_recreate=False, tables=None, checkfirst=True):
        if force_recreate: # Drop first the existing tables:
            self.drop_tables(tables=tables, checkfirst=checkfirst)
        # Creates the specified `tables` in the database:
        self._models_base.metadata.create_all(self.db_engine, tables=tables,
                                              checkfirst=checkfirst)
        return self.tables
    
    def get_db_connection(self):
        return self.db_engine.connect()
    
    @contextlib.contextmanager
    def session(self, *args, **kwargs):
        """
        A Database session context manager.
        """
        session = self.new_session(*args, **kwargs)
        try:
            yield session
        except Exception as _:
            raise
        finally:
            session.rollback() #Roll-back any uncommitted changes
            session.close()
            del session #Ensure session is marked for garbage collection
    
    def session_no_autoflush(self, *args, **kwargs):
        """
        A Database session context manager without ``autoflush`` to the Database.
        """
        kwargs['autoflush'] = False
        return self.session(*args, **kwargs)
    
    def select_or_create(self, ModelCls, session=None, **kwargs):
        """
        Select a `ModelCls` instance from a Database `session`, according to the
        keyword arguments passed as filters, if it already exists; otherwise
        creates the instance, and adds it to the `session`.

        :param class ModelCls: a SQLAlchemy model class.
        :param Session session: an optional SQLAlchemy database session. 
        If None is provided, a new one will be created and used.

        :return tuple : an instance of the model class passed, and a boolean
        indicating if this is a newly created instance (True) or not (False).
        """
        if session is None:
            with self.session() as session:
                model_obj, isnew = select_or_create(session, ModelCls, **kwargs)
                session.commit()
            return model_obj, isnew
        else:
            return select_or_create(session, ModelCls, **kwargs)
    
    def get_or_create(self, ModelCls, pk, session=None, **extrakwargs):
        """
        Get a `ModelCls` instance from a database `session` using its Primary
        Key (`pk`), if it already exists; otherwise creates the instance with
        the supplied `pk` and optional extra keyword arguments
        (`extrakwargs`), and adds it to the `session`.

        :param class ModelCls: a SQLAlchemy model class.
        :param object pk: a Primary Key value for the specified `ModelCls`.
        :param Session session: an optional SQLAlchemy database session. 
        If None is provided, a new one will be created and used.

        :return tuple : an instance of the supplied model class, and a boolean
        indicating if this is a newly created instance (True) or not (False).
        """
        if session is None:
            with self.session() as session:
                model_obj, isnew = get_or_create(session, ModelCls, pk, **extrakwargs)
                session.commit()
            return model_obj, isnew
        else:
            return get_or_create(session, ModelCls, pk, **extrakwargs)


#===============================================================================
# Database Custom Fields Classes definitions
#===============================================================================
class MutableList(Mutable, list):
    """
    A `list` type that implements SQLAlchemy :class:`Mutable`. More or less
    similar to :class:`MutableDict` in :module:`sqlalchemy.ext.mutable`.
    """
    def __setitem__(self, index, value):
        """Detect list set events and emit change events."""
        list.__setitem__(self, index, value)
        self.changed()

    def __delitem__(self, index):
        """Detect list del events and emit change events."""
        list.__delitem__(self, index)
        self.changed()

    def __setslice__(self, i, j, sequence):
        """Detect list set slice events and emit change events."""
        list.__setslice__(self, i, j, sequence)
        self.changed()

    def __delslice__(self, i, j):
        """Detect list del slice events and emit change events."""
        list.__delslice__(self, i, j)
        self.changed()

    def append(self, value):
        """Detect list append calls and emit change events."""
        list.append(self, value)
        self.changed()

    def remove(self, value):
        """Detect list remove calls and emit change events."""
        list.remove(self, value)
        self.changed()

    def pop(self, index=-1):
        """Detect list pop calls and emit change events."""
        list.pop(self, index)
        self.changed()

    def extend(self, value):
        """Detect list extend calls and emit change events."""
        list.extend(self, value)
        self.changed()

    @classmethod
    def coerce(cls, key, value):
        """
        Convert `value` plain :class:`list` instances to :class:`MutableList`
        objects.
        """
        if not isinstance(value, MutableList):
            if isinstance(value, list):
                return MutableList(value)
            return Mutable.coerce(key, value)
        else:
            return value


class UnicodeTextAsText(types.TypeDecorator):
    """
    Safely coerce Python byte-strings to Unicode before passing off to the
    database UnicodeText field type, and the other way (database Unicode to
    Python byte-strings).
    
    X-NOTE: This is needed because of the need to specify big lengths for some
    fields, like protein sequences and JSON fields. Text field type cannot
    handle it, but UnicodeText field type can.
    """
    impl = types.UnicodeText
    
    def process_bind_param(self, value, dialect):
        if isinstance(value, str): #Avoid problems with not string objects at this point
            value = value.decode('utf8') # str -> Unicode.
        return value
    
    def process_result_value(self, value, dialect):
        if value: #Avoid problems with None/Null values from database
            value = value.encode('utf8') # Unicode -> str.
        return value


class JSONDictField(types.TypeDecorator):
    """
    Class for a custom SQLAlchemy JSON dict field, whose data is available as a
    dictionary-like object in record instances.

    X-NOTE: Derived from the one described at:
    http://docs.sqlalchemy.org/en/rel_0_9/orm/extensions/mutable.html
    """
    __emulated_cls__ = dict
    impl = UnicodeTextAsText
    
    def process_bind_param(self, value, dialect):
        if value:
            return json.dumps(value) # dict-like -> JSON string -> Unicode
        else:
            return None
    
    def process_result_value(self, value, dialect):
        if value:
            return json.loads(value) # Unicode -> JSON string -> dict-like
        else:
            return self.__emulated_cls__()

MutableDict.associate_with(JSONDictField)


class JSONListField(types.TypeDecorator):
    """
    Class for a custom SQLAlchemi JSON list field, whose data is available as a
    list-like object in record instances.
    X-NOTE: Sub-classing of :class:`JSONDictField` doesn't work, so copy-paste-rename.

    X-NOTE: Derived from the one described at:
    http://docs.sqlalchemy.org/en/rel_0_9/orm/extensions/mutable.html
    """
    __emulated_cls__ = list
    impl = UnicodeTextAsText
    
    def process_bind_param(self, value, dialect):
        if value:
            return json.dumps(value) # list-like -> JSON string -> Unicode
        else:
            return None
    
    def process_result_value(self, value, dialect):
        if value:
            return json.loads(value) # Unicode -> JSON string -> list-like
        else:
            return self.__emulated_cls__()

MutableList.associate_with(JSONListField)


#===============================================================================
# Database Mixin Classes definitions
#===============================================================================
class DictField2AttrsMixin(object):
    """
    Mixin to make a database field dictionary (:attr:``__dictfield2attrs__``)
    available as different attributes.
    """
    __dictfield2attrs__ = '' #Set this to some attribute name in sub-classes.
    
    def __init__(self, *args, **kwargs):
        super(DictField2AttrsMixin, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()
    
    def __init_dictfield2attrs__(self):
        """
        :caution: When object is created (not loaded from database) the JSON
        field can be None, so fix this by calling this method from the sub-
        classes :method:``__init__``.
        """
        if getattr(self, self.__dictfield2attrs__, None) is None:
            setattr( self, self.__dictfield2attrs__, dict() )
    
    def __getattr__(self, attr):
        """
        Make dictionary keys/tokens accessibles as object attributes.
        
        X_NOTE: maybe a mechanism or other implementation is needed to ensure
        no collision between keys in the database field dictionary and
        sub-classes real attributes/properties/methods.
        """
        jsonfield_dict = getattr(self, self.__dictfield2attrs__)
        return jsonfield_dict[attr]


class IdNameExtradataMixin(DictField2AttrsMixin):
    """
    Mixin for very basic db-model classes with db-fields:
        :attr:``id`` (int)
        :attr:``name`` (str)
        :attr:``extradata`` (dict)
    """
    __dictfield2attrs__ = 'extradata'
    
    id = Column(Integer, Sequence('idnamedesc_id_seq'), primary_key=True,
                autoincrement=True)
    name = Column( String(250) )
    extradata = Column( JSONDictField(length=2**21) ) #Allow 2MB of JSON codified extra-data

    def __init__(self, *args, **kwargs):
        super(IdNameExtradataMixin, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__() #Fix attribute access of :attr:`extradata` keys (see :super-class:`DictField2AttrsMixin`)


class PepLikeMixin(object):
    """
    Mixin for Peptide-Like classes.
    """
    _db_modifications = None #The linked PTMs. In sub-classes it should be a set, like { Modification(), ... }; usually a SQLAlchemy relationship( collection_class=set ).
    seq = None #The aminoacid sequence. In sub-classes it should be a string; usually a SQLAlchemy Column( String(length) ).
    _db_seq_w_mods = None #The aminoacid sequence with the PTM IDs (ONLY for Significant and Clear Physiological ones; Ex.:'MS(21)K(121)AK(1)AR'). In sub-classes it should be a string; usually a SQLAlchemy Column( String(length) ).
#     _modification_cls = None #In subclasses it should be a class derived from :class:`ModificationMixin` #TODO: or not??
#     _annotations = None #In subclasses it should be { obj, ... } #X_NOTE: If this is added, consider uncommenting :property:`annotations`, :method:`add_annotation`, :staticmethod:`_overlap`, :method:`iter_annotations_slice` and :method:`annotations_slice`; and also check ProtRegion equivalents.
    
    def __get_seq_aa__(self, bio_pos):
        """
        :param int bio_pos: a biological position (start at position 1).
        """
        if bio_pos > 0: #Correct from Python start of sequences at 0:
            py_pos = bio_pos - 1
        elif bio_pos < 0:
            py_pos = bio_pos
        else:
            raise IndexError('No position 0 in biological sequences')
        return ( self.seq[py_pos], self.pos2modifications[bio_pos] )
    
    def __get_seq_slice__(self, bio_slice):
        """
        :param slice bio_slice: a slice of biological positions ([start - end] both included, and with start at position 1).
        """
        #IMPROVE: slice positions checks like in :method:`__get_seq_aa__`.
        return self.seq[ bio_slice.start - 1 : bio_slice.stop ]
    
    def __getitem__(self, key):
        if not isinstance(key, slice): # `key` is a slice of biological positions ([start - end] both included, and with start at position 1):
            return self.__get_seq_aa__(key)
        else: # `key` is assumed to be a biological position (start at position 1):
            return self.__get_seq_slice__(key)
    
    @staticmethod
    def _iter_modifications_slice(peplike, start, end, omit_ptmids=None):
        """
        :param object peplike: a :class:`PepLikeMixin` like instance.
        :param int start: start point of the [`start`, `end`] slice interval.
        :param int end: end point of the [`start`, `end`] slice interval.
        :param iterable or None omit_ptmids: the PTM IDs that must be excluded 
        from the resulting slice. Preferably a set. Defaults to None (no filtering).

        :return generator : yields :class:`ProtModification` instances from the
        supplied `peplike` object whose positions are within the [`start`,
        `end`] interval, and whose IDs are not in `omit_ptmids` (if provided).
        """
        if omit_ptmids:
            omit_ptmids = set(omit_ptmids)
            return ( modification for modification in peplike._db_modifications
                     if (start <= modification.position <= end and
                         modification.mod_type.id not in omit_ptmids) )
        else:
            return ( modification for modification in peplike._db_modifications
                     if start <= modification.position <= end )

    @hybrid_property #Read-only property
    def length(self):
        return len(self.seq)
    
    @hybrid_property #Read-only property
    def modifications(self):
        """
        ALL linked Modifications (Significant and Not significant,
        Physiological or Not physiological, ...).
        
        :return set: { Modification(), ... }
        """
        return self._db_modifications
    
    def filtered_modifications(self, only_significant=True, only_physio=True):
        """
        Filter the peptide-like Modifications.
        
        :param bool only_significant: True -> only Significant PTMs are 
        considered, False -> all PTMs are considered. Defaults to True.
        :param bool only_physio: True -> only PTMs with clear physiological 
        origin are considered, False -> all PTMs are considered. Defaults to True.
        """
        not_only_sig = (not only_significant)
        if only_physio:
            excluded_ids = NO_PHYSIO_MDDIDS #IMPROVE! Try to avoid the global...
        else:
            excluded_ids = set()
        return { mod for mod in self._db_modifications 
                 if (not_only_sig or mod.significant) and 
                    (mod.mod_type.id not in excluded_ids) }
    
    @property #Read-only property
    def sig_modifications(self):
        """
        Only Significant and clear Physiological modifications.
        
        :return set: { Modification(id=ModificationType(only_physio=True), significant=True), ... }
        """
        return self.filtered_modifications(only_significant=True, only_physio=True)
    
    def _build_pos2modifications(self, pos_shift=0, only_significant=True, 
                                 only_physio=True, pos_sorted=False):
        """
        Returns the sequence positions which contains PTMs, and those PTMs, as
        a dictionary. Defaults to report ONLY Significant and clear
        Physiological modifications.
        
        :param int pos_shift: left position shift from the position specified 
        in the :class:`Modification` instance. It's a decremental value.
        :param bool only_significant: True -> only Significant PTMs are 
        considered, False -> all PTMs are considered. Defaults to True.
        :param bool only_physio: True -> only PTMs with clear physiological 
        origin are considered, False -> all PTMs are considered. Defaults to True.
        :param bool pos_sorted: True -> return a :class:``OrderedDict`` 
        instance where positions (keys) are sorted ascending; False -> return 
        a :class:``defaultdict`` instance without sorting. Defaults to False.
        
        :return dict : a :class:``defaultdict`` or :class:``OrderedDict``
        instance of the kind { position : { ProtModification(), ... } }
        calculated from :attr:``_db_modifications``.
        """
        pos2modifications = defaultdict(set)
        for ptm in self.filtered_modifications(only_significant, only_physio):
            pos2modifications[ptm.position - pos_shift].add(ptm)
        #
        if not pos_sorted:
            return pos2modifications #An unsorted :class:``defaultdict`` instance.
        else:
            return OrderedDict(sorted( pos2modifications.items() )) #A position sorted :class:``OrderedDict`` instance.
    
    #Read-only property:
    pos2modifications = property(_build_pos2modifications, 
                                 doc='defaultdict : { position in seq : { Modification(id=ModificationType(only_physio=True), significant=True), ... } }')
    
    @property
    def pos2allmodifications(self):
        """
        Returns the sequence positions which contains ALL (significant and non-
        significant, physiological and not physiological) PTMs, as a dictionary
        (a :class:`defaultdict` instance).
        
        :return defaultdict : { position in seq : { Modification(id=ModificationType(only_physio=True), significant=True), ... } }
        """
        # Calls :method:`_build_pos2modifications` with the parameters:
        #  pos_shift=0, only_significant=False, only_physio=False, pos_sorted=False :
        return self._build_pos2modifications(only_significant=False, 
                                             only_physio=False)
    
    def _build_modid2positions(self, pos_shift=0, only_significant=True, 
                               only_physio=True):
        """
        :param int pos_shift: position shift from the position specified in
        the :class:`Modification` instance. It's a decremental value.
        :param bool only_significant: True -> only Significant PTMs are 
        considered, False -> all PTMs are considered. Defaults to True.
        :param bool only_physio: True -> only PTMs with clear physiological 
        origin are considered, False -> all PTMs are considered. Defaults to 
        True.
        
        :return dict : a :class:`dict` instance of the kind 
        { ModificationType().id : [ sorted positions in sequence ] }
        calculated from :attr:``_db_modifications``.
        """
        modid2positions = defaultdict(set)
        for ptm in self.filtered_modifications(only_significant, only_physio):
            modid2positions[ptm.mod_type.id].add(ptm.position - pos_shift)
        return { modid: sorted(positions) 
                 for modid, positions in modid2positions.items() }
    
    #Read-only property:
    modid2positions = property(_build_modid2positions, 
                               doc='dict : { ModificationType(only_physio=True).id : [ positions in seq ] }')
    
    @staticmethod
    def seq2seq_w_mods(seq, pos2modifications, seps=("(", ", ", ")"), 
                       abbreviations=False):
        """
        Get the sequence with modifications indicated in parentheses:
          - `abbreviations` is False:  'ABBCCC' -> 'A(21)BB(1, 121)CCC'
          - `abbreviations` is True :  'ABBCCC' -> 'A(phos)BB(ac, ub)CCC'
        
        :param str seq: amino acid sequence.
        :param dict pos2modifications: { position in seq : { Modification(), ... } }
        :param tuple seps: separators to use to add the PTMs to the sequence.
        :param bool abbreviations: use modification abbreviations when set to 
        True, otherwise use the modification IDs. Defaults to False 
        (modifications IDs).
        
        :return str : the sequence with modification numbers indicated in
        brackets, or None if no sequence is supplied.
        """
        if seq: #Avoid empty strings
            if pos2modifications:
                if not abbreviations:
                    get_modtype = lambda mods:\
                                    map( str, sorted( {mod.mod_type.id 
                                                       for mod in mods} ) )
                else:
                    get_modtype = lambda mods:\
                                    sorted( {mod.mod_type.extradata['abbreviation']
                                             for mod in mods} )
                open_sep, mid_sep, close_sep = seps
                seq_aas = list(seq)
                for pos, mods in pos2modifications.items():
                    seq_aas[pos - 1] += (open_sep + 
                                         mid_sep.join( get_modtype(mods) ) + 
                                         close_sep)
                return "".join(seq_aas)
            else:
                return seq
        #
        return None
    
    @staticmethod
    def seq2proforma(seq, pos2modifications, descriptor='database_acc', 
                     desc_src='unimod', prefix_tag=True):
        """
        Get the sequence with modifications in ProForma format
        (https://github.com/topdownproteomics/ProteoformNomenclatureStandard):
          - `descriptor`=='database_acc': 'ABBCCC' -> '[Unimod]+A[21]BB[1][121]CCC'
          - `descriptor`=='name':         'ABBCCC' -> 'A[Phospho]BB[Acetyl, GG]CCC'
        
        :param str seq: amino acid sequence.
        :param dict pos2modifications: { position in seq : { Modification(), ... } }
        :param str descriptor: modification "tag descriptor". Describes how the 
        modifications should be represented to form a square barked "modification 
        tag". By now, only 'name', 'database_acc' and 'mass' are supported.
        Defaults to 'database_acc' (UniMod ID).
        :param str desc_src: data source for the modification tag's key/value 
        pairs to use according to the selected "tag descriptor". By now, only 
        'Unimod' is supported. Defaults to 'Unimod'.
        :param bool prefix_tag: whether to use (True) or not (False) the 
        corresponding key of the "tag descriptor" as a prefix of the ProForma
        representation; only if the same "tag descriptor" is used for all the 
        "modification tags" (the only option supported by now). 
        
        :return str : the sequence in ProForma format (with modification 
        descriptors indicated in square brackets), or None if no sequence is 
        provided.
        """
        if seq: #Avoid empty strings
            if pos2modifications:
                if descriptor == 'database_acc':
                    if desc_src == 'unimod':
                        mod_attr = 'id' #also works with 'unimod_id'
                        mod_key = 'Unimod'
                elif descriptor == 'name':
                    if desc_src == 'unimod':
                        mod_attr = 'name' #also works with 'unimod_name'
                        mod_key = '' #Unimod modification names don't need a key.
                elif descriptor == 'mass':
                    mod_attr = 'delta_mass'
                    mod_key = 'mass'
                else:
                    raise ValueError("By now, only 'name', 'database_acc' and "
                                     "'mass' are supported as `descriptor` values")
                if prefix_tag and mod_key:
                    global_tmplt = "[" + mod_key + "]+{}"
                    mod_key = ''
                else:
                    global_tmplt = "{}"
                if mod_key:
                    mod_tmplt = mod_key + ":{}"
                else:
                    mod_tmplt = "{}"
                #
                pos2things = { pos: 
                               map( mod_tmplt.format, 
                                    sorted(set( getattr(mod.mod_type, mod_attr) 
                                                for mod in mods) )) 
                               for pos, mods in pos2modifications.items() }
                #
                return global_tmplt.format( 
                         ppf.merge_seq_and_things( seq, pos2things, ('[', None, ']') )
                                           )
            else:
                return seq
        #
        return None

    @staticmethod
    def seq_w_mods2seq( seq_w_mods, seps=('(', ')') ):
        """
        Get the plain sequence from a peptide sequence with modifications
        indicated in square brackets:
          'A(21)BB(23, 1)CCC' -> 'ABBCCC'

        :param str seq_w_mods: a peptide sequence with modifications indicated
        in square brackets.
        :param tuple seps: separators used by the PTMs in the sequence.

        :return str : the plain sequence.
        """
        open_sep, close_sep = seps
        seq = list()
        append = True
        for letter in seq_w_mods:
            if letter == open_sep:
                append = False
            elif letter == close_sep:
                append = True
            elif append:
                seq.append(letter)
        return "".join(seq)
#     @staticmethod
#     def seq_w_mods2seq2(seq_w_mods):
#         return "".join( aa for aa in seq_w_mods if aa.isalpha() ) #X-NOTE: this seemed slower than the previous one... :-?
    
    def refresh_seq_w_mods(self):
        """
        Refresh the peptide database sequence with modification annotations
        (:attr:`_db_seq_w_mods`) from :attr:`seq` + PTMs from
        :attr:`pos2modifications` using :staticmethod:`seq2seq_w_mods`.
        """
        self._db_seq_w_mods = self.seq2seq_w_mods(self.seq, self.pos2modifications)
    
    @hybrid_property #For use in instances:
    def seq_w_mods(self):
        """
        Sequence with the Modification IDs included, ONLY for Significant and 
        Clear Physiological ones. Ex.: 'MS(21)K(121)AK(1)AR'
        Database-memoized property to get the value from the database, or from
        :method:`refresh_seq_w_mods`.
        """
        if not self._db_seq_w_mods:
            self.refresh_seq_w_mods()
        return self._db_seq_w_mods
    @seq_w_mods.expression #For use in SQLAlchemy SQL queries with the class:
    def seq_w_mods(cls):
        return cls._db_seq_w_mods
    
    @property
    def seq_w_abbreviatedmods(self):
        """
        Sequence with the Modification Abbreviations included, ONLY for
        Significant and Clear Physiological ones. Ex.: 'MS(phos)K(ub)AK(ac)AR'
        """
        return self.seq2seq_w_mods(self.seq, self.pos2modifications, 
                                   abbreviations=True)
    
    @property
    def seq_w_mods_incl_nophysio(self):
        """
        Sequence with the modification IDs included, ONLY for Significant ones,
        but INCLUDING No Clear Physiological ones. Ex.: 'M(35)S(21)K(121)AK(1)AR'
        """
        return self.seq2seq_w_mods(self.seq, 
                                   self._build_pos2modifications(only_significant=True, 
                                                                 only_physio=False) )
    
    @property
    def seq_w_allmods(self):
        """
        Sequence with the Modification IDs included, for ALL the linked
        Modifications. Ex.: 'M(35)S(21)K(1, 121)AK(1, 121)AR'
        """
        return self.seq2seq_w_mods(self.seq, self.pos2allmodifications)
    
    def as_proforma(self, descriptor='database_acc'):
        """
        Get the sequence with modifications in ProForma format
        (https://github.com/topdownproteomics/ProteoformNomenclatureStandard):
          - `descriptor`=='database_acc': 'ABBCCC' -> '[Unimod]+A[21]BB[1][121]CCC'
          - `descriptor`=='name':         'ABBCCC' -> 'A[Phospho]BB[Acetyl, GG]CCC'
        
        :param str seq: amino acid sequence.
        :param dict pos2modifications: { position in seq : { Modification(), ... } }
        :param str descriptor: modification "tag descriptor". Describes how the 
        modifications should be represented to form a square barked "modification 
        tag". By now, only 'name', 'database_acc' and 'mass' are supported.
        Defaults to 'database_acc' (UniMod ID).
        """
        return self.seq2proforma(self.seq, self.pos2modifications, descriptor)
    
    def get_mod_aas(self, only_significant=True, only_physio=True):
        """
        Calculates the number of modified amino acids.
        
        :param bool only_significant: True -> Only Significant PTMs are 
        considered, False -> ALL PTMs are considered. Defaults to True.
        :param bool only_physio: True -> only PTMs with clear physiological 
        origin are considered, False -> all PTMs are considered. Defaults to True.
        
        :return int : number of modified amino acids (AAs with any PTM).
        """
        return len( { ptm.position for ptm in 
                      self.filtered_modifications(only_significant, only_physio) } )
    
    #Read-only property:
    mod_aas = property(get_mod_aas, 
                       doc='Number of amino acids ONLY with Significant and Clear Physiological PTMs')
    
    def is_comodiffied(self):
        """
        Indicates whether the PepLike object has 2 or more significant
        physiological PTMs, be these of the same type or not.
        
        :return bool : True whether the PepLike object has 2 or more significant
        physiological PTMs, otherwise False.
        """
        return (self.mod_aas > 1 or 
                sum(map( len, self.modid2positions.values() )) > 1)
    
    def is_ntermonly(self):
        """
        The peptide can be located only in the N-term of a protein if its first
        amino acid is acetylated and is not a Lys (K).
        
        :return bool : True whether the Peptide is supposed to be N-term,
        otherwise False.
        """
        for mod in self.pos2modifications.get( 1, set() ):
            if mod.mod_type.id == 1 and mod.aa != 'K': #Acetylation -> mod_type.id == 1
                return True
        return False

    def add_mod_type(self, position, mod_type, mod_cls=None, db_session=None):
        """
        :param int position: biological position (start at 1) where to add the
        modification.
        :param mod_type: a :class:`ModificationType` instance or an integer
        (corresponding to a :class:`ModificationType`.id).
        :param class mod_cls: a class derived from :class:`ModificationMixin`.
        Defaults to :class:`ProtModification`.
        :param Session session: a SQLAlchemy database :class:`Session` instance.

        :return object : the added modification.
        """
        # `mod_type` can be a ModificationType.id (integer) or a
        # :class:`ModificationType` instance, but we need it to be always a
        # :class:`ModificationType` instance:
        if isinstance(mod_type, int):
            if db_session is not None: #Get :class:`ModificationType` instance from the DB session:
                mod_type = db_session.query(ModificationType).get(mod_type)
            else: #Create a new instance of :class:`ModificationType`:
                mod_type = ModificationType(id=mod_type)
        # Default mod_cls is :class:`ProtModification`:
        if mod_cls is None:
            mod_cls = ProtModification
        #
        modification = mod_cls( mod_type=mod_type, position=position )
        self._db_modifications.add(modification)
        #
        return modification

    def add_modification(self, modification, position=None):
        """
        :param modification: an instance of a class derived from 
        :class:`ModificationMixin`.
        :param int position: biological position (start at 1) where to add the
        modification, if the modifications doesn't have one. Defaults to None.

        :return object : the added modification.
        """
        if position and not modification.position:
            modification.position = position
        self._db_modifications.add(modification)
        #
        return modification
    
    def add_modifications(self, modifications):
        """
        :param iterable modifications: instances of a class derived from
        :class:`ModificationMixin`.
        """
        self._db_modifications.update(modifications)
    
    def remove_modification(self, modification):
        """
        Remove a modification from :attr:``_db_modifications``.

        :param modification: an instance of a class derived from 
        :class:`ModificationMixin`.
        """
        self._db_modifications.remove(modification)
    
#     @property
#     def annotaions(self):
#         return self._annotations
#     
#     def add_annotation(self, start, end, annotation):
#         """
#         Check and store annotations in :property:`annotations`.
# 
#         :param int start: annotation start position.
#         :param int end: annotation end position.
#         :parma obj annotation: an annotation object.
#         """
#         if end >= start:
#             self.annotations.append( (start, end, annotation) )
#         else:
#             raise ValueError('end-point is smaller than start-point')
#         return self.annotations
# 
#     @staticmethod
#     def _overlap(start1, end1, start2, end2):
#         start = max(start1, start2)
#         end = min(end1, end2)
#         if (end-start) >= 0:
#             return (start, end)
#         else:
#             return None
#     
#     def iter_annotations_slice(self, start, end):
#         """
#         :param int start: slice interval start
#         :param int end: slice interval end
# 
#         :return generator : yields (start, end, annotation)
#         """
#         for start_ann, end_ann, annotation in self.annotations:
#             overlap = self._overlap(start_ann, end_ann, start, end)
#             if overlap:
#                 yield overlap[0], overlap[1], annotation
#     
#     def annotations_slice(self, start, end):
#         return set( self.iter_annotations_slice(self.annotations, start, end) )


class ModificationMixin(object):
    """
    Mixin class to use in Modification db-model classes. Provides db-fields:
        :attr:``id``,
        :attr:``position`
        :attr:``_db_aa``
        :attr:``ascore``
        :attr:``significant``
    As well as the ForeignKey relationship :attr:`mod_type` (one -> None with 
    db-model :class:`ModificationType`), and the properties `aa` and `parent`.
    """
    id = Column(Integer, Sequence('modification_id_seq'), primary_key=True,
                autoincrement=True)
    position = Column(Integer) # Biological sequence position.
    _db_aa = Column( "aa", String(1) ) # Modified AA. Memoized database field.
    ascore = Column(Float)
    significant = Column(Boolean, index=True) # True, False, or None -> Indicates whether :attr:`ascore` has a significant value (True for values >= than the corresponding `Source` threshold for the corresponding `ModificationType`, otherwise False), or it cannot be determined (None). Used to speed-up queries.

    def __init__(self, *args, **kwargs):
        super(ModificationMixin, self).__init__(*args, **kwargs)

    @property
    def parent(self):
        raise NotImplementedError("Sub-class should point `parent` property to "
                                  "the real parent `relationship()`.")
        # return self.parent_relationship
    @parent.setter
    def parent(self, peplike):
        raise NotImplementedError("Sub-class should point `parent` property to "
                                  "the real parent `relationship()`.")
        # self.parent_relationship = peplike

    @hybrid_property #For use in instances:
    def aa(self):
        """
        Database-memoized hybrid property.
        """
        if ( not self._db_aa and self.position and getattr(self.parent, 'seq', None) ):
            self._db_aa = self.parent.seq[self.position - 1] #:attr:`position` starts at 1 (biological sequences).
        return self._db_aa
    @aa.expression #For use in SQLAlchemy SQL queries with the class:
    def aa(cls):
        return cls._db_aa

    # Relationships:
    # - one-to-None: mod_type -> ModificationType :
    @declared_attr
    def mod_type_id(self):
        # :attr-field:`mod_type_id` -> :attr:`id` of a :class`ModificationType` instance:
        return Column( Integer, ForeignKey(ModificationType.id) )
    @declared_attr
    def mod_type(self):
        # :attr:`mod_type` -> a :class`ModificationType` instance:
        return relationship(ModificationType)
    
    @property
    def only_physio(self):
        return self.mod_type.only_physio


#===============================================================================
# Database Model Classes definitions
#===============================================================================
class Organism(Base, IdNameExtradataMixin, ReprMixin):
    """
    Basic class describing organisms.
    """
    __tablename__ = 'organisms'
    __repr_init_attr__ = ('id', 'name')

    # Back-references:
    # - many-to-one: proteins (set) <-> Protein.organism :
    proteins = relationship('Protein', collection_class=set,
#                             cascade_backrefs=False,
                            back_populates='organism')
    # - many-to-one: cell_lines (set) <-> CellLine.organism :
    cell_lines = relationship('CellLine', collection_class=set,
                              cascade_backrefs=False, 
                              back_populates='organism')

    def __init__(self, *args, **kwargs):
        super(Organism, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()


class CellLine(Base, IdNameExtradataMixin, ReprMixin):
    """
    Cell lines where the :class:`ProtModification` and :class:`Evidence`
    instances have been found or predicted.
    """
    __tablename__ = 'celllines'
    __repr_init_attr__ = ('name', 'organism')

    # Relationships:
    # - one-to-many: organism <-> Organism.cell_lines (set) :
    organism_id = Column(Integer, ForeignKey(Organism.id))
    organism = relationship('Organism', back_populates='cell_lines')
    # Back-references:
    # - many-to-many: protmodifications (set) <-> ProtModification.cell_lines (set) :
    protmodifications = relationship('ProtModification', 
                                     secondary='protmodifications_celllines', 
                                     collection_class=set, 
                                     back_populates='cell_lines')
    # - many-to-one: supraexperiments (set) <-> SupraExperiment.cell_lines (set) :
    supraexperiments = relationship('SupraExperiment', 
                                    collection_class=set, 
                                    cascade_backrefs=False, 
                                    back_populates='cell_line')

    def __init__(self, *args, **kwargs):
        super(CellLine, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()


class SourceType(Base, IdNameExtradataMixin, ReprMixin):
    """
    Basic class for the different kinds of Internet Database Repositories
    (ex. gene repository, protein repository, ms data repository, ...)
    """
    __tablename__ = 'sourcetypes'
    __repr_init_attr__ = ('id', 'name')

    # Back-references:
    # - many-to-many: sources (set) <->  Source.source_types (set) :
    sources = relationship('Source', secondary='sources_sourcetypes',
                           collection_class=set,
                           back_populates='source_types')

    def __init__(self, *args, **kwargs):
        super(SourceType, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()


class Source(Base, IdNameExtradataMixin, ReprMixin):
    """
    Basic class describing data (Proteins, MSPeptides, ...) repositories/Sources.
    """
    __tablename__ = 'sources'
    __repr_init_attr__ = ('id', 'name', 'version')

    version = Column( String(50) ) #The version of the source/repository, if available (usually if the source is a database or is generated from one); otherwise, the year of publication of the dataset. E.g., '2016'.
    url = Column( String(300) ) #URL link to the place where the dataset can be downloaded, or where it's referenced (e.g., 'http://www.sciencedirect.com/science/article/pii/S1874391915301731').
    ptmid2threshold = Column(JSONDictField) #A dictionary where the keys are the UniMod ID for the PTMs whose position probability as been provided in the source dataset, and the values are the thresholds used for each PTM.The special key 'default' can be specified for a default threshold that applies to the PTM IDs not listed. E.g., '{"21": 19.0}', '{"121": 0.75}'. '{"default": 75.0}'.

    # Relationships:
    # - many-to-many: source_types (set) <->  SourceType.sources (set) :
    source_types = relationship('SourceType', secondary='sources_sourcetypes',
                                collection_class=set,
                                back_populates='sources')

    # Back-references:
    # - many-to-one: direct_proteins (set) <-> Protein.source_repository :
    direct_proteins = relationship('Protein', collection_class=set,
                                   back_populates='source_repository')
    # - many-to-one: _db_protein2repositories { 'prot_id_in_repo': Protein2Repository() } :
    _db_protein2repositories = relationship('Protein2Repository',
                                            collection_class=attribute_mapped_collection('prot_id_in_repo'),
                                            back_populates='repository')
    # Association-proxies:
    # - linked_proteins { 'prot_id_in_repo': Protein() } :
    linked_proteins = association_proxy('_db_protein2repositories',
                                        'protein',
                                        creator=lambda key, value:
                                            Protein2Repository(prot_id_in_repo=key,
                                                               protein=value)
                                        )

    def __init__(self, *args, **kwargs):
        kwargs.setdefault( 'ptmid2threshold', dict() ) #Set default for :attr:`ptmid2threshold` when object is created (not loaded from database).
        super(Source, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()

    @reconstructor
    def __init_on_dbload__(self): #IMPROVE: maybe it would be a good idea to create a JSONDictField for dictionaries with integer key values...
        """
        Convert the keys in :attr:`ptmid2threshold` (modification types IDs)
        to integers again if they where integers before JSON encode and decode.
        Ex.: {u'1': 0.7, u'default': 0.9} -> {1: 0.7, u'default': 0.9}
        """
        json_dict = self.ptmid2threshold
        if json_dict:
            self.ptmid2threshold = { int(key) if key.isdigit() else key: value
                                     for key, value in json_dict.iteritems() }

@event.listens_for(Source, 'refresh')
def source_obj_refreshed(source_obj, context, attrs): #IMPROVE: maybe it would be a good idea to create a field-class like JSONDictField but for dictionaries with integer key values...
    """
    Catch SQLAlchemy Refresh events on :class:`Source` instances, to correct
    their :attr:`ptmid2threshold` after it is loaded again from Database and
    JSON decoded.
    X_NOTE: see http://stackoverflow.com/a/42893258
    """
    if attrs is None or 'ptmid2threshold' in attrs:
        Source.__init_on_dbload__(source_obj)


source2sourcetype = Table('sources_sourcetypes', Base.metadata,
    Column('source_id', Integer, ForeignKey(Source.id), nullable=False),
    Column('sourcetype_id', Integer, ForeignKey(SourceType.id), nullable=False)
                          )


class SupraExperiment(Base, DictField2AttrsMixin, ReprMixin):
    """
    Contains information about the Supar-Experiment/Conditions/Group of
    experiments with the same biological treatments.
    """
    __tablename__ = 'supraexperiments'
    __dictfield2attrs__ = 'extradata'
    __repr_init_attr__ = ('id', 'name')
    
    id = Column(String(20), Sequence('supraexp_idname_seq'), primary_key=True)
    name = Column( String(250) )
    extradata = Column( JSONDictField(length=2**21) ) #Allow 2MB of JSON codified extra-data

    # Relationships:
    # - one-to-None: source -> Source :
    source_id = Column(Integer, ForeignKey(Source.id) )
    source = relationship('Source')
    # - one-to-many: cell_line <-> CellLine.supraexperiments (set) :
    cell_line_id = Column( Integer, ForeignKey(CellLine.id) )
    cell_line = relationship('CellLine', 
                             back_populates='supraexperiments')

    # Back-references:
    # - many-to-one: experiments (set) <-> Experiment.supraexp :
    experiments = relationship('Experiment', collection_class=set, 
                               cascade_backrefs=False, 
                               back_populates='supraexp')

    def __init__(self, *args, **kwargs):
        super(SupraExperiment, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__() #Fix attribute access of :attr:`extradata` keys (see :super-class:`DictField2AttrsMixin`)


class Experiment(Base, DictField2AttrsMixin, ReprMixin):
    """
    Experiment information
    """
    __tablename__ = 'experiments'
    __dictfield2attrs__ = 'extradata'
    __repr_init_attr__ = ('id', 'name')
    
    id = Column(String(20), Sequence('exp_idname_seq'), primary_key=True)
    target_proteome = Column( String(20) ) #TODO: Would it be better to use an Enum()?
    name = Column( String(250) )
    extradata = Column( JSONDictField(length=2**21) ) #Allow 2MB of JSON codified extra-data
    ms_instrument = Column( String(45) )
    quant_type = Column( String(45) )
    quant_labeling = Column(JSONListField)
    quant_conditions = Column(JSONListField)
    date = Column(Date)
    
    # Relationships:
    # - one-to-many: supraexp <-> SupraExperiment.experiments (set) :
    supraexp_id = Column( String(20), ForeignKey(SupraExperiment.id) )
    supraexp = relationship('SupraExperiment', back_populates='experiments')
    
    # Back-references:
    # - many-to-many: mspeptides (set) <-> MSPeptide.experiments (set) :
    mspeptides = relationship('MSPeptide', secondary='mspeptides_experiments',
                              collection_class=set,
                              back_populates='experiments')

    def __init__(self, *args, **kwargs):
        super(Experiment, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__() #Fix attribute access of :attr:`extradata` keys (see :super-class:`DictField2AttrsMixin`)


class Evidence(Base, IdNameExtradataMixin, ReprMixin):
    """
    Evidences for everything
    """
    __tablename__ = 'evidences'
    __repr_init_attr__ = ('id', 'type', 'name')
    #X-NOTE: GO Evidence codes/types from GO Annotations Evidence codes: http://geneontology.org/page/guide-go-evidence-codes
    __TYPES__ = {'EXP': 'Inferred from Experiment',
                 'IC' : 'Inferred by Curator',
                 'IBA': 'Inferred from Biological aspect of Ancestor',
                 'IBD': 'Inferred from Biological aspect of Descendant',
                 'IDA': 'Inferred from Direct Assay',
                 'IEA': 'Inferred from Electronic Annotation',
                 'IEP': 'Inferred from Expression Pattern',
                 'IGC': 'Inferred from Genomic Context',
                 'IGI': 'Inferred from Genetic Interaction',
                 'IKR': 'Inferred from Key Residues',
                 'IMP': 'Inferred from Mutant Phenotype',
                 'IPI': 'Inferred from Physical Interaction',
                 'IRD': 'Inferred from Rapid Divergence',
                 'ISA': 'Inferred from Sequence Alignment',
                 'ISM': 'Inferred from Sequence Model',
                 'ISO': 'Inferred from Sequence Orthology',
                 'ISS': 'Inferred from Sequence or Structural Similarity',
                 'NAS': 'Non-traceable Author Statement',
                 'ND' : 'No biological Data available',
                 'NR' : 'Not Recorded',
                 'RCA': 'Inferred from Reviewed Computational Analysis',
                 'TAS': 'Traceable Author Statement',
                 }
    TYPE_KEYS = tuple( __TYPES__.keys() )    
    SIGNIFICANT_TYPEKEYS = ('EXP', 'IC', 'IDA', 'IEP', 'IMP', 'TAS')

#     type = Column(Enum( *__TYPES__.keys() ))
    type = Column( String(11) )
    linked_ids = Column(JSONDictField)

    # Back-references:
    # - many-to-many: protmodifications (set) <-> ProtModification.evidences (set) :
    protmodifications = relationship('ProtModification',
                                     secondary='protmodifications_evidences',
                                     collection_class=set,
                                     back_populates='evidences')
    # - many-to-one: goannotations (set) <-> GOAnnotation.evidence :
    goannotations = relationship('GOAnnotation', collection_class=set,
                                 back_populates='evidence')

    def __init__(self, *args, **kwargs):
        kwargs.setdefault( 'linked_ids', dict() ) #Set default for :attr:`linked_ids` when object is created (not loaded from database).
        super(Evidence, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()

    @validates('type')
    def validate_type(self, attr, value):
        if value.startswith('ECO:'):
            return value
        else:
            return validate_enum( self, attr, value, self.TYPE_KEYS )


class GOTerm(Base, IdNameExtradataMixin, ReprMixin):
    """
    Gene Ontology Term
    """
    __tablename__ = 'goterms'
    __repr_init_attr__ = ('id', 'go_term')
    # Ontology Key -> Ontology Description:
    __ONTOLOGIES__ = {'P': 'Biological process',
                      'F': 'Molecular function',
                      'C': 'Cellular component'}

    id = Column(String(10), Sequence('goterm_id_seq'), primary_key=True) #Ex.: 'GO:0000001'
    ontology = Column(Enum( *__ONTOLOGIES__.keys() ))
    
    # Relationships:
    # - one-to-None: source -> Source :
    source_id = Column( Integer, ForeignKey(Source.id) )
    source = relationship('Source')
    
    def __init__(self, *args, **kwargs):
        super(GOTerm, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()

    @property
    def go_term(self):
        if self.ontology is not None and self.name is not None:
            return '{0}:{1}'.format(self.ontology, self.name)
        else:
            return None
    @go_term.setter
    def go_term(self, go_term):
        if go_term is not None:
            self.ontology, _, self.name = go_term.partition(':')
        else:
            self.ontology = self.name = None

    @validates('ontology')
    def validate_ontology(self, attr, value):
        return validate_enum( self, attr, value, self.__ONTOLOGIES__.keys() )


class ModificationType(Base, IdNameExtradataMixin, ReprMixin):
    """
    Basic class for the different generic kinds of protein modifications (ex.
    phosporylation, acetylation, ...).
    The :attr:`id`, and :attr:`name` are based on UniMod (http://www.unimod.org).
    """
    __tablename__ = 'modificationtypes'
    __repr_init_attr__ = ('id', 'name')
    
    only_physio = Column(Boolean, index=True) # True or False -> Indicates whether this Modification Type is due to physiological processes (True) or not (False).
    
    def __init__(self, *args, **kwargs):
        super(ModificationType, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()


class MSPeptide(Base, PepLikeMixin, ReprMixin):
    """
    Mass Spectrometry Peptides. The matching peptides found by proteomic search
    engines. They are referenced by one or more :class:`PSM` instances and
    point to one or more :class:`Protein` instances (their sequences are found
    in one or more proteins).
    A Unique MSPeptide is defined by :attr:`seq_w_mods` and :attr:`source`.
    """
    __tablename__ = 'mspeptides'
    __repr_init_attr__ = ('id', 'seq_w_allmods', 'seq_w_mods', 'source')
    
    id = Column(Integer, Sequence('mspep_id_seq'), primary_key=True,
                autoincrement=True)
    _db_seq_w_allmods = Column(String(250), index=True) #The sequence annotated with ALL PTM IDs; Ex.:'M(35)S(21)K(1, 121)AK(1, 121)AR'.
    _db_seq_w_mods = Column(String(250), index=True) #Overwrites :property:`PepLikeMixin._db_seq_w_mods` with a DataBase Field (the sequence annotated with Significant and only Physiological PTM IDs; Ex.:'MS(21)K(121)AK(1)AR').
    seq = Column(String(75), index=True)
    _db_mod_aas = Column(Integer, index=True) #Cached number of Significant and clear Physiological modified amino acids. Added to speed up Database queries.
    _db_allphysiomods_signif = Column(Boolean, index=True) #Indicates whether ALL Clear Physiological PTMs are Significant (True) or not (False). `None` indicates both 'Not Set' or 'there is Not Any clear physiological PTM'.
    missing_cleavages = Column(Integer) #Number of missing cleavages that this peptide has (None if it's unknown).
    prots_from_source = Column(JSONListField) #Related proteins list  according to source repository/article.
    regulation = Column(JSONDictField) #TEST: provisional: {experiment ID: {statistic indicating regulation: {condition: 1 (Up), 0 (no-change) or -1 (Down), ...}, ...}, ...}
#     _db_mod_type_ids = Column(JSONListField, index=True) #Cached sorted list of Significant No physiological Modification type IDs. X-NOTE: Only add if more query speed up is required.
    
    # Relationships:
    # - one-to-None: source -> Source :
    source_id = Column( Integer, ForeignKey(Source.id) )
    source = relationship('Source')
    
    # Back-references:
    # - many-to-one: psms (set) <-> PSM.mspeptide :
    psms = relationship('PSM', collection_class=set, back_populates='mspeptide')
    # - many-to-one: _db_modifications (set) <-> MSPepModification.mspeptide :
    _db_modifications = relationship('MSPepModification', collection_class=set,
                                     cascade='all, delete-orphan',
                                     back_populates='mspeptide')
    #  - many-to-many: proteins (set) <-> Protein.mspeptides (set) :
    proteins = relationship('Protein', secondary='proteins_mspeptides',
                            collection_class=set,
                            back_populates='mspeptides')
    # - many-to-many: experiments (set) <-> Experiment.mspeptides (set) :
    experiments = relationship('Experiment', secondary='mspeptides_experiments',
                               collection_class=set,
                               back_populates='mspeptides')
    
    def __init__(self, *args, **kwargs):
        kwargs.setdefault( 'regulation', dict() ) #Set default for :attr:`regulation` when object is created (not loaded from database).
        kwargs.setdefault( 'prots_from_source', list() ) #Set default for :attr:`prots_from_source` when object is created (not loaded from database).
        super(MSPeptide, self).__init__(*args, **kwargs)
    
    @property
    def pos2sigmodifications(self):
        """
        Returns the sequence positions which contains ONLY Significant 
        (physiological and not physiological) PTMs, as a dictionary 
        (a :class:`defaultdict` instance).
        
        :return defaultdict : { position in seq : { Modification(id=ModificationType(only_physio=True), significant=True), ... } }
        """
        # Calls :method:`_build_pos2modifications` with the parameters:
        #  pos_shift=0, only_significant=True, only_physio=False, pos_sorted=False :
        return self._build_pos2modifications(only_significant=True, 
                                             only_physio=False)
    
    def refresh_mod_aas(self, only_significant=True, only_physio=True):
        """
        Update database number of modified amino acids.
        
        :param bool only_significant: True -> only Significant PTMs are 
        considered, False -> all PTMs are considered. Defaults to True.
        :param bool only_physio: True -> only PTMs with clear physiological 
        origin are considered, False -> all PTMs are considered. Defaults to True.
        """
        self._db_mod_aas = self.get_mod_aas(only_significant, only_physio)
    
    @hybrid_property #For use in instances:
    def mod_aas(self):
        """
        Database-memoized property to get the number of Significant modified
        amino acids from the Database if they are there, or, otherwise, from the
        associated modifications.
        """
        if self._db_mod_aas is None:
            self.refresh_mod_aas()
        return self._db_mod_aas
    @mod_aas.expression #For use in SQLAlchemy SQL queries with the class:
    def mod_aas(cls):
        return cls._db_mod_aas
    
    def refresh_seq_w_allmods(self):
        """
        Refresh the peptide database sequence with modification annotations
        (:attr:`_db_seq_w_allmods`) from :attr:`seq` + ALL the modifications 
        (from :attr:`pos2allmodifications`) using :staticmethod:`seq2seq_w_mods`.
        """
        self._db_seq_w_allmods = self.seq2seq_w_mods(self.seq, self.pos2allmodifications)
    
    @hybrid_property #For use in instances:
    def seq_w_allmods(self):
        """
        Sequence with ALL the Modification IDs included. Ex.: 'M(35)S(21)K(1,121)AK(1,121)AR'
        Database-memoized property to get the value from the database, or from
        :method:`refresh_seq_w_allmods`.
        Overrides :class:`PepLikeMixin` :property:`seq_w_allmods`.
        """
        if not self._db_seq_w_allmods:
            self.refresh_seq_w_allmods()
        return self._db_seq_w_allmods
    @seq_w_allmods.setter
    def seq_w_allmods(self, value):
        self._db_seq_w_allmods = value
    @seq_w_allmods.expression #For use in SQLAlchemy SQL queries with the class:
    def seq_w_allmods(cls):
        return cls._db_seq_w_allmods
    
    def refresh_allphysiomods_signif(self):
        """
        Refresh the peptide database :attr:`_db_allphysiomods_signif` taking
        into account ONLY the Clear Physiological Modifications: True -> ALL
        Clear Physiological Modification are Significant; False -> Any of them
        is Not significant; None -> there is Not Any clear physiological
        Modification.
        """
        physiomods_pos_id = set()
        significantmods_pos_id = set()
        for mod in self.modifications:
            mod_type_id = mod.mod_type.id
            if mod_type_id not in NO_PHYSIO_MDDIDS: #Exclude Unclear Physiological Modifications
                mod_pos_id = (mod.position, mod_type_id)
                physiomods_pos_id.add(mod_pos_id)
                if mod.significant:
                    significantmods_pos_id.add(mod_pos_id)
        #
        if physiomods_pos_id:
            self._db_allphysiomods_signif = (physiomods_pos_id==significantmods_pos_id)
        else:
            self._db_allphysiomods_signif = None
    
    @hybrid_property #For use in instances:
    def allphysiomods_signif(self):
        """
        Indicates whether ALL Clear Physiological Modification are Significant
        (True) or not (False). None indicates both 'Not Set' or
        'there is Not Any Clear Physiological Modification'.
        """
        if self._db_allphysiomods_signif is None and self.modifications:
            self.refresh_allphysiomods_signif()
        return self._db_allphysiomods_signif
    @allphysiomods_signif.expression #For use in SQLAlchemy SQL queries with the class:
    def allphysiomods_signif(cls):
        return cls._db_allphysiomods_signif
    
    def add_mod_type(self, position, mod_type, session=None):
        return super(MSPeptide, self).add_mod_type(position, mod_type,
                                                   mod_cls=MSPepModification,
                                                   db_session=session)


mspeptide2experiments = Table('mspeptides_experiments', Base.metadata,
    Column('mspeptide_id', Integer, ForeignKey(MSPeptide.id), nullable=False),
    Column('experiment_id', String(20), ForeignKey(Experiment.id), nullable=False)
                              )


class MSPepModification(Base, ModificationMixin, ReprMixin):
    """
    Modifications found on :class:`MSPeptide` instances. They aggregate Ascore
    data from the corresponding :class:`PSMModification` instances.
    """
    __tablename__ = 'mspepmodifications'
    __repr_init_attr__ = ('mod_type', 'position', 'mspeptide')

    # Relationships:
    # - one-to-many: mspeptide <-> MSPeptide._db_modifications (set) :
    mspeptide_id = Column( Integer, ForeignKey(MSPeptide.id) )
    mspeptide = relationship('MSPeptide', back_populates='_db_modifications')
    # - many-to-None: evidences (set) -> Evidence :
    evidences = relationship('Evidence',
                             secondary='mspepmodifications_evidences',
                             collection_class=set)

    # Back-references:
    # - many-to-many: protmodifications (set) <-> ProtModification.mspepmodifications (set) :
    protmodifications = relationship('ProtModification',
                                     secondary='protmods_mspepmods',
                                     collection_class=set,
                                     back_populates='mspepmodifications')

    @hybrid_property
    def parent(self):
        return self.mspeptide
    @parent.setter
    def parent(self, peplike):
        self.mspeptide = peplike


mspepmodification2evidence = Table('mspepmodifications_evidences', Base.metadata,
    Column('mspepmodification_id', Integer, ForeignKey(MSPepModification.id), nullable=False),
    Column('evidence_id', Integer, ForeignKey(Evidence.id), nullable=False)
                                   )


class Spectrum(Base, ReprMixin):
    """
    Mass Spectrometry Spectrum data.
    """
    __tablename__ = 'spectra'
    __repr_init_attr__ = ('id', 'ms_n', 'raw_file')

    id = Column(Integer, Sequence('spectra_id_seq'), primary_key=True,
                autoincrement=True)
    ms_n = Column(Integer) #MS level.
    has_mzs_intensities = Column(Boolean) #Indicates if :attr:`masses` and :attr:`intensities` contain data. For filtering.
    masses = deferred( Column(JSONListField(length=2**22)) ) #Allow almost 5MB of JSON encoded data
    intensities = deferred( Column(JSONListField(length=2**22)) ) #Allow almost 5MB of JSON encoded data
    raw_file = Column( String(200) ) #Original RAW file name.
    scan_i = Column(Integer) #Initial scan.
    scan_f = Column(Integer) #Final scan.
    rt = Column(Integer) #Retention time (min.).
    charge = Column(Integer) #Charge of the parent fragmented ion.
    parent_mass = Column(Float) #Mass of the parent fragmented ion.

    # Relationships:
    # - adjacency-list one-to-many: parent_spectrum <-> Spectrum.child_spectra (set) :
    parent_spectrum_id = Column( Integer, ForeignKey('spectra.id') )
    parent_spectrum = relationship('Spectrum', remote_side=[id],
                                   back_populates='child_spectra')
    # - one-to-None: experiment -> Experiment :
    experiment_id = Column( String(20), ForeignKey(Experiment.id) )
    experiment = relationship('Experiment')
#     # - one-to-None: source_repository <-> Source
#     source_repository_id = Column(Integer, ForeignKey(Source.id) )
#     source_repository = relationship('Source')

    # Back-references:
    # - many-to-one: psms (set) <-> PSM.spectrum :
    psms = relationship('PSM', collection_class=set, back_populates='spectrum')
    # - adjacency-list many-to-one: child_spectra (set) <-> Spectrum.parent_spectrum :
    child_spectra = relationship('Spectrum', collection_class=set,
                                 back_populates='parent_spectrum')
    
    def __init__(self, *args, **kwargs):
        kwargs.setdefault( 'masses', list() ) #Set default for :attr:`masses` when object is created (not loaded from database).
        kwargs.setdefault( 'intensities', list() ) #Set default for :attr:`intensities` when object is created (not loaded from database).
        super(Spectrum, self).__init__(*args, **kwargs)


class PSM(Base, DictField2AttrsMixin, PepLikeMixin, ReprMixin):
    """
    Peptide Spectrum Matches found by proteomic search engines. They correlate
    Spectrum information (:class:`Spectrum` instances) with Mass Spectrometry
    Peptides (:class:`MSPeptide` instances).
    """
    __tablename__ = 'psms'
    __dictfield2attrs__ = 'scores'
    __repr_init_attr__ = ('id', 'mspeptide', 'spectrum')

    id = Column(Integer, Sequence('psm_id_seq'), primary_key=True,
                autoincrement=True)
    scores = Column(JSONDictField) # { 'searchengine_statistics': score, ... }

    # Relationships:
    # - one-to-many: mspeptide <-> MSPeptide.psms (set) :
    mspeptide_id = Column(Integer, ForeignKey(MSPeptide.id), nullable=False)
    mspeptide = relationship('MSPeptide', back_populates='psms')
    # - one-to-many: spectrum <-> Spectrum.psms (set) :
    spectrum_id = Column(Integer, ForeignKey(Spectrum.id), nullable=False)
    spectrum = relationship('Spectrum', back_populates='psms')
    # - one-to-None: experiment -> Experiment :
    experiment_id = Column( String(20), ForeignKey(Experiment.id) )
    experiment = relationship('Experiment')

    # Back-references:
    # - many-to-one: _db_modifications (set) <-> PSMModification.psm :
    _db_modifications = relationship('PSMModification', collection_class=set,
                                     cascade='all, delete-orphan',
                                     back_populates='psm')

    def __init__(self, *args, **kwargs):
        kwargs.setdefault( 'scores', dict() ) #Set default for :attr:`scores` when object is created (not loaded from database).
        super(PSM, self).__init__(*args, **kwargs)
#         self.__init_dictfield2attrs__() #Fix attribute access of :attr:`extradata` keys (see :super-class:`DictField2AttrsMixin`)

    @hybrid_property #For use in SQLAlchemy SQL queries with the class:
    def seq(self):
        return self.mspeptide.seq
    @seq.expression #For use in SQLAlchemy SQL queries with the class:
    def seq(cls):
        return MSPeptide.seq

    def add_mod_type(self, position, mod_type, session=None):
        return super(PSM, self).add_mod_type(position, mod_type,
                                             mod_cls=PSMModification,
                                             db_session=session)


class PSMModification(Base, ModificationMixin, ReprMixin):
    """
    Modifications found on :class:`PSM` instances.
    """
    __tablename__ = 'psmmodifications'
    __repr_init_attr__ = ('mod_type', 'position', 'psm')

    # Relationships:
    # - one-to-many psm <-> PSM._db_modifications (set):
    psm_id = Column( Integer, ForeignKey(PSM.id) )
    psm = relationship('PSM', back_populates='_db_modifications')

    @hybrid_property
    def parent(self):
        return self.psm
    @parent.setter
    def parent(self, peplike):
        self.psm = peplike


class Gene(Base, IdNameExtradataMixin, ReprMixin):
    """
    Basic class describing genes.
    """
    __tablename__ = 'genes'
    __repr_init_attr__ = ('id', 'name')

    chr = Column(String(5), index=True) # Chromosome location of the encoding gene.
    
    # Back-references:
    # - many-to-one: proteins (set) <-> Protein.gene :
    proteins = relationship('Protein', collection_class=set,
                            back_populates='gene')
    
    def __init__(self, *args, **kwargs):
        super(Gene, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()


class Protein(Base, PepLikeMixin, ReprMixin):
    """
    Basic class defining Proteins.
    """
    __tablename__ = 'proteins'
    __repr_init_attr__ = ('ac', 'name')
    
    _max_ptms_distance = 7 #Maximum allowed distance between PTMs to determine a PTMs High Density Region.
    
    ac = Column(String(25), Sequence('prot_ac_seq'), primary_key=True) #ACcession number.
    ac_noisoform = Column(String(25), index=True) #ACcession number without isoform information. 
    is_decoy = Column(Boolean) #The Protein is a 'decoy'?
    reviewed = Column(Boolean, index=True) #The Protein has been reviewed/curated?
    id_in_source_repository = Column( String(25) ) #ID in source/original repository.
    name = Column( String(600) )
    seq = deferred( Column( UnicodeTextAsText(length=2**21) ) ) #Amino acid primary sequence.
    length = Column(Integer, index=True) #Cached sequence length. Added to speed up queries.
    function = Column(Text)
    locations = Column(JSONListField) #Sub-cellular locations.
    mod_aas = Column(Integer, index=True) #Cached number of Significant and clear Physiological modified amino acids. Added to speed up queries.
    
    # Relationships:
    # - one-to-many: organism <-> Organism.proteins (set) :
    organism_id = Column( Integer, ForeignKey(Organism.id) )
    organism = relationship('Organism', back_populates='proteins')
    # - one-to-many gene <-> Gene.proteins (set) :
    gene_id = Column( Integer, ForeignKey(Gene.id) )
    gene = relationship('Gene', back_populates='proteins')
    # - one-to-many source_repository <-> Source.direct_proteins (set) :
    source_repository_id = Column(Integer, ForeignKey(Source.id) )
    source_repository = relationship('Source',
                                     back_populates='direct_proteins')
    
    # Back-references:
    # - many-to-one goannotations (set) <-> GOAnnotation.protein :
    goannotations = relationship('GOAnnotation', collection_class=set,
                                 back_populates='protein')
    # - many-to-one: digestion_peptides (set) <-> DigestionPeptide.protein :
    digestion_peptides = relationship('DigestionPeptide',
                                      collection_class=set,
                                      cascade='all, delete-orphan',
                                      back_populates='protein')
    # - many-to-one: _db_modifications (set) <-> ProtModification.protein :
    _db_modifications = relationship('ProtModification', collection_class=set,
                                     cascade='all, delete-orphan',
                                     back_populates='protein')
    # - many-to-one: _db_protein2repositories { 'prot_id_in_repo': Protein2Repository() } :
    _db_protein2repositories = relationship('Protein2Repository',
                                            collection_class=attribute_mapped_collection('prot_id_in_repo'),
                                            back_populates='protein')
    # - many-to-many: mspeptides (set) <-> MSPeptide.proteins (set) :
    mspeptides = relationship('MSPeptide', secondary='proteins_mspeptides',
                              collection_class=set,
                              back_populates='proteins')
    
    # Association-proxies:
    # - { 'prot_id_in_repo': Source() } <-> _db_protein2repositories.repository :
    linked_repositories = association_proxy('_db_protein2repositories',
                                            'repository',
                                            creator=lambda key, value:
                                                Protein2Repository(prot_id_in_repo=key,
                                                                   repository=value)
                                            )
    
    # IMPLEMENT! all?:
#     _db_protein2protein_in = [ Protein2Protein(), ... ]
#     _db_protein2protein_out = [ Protein2Protein(), ... ]
#     exp_go = list()
#     pred_go = list()
#     pfam_domains = { (start, end) : PfamDomain() }
#     disorder_levels = dict()
#     interacting_domains = { (start, end) : Protein() }
    
    def __init__(self, *args, **kwargs):
        super(Protein, self).__init__(*args, **kwargs)
        self.__init_common__(*args, **kwargs)
    
    @reconstructor
    def __init_on_dbload__(self, *args, **kwargs):
        self.__init_common__(*args, **kwargs)
    
    def __init_common__(self, *args, **kwargs):
        self._hd_ptm_regions = dict()
    
    def __get_seq_slice__(self, bio_slice):
        """
        :param slice bio_slice: a slice of biological positions ([start - end] both included, and with start at position 1):
        """
        return ProtRegion(self, bio_slice.start, bio_slice.stop)
    
#     # Needs a Many-to-Many relationship at :attr:`_db_protein2mspeptide`:
#     @property
#     def pos2mspeptides(self):
#         """
#         :return dict : { (start, end) : { MSPeptide(), ... } }
#         """
#         mspeptides = defaultdict(set)
#         for prot2mspep in self._db_protein2mspeptide:
#             mspeptides[prot2mspep.prot_start, prot2mspep.prot_end].add(prot2mspep.mspeptide)
#         return mspeptides
    
    @hybrid_property #For use in instances:
    def chr(self):
        return self.gene.chr
    @chr.expression #For use in SQLAlchemy SQL queries with the class:
    def chr(cls):
        return Gene.chr
    
    @property
    def pos2digestion_peptides(self):
        """
        Tryptic peptides from Protein sequence.

        If you want to use a Protease enzyme other than trypsin, use
        :method:`digest_with` instead.

        :return dict : { (start, end) : DigestionPeptide() }
        """
        pos2dig_pep = dict()
        # Populates ``pos2dig_pep`` from formating :attr:`digestion_peptides`
        # to { (start, end) : DigestionPeptide() } :
        if self.digestion_peptides:
            for dig_pep in self.digestion_peptides:
                pos2dig_pep[dig_pep.prot_start, dig_pep.prot_end] = dig_pep
        return pos2dig_pep
    
    def add_goannotation(self, goterm, evidence=None, db_session=None, **kwargs):
        """
        :param object goterm: a :class:`GOTerm` object or a GO ID string.
        :param Evidence evidence: optional :class:`Evidence` object.
        :param Session db_session: a SQLAlchemy :class:`Session` instance.
        """
        if not isinstance(goterm, GOTerm):
            go_id = goterm
            goterm = None
            if db_session is not None: #Get :class:`GOTerm` instance from the DB session:
                goterm = db_session.query(GOTerm).get(go_id)
            if goterm is None: #Create a new instance of :class:`GOTerm`:
                goterm = GOTerm(id=go_id)
        goannotation = GOAnnotation(go=goterm, evidence = evidence, **kwargs)
        self.goannotations.add(goannotation)
        return goannotation
    
    def digest_with(self, protease, missing_cleavages=0, min_length=1,
                    max_length=None, **kwargs):
        """
        Virtually digest the protein using a `protease`.
        Really a proxy for the `protease` :method:`digest`.

        :param Protease protease: the :class:`Proteoase` instance to use for 
        digestion.
        :param int missing_cleavages: the missing cleavages degree. Defaults
        to 0.
        :param int min_length: the minimum amino acid length of the digestion
        peptides to be returned. Should be a value >= 1. Defaults to 1 (but 
        usually, for proteomics purposes, it's better to set it at least to 5-6).
        :param int max_length: the maximum amino acid length of the digestion
        peptides to be returned. Defaults to None, no maximum length.

        :return set : { DigestionPeptide(), ... }
        """
        return protease.digest(self, missing_cleavages, min_length, max_length, 
                               **kwargs)
    
    def finditer_hd_ptm_regions(self, max_ptms_distance=None, min_num_ptms=2,
                                min_dif_ptms=1):
        """
        Iteratively find the protein's high density PTM regions.

        :param int max_ptms_distance: maximum allowed distance between
        modifications. Defaults to ``self._max_ptms_distance``.
        :param int min_num_ptms: minimum number of PTMs in the protein region.
        Defaults to 2.
        :param int min_dif_ptms: minimum different PTM types in the protein
        region. Defaults to 1 (all PTMs can be of the same kind).

        :return generator : yields ( start, end ) in sequence order.
        """
        if max_ptms_distance is None:
            max_ptms_distance = self._max_ptms_distance
        #
        sortedpos_mods = sorted( self.pos2modifications.items() ) #Sort by position
        # First position:
        start, mods = sortedpos_mods[0]
        end = start
        total_dif_ptms = {mod.mod_type for mod in mods}
        num_ptms = len(total_dif_ptms)
        # Iterate PTMs positions:
        for pos, mods in sortedpos_mods[1:]:
            if pos - end > max_ptms_distance: # End of region:
                # Check previous possible region:
                if num_ptms >= min_num_ptms and len(total_dif_ptms) >= min_dif_ptms:
                    #
                    yield start, end
                # New possible region:
                start = end = pos
                total_dif_ptms = {mod.mod_type for mod in mods}
                num_ptms = len(total_dif_ptms)
            else: # Continue increasing previous region:
                end = pos
                curr_dif_ptms = {mod.mod_type for mod in mods}
                num_ptms += len(curr_dif_ptms)
                total_dif_ptms.update(curr_dif_ptms)
        # Check remaining possible region:
        if num_ptms >= min_num_ptms and len(total_dif_ptms) >= min_dif_ptms:
            #
            yield start, end
    
    @property
    def hd_ptm_regions(self):
        """
        The Protein's High Density PTM Regions dictionary.
        Lazy memoized property.
        
        :return dict : { (start, end) : ProtRegion() }
        """
        if not self._hd_ptm_regions and self.mod_aas: # Only if there are significant modifications (:attr:`mod_aas`), try to find the Protein's High Density PTM Regions:
            hd_ptm_regions = dict()
            for start, end in self.finditer_hd_ptm_regions():
                hd_ptm_regions[start, end] = ProtRegion(self, start, end)
            self._hd_ptm_regions = hd_ptm_regions
        #
        return self._hd_ptm_regions


class Protein2Repository(Base, ReprMixin):
    """
    Associations between :class:`Protein` instances and :class:`Source`
    instances. Supplies additional information such as the ID of the Protein in
    the associated repository/Source.
    """
    __tablename__ = 'proteins_repositories'
    __repr_init_attr__ = ('id', 'protein', 'repository')

    id = Column(Integer, Sequence('prot2protrepo_id_seq'), primary_key=True,
                autoincrement=True)
    prot_id_in_repo = Column( String(25) )

    # Relationships:
    # - one-to-many: protein <-> Protein._db_protein2repositories (dict) :
    protein_ac = Column( String(25), ForeignKey(Protein.ac) )
    protein = relationship('Protein', back_populates='_db_protein2repositories')
    # - one-to-many: repository <-> Source._db_protein2repositories (dict) :
    repository_id = Column( Integer, ForeignKey(Source.id) )
    repository = relationship('Source',
                              back_populates='_db_protein2repositories')


protein2mspeptide = Table('proteins_mspeptides', Base.metadata,
    Column('protein_ac', String(25), ForeignKey(Protein.ac), nullable=False),
    Column('mspeptide_id', Integer, ForeignKey(MSPeptide.id), nullable=False)
                          )


class Protein2Protein(Base, ReprMixin):
    """
    Interaction domains between :class:`Protein` instances and :class:`Protein`
    instances. Supplies additional information such as the start and end of the
    protein regions interacting.
    """
    __tablename__ = 'proteins_proteins'
    __repr_init_attr__ = ('id', 'protein_a', 'protein_b')

    id = Column(Integer, Sequence('prot2prot_id_seq'), primary_key=True,
                autoincrement=True)
    protein_a_start = Column(Integer)
    protein_a_end = Column(Integer)
    protein_b_start = Column(Integer)
    protein_b_end = Column(Integer)

    # Relationships:
    # - one-to-many protein_a <-> Protein._db_protein2protein_out :
    protein_a_ac = Column(String(25), ForeignKey(Protein.ac), nullable=False)
    protein_a = relationship('Protein', foreign_keys=[protein_a_ac],
                             backref=backref('_db_protein2protein_out')
                             )
    # - one-to-many protein_b <-> Protein._db_protein2protein_in :
    protein_b_ac = Column(String(25), ForeignKey(Protein.ac), nullable=False)
    protein_b = relationship('Protein', foreign_keys=[protein_b_ac],
                             backref=backref('_db_protein2protein_in')
                             )


class ProtModification(Base, ModificationMixin, ReprMixin):
    """
    Modifications found on :class:`Protein` instances. They aggregate Ascore
    data from the corresponding :class:`MSPepModification` instances and other
    evidences.
    """
    __tablename__ = 'protmodifications'
    __repr_init_attr__ = ('mod_type', 'position', 'protein')

    experimental = Column(Boolean)
#     prediction = Column(Boolean)

    # Relationships:
    # - one-to-many: protein <-> Protein._db_modifications (set) :
    protein_ac = Column(String(25), ForeignKey(Protein.ac))
    protein = relationship('Protein', back_populates = '_db_modifications')
    # - one-to-None: source_repository -> Source :
    source_id = Column(Integer, ForeignKey(Source.id) )
    source = relationship('Source')
    # - many-to-many: cell_lines (set) <-> CellLines.protmodifications (set) :
    cell_lines = relationship('CellLine',
                              secondary='protmodifications_celllines',
                              collection_class=set,
                              back_populates='protmodifications')
    # - many-to-many: evidences (set) <-> Evidence.protmodifications (set) :
    evidences = relationship('Evidence',
                             secondary='protmodifications_evidences',
                             collection_class=set,
                             back_populates='protmodifications')
    # - many-to-many: mspepmodifications (set) <-> MSPepModification.protmodifications (set) :
    mspepmodifications = relationship('MSPepModification',
                                      secondary='protmods_mspepmods',
                                      collection_class=set,
                                      back_populates='protmodifications')
    
    # Back-references:
    # - many-to-many: digestion_peptides (set) <-> DigestionPeptide._db_modifications (set) :
    digestion_peptides = relationship('DigestionPeptide',
                                      secondary='digestionpeptides_protmodifications',
                                      collection_class=set,
                                      back_populates='_db_modifications')

    @hybrid_property
    def parent(self):
        return self.protein
    @parent.setter
    def parent(self, peplike):
        self.protein = peplike

#     @property
#     def exp_evidences(self):
#         """
#         :return list :
#         """
#         pass
# 
#     @property
#     def pred_evidences(self):
#         """
#         :return list :
#         """
#         pass
# 
#     @property
#     def exp_go(self):
#         """
#         :return list :
#         """
#         pass
# 
#     @property
#     def pred_go(self):
#         """
#         :return list :
#         """
#         pass


protmodification2cellline = Table('protmodifications_celllines', Base.metadata,
    Column('protmodification_id', Integer, ForeignKey(ProtModification.id), nullable=False),
    Column('cellline_id', Integer, ForeignKey(CellLine.id), nullable=False)
                                  )


protmodification2evidence = Table('protmodifications_evidences', Base.metadata,
    Column('protmodification_id', Integer, ForeignKey(ProtModification.id), nullable=False),
    Column('evidence_id', Integer, ForeignKey(Evidence.id), nullable=False)
                                  )


protmodification2mspepmodification = Table('protmods_mspepmods', Base.metadata,
    Column('protmodification_id', Integer, ForeignKey(ProtModification.id), nullable=False),
    Column('mspepmodification_id', Integer, ForeignKey(MSPepModification.id), nullable=False)
                                  )


class Protease(Base, IdNameExtradataMixin, ReprMixin):
    """
    Class for protease enzymes.
    """
    __tablename__ = 'proteases'
    __repr_init_attr__ = ('name', 'target_seq_re')
    
    target_seq_re = Column( String(100) ) #Regular expression string to match the target sites.
    relative_cut_pos = Column(SmallInteger) #Indicates the cutting position relative to the beginning of the target site (0). Usually 1 (after the first and only AA of the target sequence).
    target_ptm_exceptions = Column(JSONDictField) #Don't cut if found some listed PTM in some target AA: {'aa': [ptm_id,...] , ...}
    context_length = Column(SmallInteger) #Number of AA before and after the target sequence to use as a context for it.
    context_rules4cuttype = Column(JSONListField) #Use specified cut_type (0 -> Don't cut; 1 -> Full cleavage; 2 -> Ambiguous cleavage) if found some listed AA in the surroundings of the target sequence: [ ('rule1_for_Nterm_context', 'rule1_for_Cterm_context', cut_type), ....] with boolean relation (nterm_rule1 AND cterm_rule1)    
    max_contiguousambiguous = Column(SmallInteger) #Maximum number of contiguous ambiguous cut positions allowed by protease.
    digpep_omited_ptms = Column(JSONListField) #Exclude the listed PTMs from the resulting Digestion Peptides: [ptm_id, ...]
    
    def __init__(self, *args, **kwargs):
        kwargs.setdefault( 'target_ptm_exceptions', dict() ) #Set default for :attr:`target_ptm_exceptions` when object is created (not loaded from database).
        kwargs.setdefault( 'context_rules4cuttype', list() ) #Set default for :attr:`context_rules4cuttype` when object is created (not loaded from database).
        super(Protease, self).__init__(*args, **kwargs)
        self.__init_dictfield2attrs__()
    
    def _cleaving_type_factory(self, peplike):
        """
        Produces a specific function (for the current Protease and the target
        Peptide/Protein) to verify if the Protease can or can't cut a found
        target sequence, depending on cut exceptions due to PTMs (stored as a
        dictionary at :attr:`target_ptm_exceptions`) and ambiguity cut rules
        for the N-Term and C-Term context of the target sequence (stored as
        lists at :attr:`context_rules4missing`).

        :param object peplike: a subclass of :class:`PepLikeMixin` representing
        the polypeptide to be digested. Usually an instance of :class:`Protein`.

        :return function : function to determine cleaving type of the Protease:
        0 -> Don't cut; 1 -> Full cleavage; 2 -> Ambiguous cleavage.
        """
        # Get Protease attributes:
        aa2ptmexcepts = self.target_ptm_exceptions
        check_ptmexcepts = bool(aa2ptmexcepts)
        ptm_exception_aas = set( aa2ptmexcepts.keys() )
        context_rules4cuttype = self.context_rules4cuttype
        check_context = bool(context_rules4cuttype)
        # Summarize modifications and positions in peptide-like sequence:
        pos2mod_ids = { pos: {mod.mod_type.id for mod in mods} for
                        pos, mods in peplike.pos2modifications.items() }
        mod_poss = set( pos2mod_ids.keys() )
        # Function to determine cut exception due to PTMs:
        def is_ptm_blckd(aa, aa_pos): 
            """
            Determine if the AA (`aa`) at the protein position (`aa_pos`) is 
            modified with a PTM not compatible with the protease cut.
            """
            return ( aa in ptm_exception_aas and aa_pos in mod_poss and 
                     pos2mod_ids[aa_pos].intersection( aa2ptmexcepts[aa] ) )
        #
        # Function to search for cut exceptions due to PTMs and context rules:
        def cleaving_type_func(match_start, match_end, target_aas, nt_context, ct_context):
            """
            Function to determine cleaving type and action of the Protease on a
            found target sequence, depending on cut exceptions due to PTMs and
            ambiguity cut rules for the N-Term and C-Term context of the target
            sequence.

            :param int match_start: python start position of the target cut
            sequence inside the peplike sequence.            
            :param int match_end: python end position of the target cut
            sequence inside the peplike sequence.            
            :param str target_aas: amino acids in the target cut sequence.
            :param str nt_context: amino acids at N-Term of the target cut 
            sequence.
            :param str ct_context: amino acids at C-Term of the target cut 
            sequence.        

            :return integer : Cut type: 0 -> Don't cut; 1 -> Full cleavage; 
            2 -> Ambiguous cleavage, wait for the next...
            """
            # Search PTMs that prevent the Protease to cut the target AAs 
            # sequence:
            if check_ptmexcepts:
                for idx, target_aa in enumerate(target_aas, start=1): #start=1 to transform Python str position -> Biological position
                    target_aa_pos = match_start + idx
                    if is_ptm_blckd(target_aa, target_aa_pos):
                        return 0 # -Don't cut.
            # Check the surrounding AA according to the context rules to detect 
            # special cutting position:
            if check_context:
                if check_ptmexcepts:
                    last_nt_aa = nt_context[-1:]
                    last_nt_pos = match_start
                    not_nt_ptm_blckd = not is_ptm_blckd(last_nt_aa, last_nt_pos)
                    first_ct_aa = ct_context[:1]
                    first_ct_pos = match_end + 1
                    not_ct_ptm_blckd = not is_ptm_blckd(first_ct_aa, first_ct_pos)
                else:
                    not_nt_ptm_blckd = not_ct_ptm_blckd = False
                #X_NOTE: Boolean AND relations between each pair of context rules to determine the special cleavage: (nterm_rule1 AND cterm_rule1) -> cut_type
                for nt_rule, ct_rule, cut_type in context_rules4cuttype:
                    if (( not nt_rule or (not_nt_ptm_blckd and re.search(nt_rule, nt_context)) )
                        and 
                        ( not ct_rule or (not_ct_ptm_blckd and re.search(ct_rule, ct_context)) )):
                        return cut_type # 0 -> Don't cut; 1 -> Full cleavage; 2 -> Ambiguous cleavage (usually 2)
            return 1 # -Otherwise Full cleavage.
        #
        return cleaving_type_func
    
    def iter_fulldigest_basicdata(self, peplike, missing_cleavages=0):
        """
        Iteratively digest a Peptide/Protein, yielding all possible
        combinations of N-Term and C-Term positions for Digestion Peptides, as
        well as the number of missing cleavages due to possible PTM exceptions 
        and ambiguous cleavages.

        :param object peplike: a subclass of :class:`PepLikeMixin` representing
        the polypeptide to be digested. Usually an instance of :class:`Protein`.
        :param int missing_cleavages: the 'extra' missing cleavages degree that 
        will be needed later by caller. Defaults to 0.
        
        :return generator : yields tuples containing biological start (parent 
        N-Term cut) position, end position (parent C-Term cut), and number of 
        missing cleavages, for Digestion Peptides.
        """
        # Frequently accessed attributes as variables (some speed optimization):
        peplike_seq = peplike.seq
        peplike_length = peplike.length
        relative_cut_pos = self.relative_cut_pos
        contextlen = self.context_length or 0 #:attr:`context_length` can be None, so avoid it and use 0 instead.
        max_contiguousambiguous = self.max_contiguousambiguous or 1 #:attr:`max_contiguousambiguous` can be None, so avoid it and use 1 instead.
        always_cleaving = lambda match_start, match_end, target_aas, nt_context, ct_context: 1 #Always cut the target sequence at cutting position (no exceptions or ambiguity)
        # Get a function to detect exceptions (missing cleavages) due to PTMs 
        # in target sequence AAs, and ambiguous cuts due to surrounding AAs:
        if self.target_ptm_exceptions or self.context_rules4cuttype:
            cleaving_type = self._cleaving_type_factory(peplike)
        else:
            cleaving_type = always_cleaving
        #
        # Find and yield all possible cutting positions:
        last_clearcut_pos = 0 #Position of the last clear cut (not ambiguous).
        ptm_misscut_poss = list() #Accumulated positions with missed cleavages due to PTMs, from last clear cut.
        ambiguous_pos_grps = list() #A list of ambiguous position groups (each one another list of ambiguous positions: only one position in a group can be cut at a time). Ex.: [ [0], [9,10], [15, 16] ]
        iter_cut_matches = re.finditer(self.target_seq_re, peplike_seq) #Iterator over matches of the target sequence of the Protease on the Protein/Peptide sequence.
        while True:
            try:
                # Get a new probable cutting match over the target sequence:
                match = iter_cut_matches.next()
            except StopIteration as _:
                if last_clearcut_pos < peplike_length:
                    # Generate a 'virtual' match at C-Term of target sequence 
                    # to allow processing the remaining C-Terminal Peptide/s:
                    match = re.search(".$", peplike_seq) #The last AA
                    cleaving_type = always_cleaving
                else:
                    # End the processing loop: all 'fully' digested peptides 
                    # have been generated:
                    break
            # Get data about the new probable cutting position and its 
            # surrounding context:
            match_start = match.start()
            cutting_pos = match_start + relative_cut_pos #Current possible cutting biological position.
            match_end = match.end()
            nt_context = peplike_seq[max(0, match_start-contextlen):match_start] #N-Term context: amino acids at N-Term of the target cut sequence
            ct_context = peplike_seq[ match_end:min(match_end+contextlen, peplike_length) ] #C-Term context: amino acids at C-Term of the target cut sequence
            # Analyze the data to determine how the protease can cut the found 
            # position: 
            cleaving = cleaving_type(match_start, match_end, match.group(0), 
                                     nt_context, ct_context)
            if cleaving == 1:
                # - UNAMBIGUOUS protease Cutting Position:
                if not ambiguous_pos_grps:
                    start_pos_bio = last_clearcut_pos + 1 #From python `str` position to biological position.
                    #
                    yield ( start_pos_bio, cutting_pos, len(ptm_misscut_poss) )
                    #
                else: #There are ambiguous cutting position between last unambiguous cutting position and current one:
                    ambiguous_pos_grps.append( [cutting_pos] ) #Add new clear/unambiguous cutting position as the ending group of ambiguous positions.
                    # Generate all possible combination of digestion peptides 
                    # due to the intermediate ambiguous cutting positions:
                    start_ambigrp = ambiguous_pos_grps[0]
                    for end_ambigrp in ambiguous_pos_grps[1:]:
                        # Product combinations between positions in 'start' 
                        # ambiguous group and positions in 'end' ambiguous group: 
                        #   Ex.: [1, 2] x [4, 5] -> (1, 4), (1, 5), (2, 4), (2, 5)
                        #   Ex.: [1, 2] x [4] -> (1, 4), (2, 4)
                        if len(end_ambigrp) > max_contiguousambiguous: #This group of ambiguous positions has reached maximum number of contiguous ambiguous cut positions allowed by protease
                            # Generate intermediate single K Digestion Peptides
                            # (needed for concatenation when missing cleavages):
                            start_pos_bio = end_ambigrp[0] + 1
                            for end_pos in end_ambigrp[1:-1]:
                                #
                                yield (start_pos_bio, end_pos, 0)
                                #
                                start_pos_bio = end_pos + 1
                            # Reduce `end_ambigrp` to the first positions, and
                            # prepare `next_start_ambigrp` to the last
                            # positions to use as the next loop start
                            # positions:
                            next_start_ambigrp = end_ambigrp[-max_contiguousambiguous:]
                            end_ambigrp = end_ambigrp[:max_contiguousambiguous]
                        else:
                            next_start_ambigrp = end_ambigrp
                        start_ambigrp_len = len(start_ambigrp)
                        for start_idx, start_pos in enumerate(start_ambigrp, start=1):
                            start_pos_bio = start_pos + 1 #From python `str` position to biological position.
                            base_missing_cuts = start_ambigrp_len - start_idx #The remaining ambiguous positions in the start group would be missing cleavages.
                            for end_idx, end_pos in enumerate(end_ambigrp, start=0):
                                missing_cuts = base_missing_cuts + end_idx #The previous ambiguous positions in this end group would also be missing cleavages...
                                missing_cuts += len( [misscut_pos for misscut_pos 
                                                      in ptm_misscut_poss if 
                                                      start_pos < misscut_pos < end_pos] ) #Plus missing cleavages due to PTMs
                                #
                                yield (start_pos_bio, end_pos, missing_cuts)
                                #
                        # Prepare for processing the next group of ambiguous positions:
                        start_ambigrp = next_start_ambigrp
                # Clean for next cutting position match:
                last_clearcut_pos = cutting_pos
                ambiguous_pos_grps = list()
                ptm_misscut_poss = list()
            elif cleaving == 2:
                # - AMBIGUOUS protease Cutting Position:
                if not ambiguous_pos_grps:
                    ambiguous_pos_grps.append( [last_clearcut_pos] )
                ambiguous_grp = ambiguous_pos_grps[-1] #Get last Ambiguous group form list of accumulated ones as the current group.
                if ( cutting_pos - ambiguous_grp[-1] ) > contextlen or ambiguous_grp[-1] == 0: #We are Far enough from the previous ambiguous group last position or if we are at the beginning of the sequence:
                    #Add ambiguous position to a New group:
                    ambiguous_grp = [cutting_pos]
                    ambiguous_pos_grps.append(ambiguous_grp)
                else:
                    #Add ambiguous position to the Current ambiguous group:
                    ambiguous_grp.append(cutting_pos)
            else:
                # - MISSED Cleavage due to a PTM:
                ptm_misscut_poss.append(cutting_pos)
    
    def iter_digpepsbasicdata4mssclvglvl(self, misscleavagelevel, 
                                         fulldig_protodigpeps=None, 
                                         startpos2fulldig_protodigpeps=None,
                                         yielded_protodigpeps=None):
        """
        Yield the data for the digestion peptides of the desired missing
        cleavages level.
        
        :return generator : yields tuples containing biological start (parent 
        N-Term cut) position, end position (parent C-Term cut), and number of 
        missing cleavages, for Digestion Peptides.
        """
        # Check parameters:
        if not startpos2fulldig_protodigpeps:
            startpos2fulldig_protodigpeps = defaultdict(list) #Fully digested proto-digestion peptides indexed by start_pos: { start_pos: [ (start_pos, end_pos, missing_cuts), ... ] }
            for protodigpep in fulldig_protodigpeps:
                startpos2fulldig_protodigpeps[protodigpep[0]].append(protodigpep)
        if not yielded_protodigpeps:
            yielded_protodigpeps = set()
            if not fulldig_protodigpeps:
                fulldig_protodigpeps = flatten_1lvl( startpos2fulldig_protodigpeps.values() )
            yielded_protodigpeps.update(fulldig_protodigpeps)
        #
        for start_pos in sorted(startpos2fulldig_protodigpeps)[:-misscleavagelevel]:
            elongated_peps = startpos2fulldig_protodigpeps[start_pos]
            for _ in range(misscleavagelevel):
                elongating_peps = set()
                for _, prev_end_pos, prev_misscuts in elongated_peps:
                    for _, next_end_pos, next_misscuts in startpos2fulldig_protodigpeps[prev_end_pos + 1]: #X_NOTE: ``startpos2fulldig_protodigpeps`` it's a defaultdict(list) so it returns empty lists for keys/positions that doesn't exist.
                        elongating_peps.add( (start_pos, next_end_pos, 
                                             prev_misscuts + next_misscuts + 1) ) #+1 missed cleavage
                elongated_peps = elongating_peps #Switch
            for proto_digpep in elongated_peps:
                if proto_digpep not in yielded_protodigpeps:
                    yielded_protodigpeps.add(proto_digpep)
                    yield proto_digpep
    
    def iter_digest_basicdata(self, peplike, missing_cleavages=0):
        """
        Iteratively digest a Protein/Sequence, yielding the basic data for each
        Digestion Peptide: start position, end position and number of missing
        cleavages.
        This is the Standard method. It takes into account cut exceptions due 
        to PTMs and ambiguous cuttings due to the surrounding context of the 
        target cutting position.
        
        :param object peplike: a subclass of :class:`PepLikeMixin` representing
        the polypeptide to be digested. Usually an instance of :class:`Protein`.
        :param int missing_cleavages: the 'extra' missing cleavages degree (to 
        'simulate' a partial digestion). Defaults to 0.
        
        :return generator : yields tuples containing biological start (parent 
        N-Term cut) position, end position (parent C-Term cut), and number of 
        missing cleavages, for Digestion Peptides.
        """
        yielded_protodigpeps = set() #Cache to avoid yielding already yielded proto-digestion peptides: { (start_pos, end_pos, missing_cuts), ... }
        # First, yield and store the data for ALL the 'Fully' digested peptides
        # (except for the missed cleavages due to PTMs or ambiguity):
        startpos2fulldig_protodigpeps = defaultdict(list) #Fully digested proto-digestion peptides indexed by start_pos: { start_pos: [ (start_pos, end_pos, missing_cuts), ... ] }
        for fulldig_protodigpep in self.iter_fulldigest_basicdata(peplike, missing_cleavages):
            yielded_protodigpeps.add(fulldig_protodigpep)
            startpos2fulldig_protodigpeps[fulldig_protodigpep[0]].append(fulldig_protodigpep)
            yield fulldig_protodigpep
        # Then, yield the data for the resulting digestion peptides of the
        # different missing cleavages level (..., 2 missing cleavages, 1
        # missing cleavage):
        while missing_cleavages > 0:
            for proto_digpep in self.iter_digpepsbasicdata4mssclvglvl(
                                                 missing_cleavages, None, 
                                                 startpos2fulldig_protodigpeps, 
                                                 yielded_protodigpeps):
                yield proto_digpep
            missing_cleavages -= 1
    
    def iter_digest_simple_basicdata(self, peplike, missing_cleavages=0):
        """
        Iteratively digest a Protein/Sequence, yielding the basic data for each
        Digestion Peptide: start position, end position and number of missing
        cleavages.
        This is the Simplified method. It does NOT take into account any cut 
        exception due to PTMs or ambiguous cleavages.

        :param object peplike: a subclass of :class:`PepLikeMixin` representing
        the polypeptide to be digested. Usually an instance of :class:`Protein`.
        :param int missing_cleavages: the missing cleavages degree (to 
        'simulate' a partial digestion, ...). Defaults to 0.
        
        :return generator : yields tuples containing biological start (parent 
        N-Term cut) position, end position (parent C-Term cut), and number of 
        missing cleavages, for Digestion Peptides.
        """
        # Frequently accessed attributes as variables (some speed optimization):
        peplike_seq = peplike.seq
        peplike_length = peplike.length
        relative_cut_pos = self.relative_cut_pos
        # First, yields and store the data for ALL the FULLY digested peptides
        # (without any exception for PTMs or ambiguous cleavages):
        data_fulldigpeps = list() #Fully digested peptides basic data
        last_cut_pos = 0
        for match in re.finditer(self.target_seq_re, peplike_seq):
            cut_pos = match.start() + relative_cut_pos
            digpep_data = (last_cut_pos + 1, cut_pos, 0)
            data_fulldigpeps.append(digpep_data)
            yield digpep_data
            last_cut_pos = cut_pos
        if last_cut_pos < peplike_length:
            digpep_data = (last_cut_pos + 1, peplike_length, 0)
            data_fulldigpeps.append(digpep_data)
            yield digpep_data
        # Then, yields the data for the resulting digestion peptides of the
        # different missing cleavages degrees (..., 2 missing cleavages, 1
        # missing cleavage):
        while missing_cleavages > 0:
            for idx, startdigpep_data in enumerate( data_fulldigpeps[:-missing_cleavages] ):
                enddigpep_data = data_fulldigpeps[idx+missing_cleavages]
                yield startdigpep_data[0], enddigpep_data[1], missing_cleavages
            missing_cleavages -= 1
    
    def digest(self, peplike, 
               missing_cleavages=0, min_length=1, max_length=None, 
               digestion_condition=None, create_digestion_condition=True, 
               digestion_method='standard', follow_omited_ptms=True, 
               DigPepCls=None):
        """
        Virtually digest a polypeptide (`peplike`).

        :param object peplike: a subclass of :class:`PepLikeMixin` representing
        the polypeptide to be digested. Usually an instance of :class:`Protein`.
        :param int missing_cleavages: the 'extra' missing cleavages degree. 
        Defaults to 0.
        :param int min_length: the minimum amino acid length of the digestion
        peptides to be returned. Should be a value >= 1. Defaults to 1 (but 
        usually, for proteomics purposes, it's better to set it at least to 5-6).
        :param int max_length: the maximum amino acid length of the digestion
        peptides to be returned. Should be None, no maximum length, or a 
        value >= :param:`min_length`. Defaults to None.
        :param DigestionCondition digestion_condition: an instance of 
        :class:`DigestionCondition`. Defaults to None; so a new one would be 
        created if :param:`create_digestion_condition` is True.
        :param bool create_digestion_condition: sets what to do when 
        the :param:`digestion_condition` is None: True -> create a default 
        Digestion Condition, False -> let :param:`disgestion_condition` as None 
        (so new Digestion Peptide will not be associated with any Digestion 
        Condition).
        :param str gisgestion_method: 'standard' -> take into account missed
        cleavages due to PTMs and ambiguous cuttings due to the surrounding 
        context of the target cutting position. 'simplified' -> don't take 
        those rules into account and do it fast.
        :param bool follow_omited_ptms: sets what to do about Protease omitted
        PTMs (the PTM IDs in :attr:`digpep_omited_ptms`): True -> omit the PTM
        IDs from :attr:`digpep_omited_ptms` in the resulting Digestion
        Peptides, False -> ignore :attr:`digpep_omited_ptms`. Defaults to True.
        :param class or None DigPepCls: class to use to create resulting 
        Digestion Peptides and store its information. If None is provided, 
        defaults to :class:`DigestionPeptide`.
        
        :return list : [ DigestionPeptide(), ... ]
        """
        # Check parameters:
        if min_length < 1:
            raise ValueError("'min_length' should be >= 1")
        if max_length and (max_length < min_length):
            raise ValueError("If provided, 'max_length' should be >= 'min_length'")
        # Set the method used for the digestion:
        if digestion_method == 'standard':
            if missing_cleavages:
                iter_digest_method = self.iter_digest_basicdata
            else:
                iter_digest_method = self.iter_fulldigest_basicdata
        elif digestion_method == 'simplified':
            iter_digest_method = self.iter_digest_simple_basicdata
        else:
            raise TypeError("`digestion_method` must be 'standard' or 'simplified'")
        # Decide what to do with the Protease omitted PTMs:
        if follow_omited_ptms and self.digpep_omited_ptms:
            omit_ptmids = self.digpep_omited_ptms
        else:
            omit_ptmids = None
        # Set the default class to store resulting Digestion Peptides information:
        if DigPepCls is None:
            DigPepCls = DigestionPeptide
        # Create a new :class:`DigestionCondition` instance, if needed:
        if digestion_condition is None and create_digestion_condition:
            digestion_condition = DigestionCondition(name=self.name+" digestion", 
                                                     extradata={ 'digestion_method': digestion_method,
                                                                 'date_time': datetime.now().isoformat() }, 
                                                     min_digpep_length=min_length, 
                                                     max_digpep_length=max_length,
                                                     digestion_enzyme=self)
        # Frequently accessed variables (some speed optimization):
        check_maxlength = not (max_length is None)
        peplike_seq = peplike.seq
        # Set some variables depending on the kind of :param:`peplike` object:
        if isinstance(peplike, Protein): # Digesting a :class:`Protein` instance:
            parent_prot = peplike #The original/parent Protein: the peptide-like instance to digest.
            parent_pos_shift = 0
            consensus = True
            consensus_pep = None
        else: # Assume an instance of class or sub-class of :class:`DigestionPeptideNoDB` to be digested:
            parent_prot = peplike.protein #The original/parent digested Protein from witch the Digestion Peptide was generated.
            parent_pos_shift = (peplike.prot_start or 1) - 1 #Incremental shift relative to the original/parent digested Protein. X_NOTE: avoid error if `prot_start` is None
            consensus = peplike.consensus
            consensus_pep = peplike.consensus_peptide
        #
        # Generate Digestion Peptides:
        dig_peps = list()
        for start_pos, end_pos, mssng_clvgs in iter_digest_method(peplike, missing_cleavages):
            # - Check Digestion Peptide length:
            dp_length = end_pos - start_pos + 1
            if dp_length < min_length or (check_maxlength and dp_length > max_length):
                continue #Skip this Digestion Peptide.
            # - Generate Digestion Peptide:
            digpepseq = peplike_seq[start_pos-1:end_pos]
            prot_start = start_pos + parent_pos_shift #Make positions relatives to parent protein
            prot_end = end_pos + parent_pos_shift     #
            mods = set( PepLikeMixin._iter_modifications_slice(peplike, 
                                                               prot_start, 
                                                               prot_end, 
                                                               omit_ptmids) )
#             # - Apply user-provided filter functions (they return False if 
#             #   digestion peptide do not pass the filter):
#             if not all(filter_func(prot_start, prot_end, mssng_clvgs, digpepseq, 
#                                    mods, peplike) 
#                        for filter_func in filter_funcs):
#                 continue #Skip this Digestion Peptide (doesn't pass any filter)
            dig_peps.append( DigPepCls(protein=parent_prot, 
                                       prot_start=prot_start, 
                                       prot_end=prot_end, 
                                       seq=digpepseq, consensus=consensus,
                                       consensus_peptide=consensus_pep, 
#                                        consensus_peptide=None, #TEST!! #DEBUG Try to avoid Memory leak problems
                                       digestion_condition=digestion_condition,
                                       missing_cleavages=mssng_clvgs, 
                                       _db_modifications=mods) )
        #
        return dig_peps


class ProtRegion(PepLikeMixin, ReprMixin):
    """
    A Proxy Class representing a Protein Region. It has no database representation.
    """
    __repr_init_attr__ = ('protein', 'prot_start', 'prot_end')

    def __init__(self, protein, prot_start, prot_end):
        """
        [protein start position - protein end position] both included, and both
        are biological positions (with start at 1, not 0 as in python).
        """
        self._protein = protein
        self.prot_start = prot_start
        self.prot_end = prot_end

    def __get_seq_slice__(self, bio_slice):
        """
        :param slice bio_slice: a slice of biological positions ([start - end] both included, and with start at position 1):
        """
        return ProtRegion( self.protein,
                           bio_slice.start + (self.prot_start - 1),
                           bio_slice.stop + (self.prot_start - 1) )

    def __getattr__(self, attr):
        """
        Proxy to the associated :class:`Protein` object.
        """
        return getattr(self.protein, attr)

    @property
    def protein(self):
        return self._protein

    @property
    def seq(self):
        return self.protein.seq[(self.prot_start - 1):self.prot_end]
    @seq.setter
    def seq(self, new_seq):
        """
        :caution: use with care, this overwrites the Protein sequence!!
        """
        self.protein.seq = self.protein.seq[:self.prot_start-1] + new_seq + self.protein.seq[self.prot_end:]
        self.prot_end = self.prot_start + len(new_seq) - 1

    @property
    def _db_modifications(self):
        return set( self._iter_modifications_slice(self.protein,
                                                   self.prot_start,
                                                   self.prot_end) )

    @property
    def prot_pos2modifications(self):
        """
        :return defaultdict : returns Only the Significant modifications with
        positions relative to the start of referenced Protein.
        { position in protein sequence : { Modification(significant=True), ... } }
        """
        return self._build_pos2modifications()
    
    @property
    def prot_pos2allmodifications(self):
        """
        :return defaultdict : returns ALL the modifications with positions
        relative to the start of referenced Protein.
        { position in protein sequence : { Modification(), ... } }
        """
        return self._build_pos2modifications(only_significant=False, 
                                             only_physio=False)
    
    @property
    def pos2modifications(self):
        """
        :return defaultdict : returns Only the Significant modifications with
        positions relative to the :attr:``prot_start`` position.
        { position in region sequence : { Modification(significant=True), ... } }
        """
        return self._build_pos2modifications(self.prot_start - 1)

    @property
    def pos2allmodifications(self):
        """
        :return defaultdict :  returns ALL the modifications, with positions
        relative to the :attr:`prot_start` position.
        { position in region sequence : { Modification(), ... } }
        """
        return self._build_pos2modifications(self.prot_start - 1, 
                                             only_significant=False, 
                                             only_physio=False)
    
    @property
    def modid2protpositions(self):
        """
        :return dict : { ModificationType().id : [ positions in seq ] } where
        positions are relative to the start of referenced protein.
        """
        return self._build_modid2positions()
    
    @property
    def modid2positions(self):
        """
        :return dict : { ModificationType().id : [ positions in seq ] } where
        positions are relative to the :attr:``prot_start`` position.
        """
        return self._build_modid2positions(self.prot_start - 1)
    
    def add_mod_type(self, position, mod_type, session=None):
        prot_position = position + self.prot_start - 1
        return self.protein.add_mod_type(prot_position, mod_type, session)

    def add_modification(self, modification, position=None):
        if position:
            prot_position = position + self.prot_start - 1
        else:
            prot_position = None
        return self.protein.add_modification(modification, prot_position)
    
    def remove_modification(self, modification):
        self.protein.remove_modification(modification)
    
#     def iter_prot_annotations(self):
#         """
#         :return generator : ( (start, end), annotation )
#         """
#         return self.protein.iter_annotations_slice(self.prot_start, self.prot_end)
#     
#     @property
#     def prot_annotations(self):
#         """
#         :return dict : { (start, end): {annotations} }
#         """
#         prot_annotations = defaultdict(set)
#         for start, end, annotation in self.iter_prot_annotations():
#             prot_annotations[start, end].add(annotation)
#         return prot_annotations
#     
#     @property
#     def annotations(self):
#         """
#         :return dict : { (start, end): {annotations} }
#         """
#         prot_annotations = defaultdict(set)
#         for start, end, annotation in self.iter_prot_annotations():
#             prot_annotations[start-self.prot_start, end-self.prot_start].add(annotation)
#         return prot_annotations
#     
#     def overlaps(self, other):
#         """
#         :return bool :
#         """
#         raise NotImplementedError #IMPLEMENT!
#         
#         return


class DigestionCondition(Base, IdNameExtradataMixin, ReprMixin):
    """
    Class for protease digestion conditions.
    """
    __tablename__ = 'digestionconditions'
    __repr_init_attr__ = ('name', 'digestion_enzyme', 'max_missing_cleavages')
    
    max_missing_cleavages = Column(Integer)
    min_digpep_length = Column(Integer)
    max_digpep_length = Column(Integer)
    
    # Relationships:
    # - one-to-None digestion_enzyme -> Protease :
    digestion_enzyme_id = Column( Integer, ForeignKey(Protease.id) )
    digestion_enzyme = relationship('Protease')
    
    # Back-references:
    # - many-to-one: digestion_peptides (set) <-> DigestionPeptide.digestion_condition :
    digestion_peptides = relationship('DigestionPeptide',
                                      collection_class=set,
                                      cascade='all, delete-orphan',
                                      back_populates='digestion_condition')


class DigestionPeptideNoDB(PepLikeMixin, ReprMixin, InitKwargs2AttrMixin):
    """
    Digestion Peptides. The peptides resulting from Protein digestion
    by a Protease (like trypsin). They map all the possible modifications found
    in their corresponding Protein region.
    This is the version of the class no linked to a SQLAlchemy Database.
    For the Database-linked version see sub-class :class:`DigestionPeptide`.
    """
    __repr_init_attr__ = ('seq', 'protein', 'prot_start', 'prot_end')

    id = None
    prot_start = None #[protein start position - protein end position] both included,
    prot_end = None   #and both are biological positions (with start at 1, not 0 as in python).
    seq = '' #Should be provided, or it will be inferred during :method:`__init__` from :attr:``protein.seq``, :attr:`prot_start` and :attr:`prot_end`.
    missing_cleavages = None #Number of missing cleavages that this peptide has (None if it's unknown).
    consensus = None #Should be provided, or it will default to True during :method:`__init__`.
    _db_seq_w_mods = None #Cached sequence with modifications.
    _db_mod_aas = None #Cached number of Significant and clear Physiological modified amino acids. Added to speed up Database queries.
    
    protein = None
    digestion_condition = None
    consensus_peptide = None
    
    combinatorial_peptides = None # List of combinatorial peptides derived from this peptide. #X_NOTE: Used instead of a SQLAlchemy relationship because causing memory leaks when generating possible co-modified peptides from Consensus Digestion Peptides in :method:``tcellxtalk.views.ProteinDigestCombineView._filtered_combdigpeps``).
    
    def __init__(self, *args, **kwargs):
        """
        :caution: `protein`, `prot_start` and `prot_end` are required keyword 
        arguments.
        """
        kwargs.setdefault('consensus', True) #Set default for :attr:`consensus` when object is created (not loaded from database).
        kwargs.setdefault( 'combinatorial_peptides', list() ) #Set default for :attr:`combinatorial_peptides` when object is created (not loaded from database).
        super(DigestionPeptideNoDB, self).__init__(*args, **kwargs)
        #
        if not self.seq: # The sequence is necessary, but ...:
            if self.protein and self.prot_start and self.prot_end: # It can be get from the :attr:`protein` sequence according to :attr:`prot_start` and :attr:`prot_end`:
                self.seq = self.protein.seq[self.prot_start-1:self.prot_end]
            else:
                raise TypeError("A `seq` argument is required. Alternatively "
                                "`protein` , `prot_start` and `prot_end` "
                                "arguments are needed...")
    
    @property
    def digestion_enzime(self):
        if self.digestion_condition:
            return self.digestion_condition.digestion_enzime
        else:
            return None
    
    def refresh_modifications(self):
        # Get PTMs from Protein region covered by this Digestion Peptide:
        mods = set( self._iter_modifications_slice(self.protein,
                                                   self.prot_start,
                                                   self.prot_end) )
        # Update database modifications:
        self._db_modifications = mods
    
    @hybrid_property #For use in instances:
    def modifications(self):
        """
        Database-memoized property to get the modifications from the database
        if they are there; otherwise, if this is a Consensus Digestion Peptide
        (:attr:`consensus`), from the associated protein (:attr:`protein`).
        X_NOTE: Combinatorial Digestion Peptides (non-consensus ones) must have 
        the associated Protein Modifications (:attr:`_db_modifications`) set.
        """
        if not self._db_modifications and self.consensus and self.protein:
            self.refresh_modifications()
        return self._db_modifications
    @modifications.expression #For use in SQLAlchemy SQL queries with the class:
    def modifications(cls):
        return cls._db_modifications
    
    def refresh_mod_aas(self, only_significant=True, only_physio=True):
        """
        Update database number of modified amino acids.
        
        :param bool only_significant: True -> only Significant PTMs are 
        considered, False -> all PTMs are considered. Defaults to True.
        :param bool only_physio: True -> only PTMs with clear physiological 
        origin are considered, False -> all PTMs are considered. Defaults to True.
        """
        self._db_mod_aas = self.get_mod_aas(only_significant, only_physio)
    
    @hybrid_property #For use in instances:
    def mod_aas(self):
        """
        Database-memoized property to get the number of Significant modified
        amino acids from the Database if they are there, or, otherwise, from
        the associated modifications.
        """
        if self._db_mod_aas is None:
            self.refresh_mod_aas()
        return self._db_mod_aas
    @mod_aas.expression #For use in SQLAlchemy SQL queries with the class:
    def mod_aas(cls):
        return cls._db_mod_aas
    
    @property
    def prot_pos2modifications(self):
        """
        :return defaultdict : returns Only the Significant modifications with
        positions relative to the start of referenced Protein.
        { position in prot : { Modification(significant=True), ... } }
        """
        return self._build_pos2modifications()
    
    @property
    def prot_pos2allmodifications(self):
        """
        :return defaultdict : returns ALL the modifications with positions
        relative to the start of referenced Protein.
        { position in prot : { Modification(), ... } }
        """
        return self._build_pos2modifications(only_significant=False, 
                                             only_physio=False)
    
    @property
    def pos2modifications(self):
        """
        :return defaultdict : returns Only the Significant modifications, with 
        positions relative to the :attr:`prot_start` position.
        { position in seq : { Modification(significant=True), ... } }
        """
        return self._build_pos2modifications( (self.prot_start - 1) 
                                              if self.prot_start else 0 )
    
    @property
    def pos2allmodifications(self):
        """
        :return defaultdict :  returns ALL the modifications, with positions
        relative to the :attr:`prot_start` position.
        { position in seq : { Modification(), ... } }
        """
        return self._build_pos2modifications( (self.prot_start - 1) 
                                              if self.prot_start else 0, 
                                              only_significant=False, 
                                              only_physio=False )
    
    @property
    def modid2protpositions(self):
        """
        :return dict : { ModificationType().id : [ positions in seq ] } where
        positions are relative to the start of referenced Protein.
        """
        return self._build_modid2positions()
    
    @property
    def modid2positions(self):
        """
        :return dict : { ModificationType().id : [ positions in seq ] } where
        positions are relative to the :attr:`prot_start` position.
        """
        return self._build_modid2positions(self.prot_start - 1)
    
    def _build_pos_modtype2modifications(self, only_significant=True, 
                                         only_physio=True, pos_sorted=True):
        """
        :param bool only_significant: True -> only Significant PTMs are 
        considered, False -> all PTMs are considered. Defaults to True.
        :param bool only_physio: True -> only PTMs with clear physiological 
        origin are considered, False -> all PTMs are considered. Defaults to True.
        :param bool pos_sorted: True -> return a :class:``OrderedDict`` 
        instance where positions (part of the keys) are sorted ascending, 
        False -> return  a :class:``defaultdict`` instance without sorting. 
        Defaults to True.

        :return dict : a :class:``defaultdict`` or :class:``OrderedDict``
        instance of the kind { position (relative to the peptide), ModificationType(): {ProtModification(), ... } }
        calculated from ``self.modifications``.
        """
        pos_modtype2modifications = defaultdict(set)
        for ptm in self.filtered_modifications(only_significant, only_physio):
            pos_modtype = (ptm.position - (self.prot_start or 1) + 1, ptm.mod_type) #X_NOTE: avoid error when `prot_start` is None
            pos_modtype2modifications[pos_modtype].add(ptm)
        #
        if not pos_sorted:
            return pos_modtype2modifications #An unsorted :class:``defaultdict`` instance.
        else:
            return OrderedDict(sorted( pos_modtype2modifications.items() )) #A position sorted :class:``OrderedDict`` instance.
    
    #Read-only property:
    pos_modtype2modifications = property(_build_pos_modtype2modifications, 
                                         doc='OrderedDict : { position, ModificationType(): {ProtModification(), ... } }')
    
    def add_mod_type(self, position, mod_type, db_session=None):
        """
        :caution: the new generated modification will be also linked to the
        Digestion Peptide's Protein.
        """
        prot_position = position + (self.prot_start or 1) - 1 #X_NOTE: avoid error when `prot_start` is None
        modification = super(DigestionPeptideNoDB, self).add_mod_type(prot_position,
                                                                      mod_type,
                                                                      db_session=db_session)
        modification.protein = self.protein #The modification belongs to the digestion peptide's protein
        return modification
    
    def add_modification(self, modification, position=None):
        """
        :caution: the modification will be also re-linked to the Digestion
        Peptide Protein.
        """
        if position:
            prot_position = position + (self.prot_start or 1) - 1 #X_NOTE: avoid error when `prot_start` is None
        else:
            prot_position = None
        modification.protein = self.protein #The modification belongs to the digestion peptide's protein
        return super(DigestionPeptideNoDB, self).add_modification(modification,
                                                                  prot_position)
    
    def mspeptides(self, exact_ptms=False, only_w_sig_ptms=True, 
                   eagerload_ptms=True, db_session=None):
        """
        Returns the Mass Spectrometry Peptides from the database that are like
        or similar to this Digestion Peptide.
        
        :param bool exact_ptms: if True, search for MS Peptides based in
        sequence with modifications :attr:`seq_w_mods` (take PTMs into
        account). If False, compare only sequence (:attr:`seq`). Defaults to
        False.
        :param bool only_w_sig_ptms: True -> only MS Peptides with Significant 
        and clear Physiological PTMs are considered, False -> all MS Peptides 
        are considered. Defaults to True.
        :param bool eagerload_ptms: if True, populate also the related 
        collection :attr:`_db_modifications` and the related :attr:`source` of 
        each MS Peptide found. Defaults to True.
        :param Session db_session: a required SQLAlchemy :class:`Session` 
        instance.
        
        :return set : a set of :class:`MSPeptide` instances.
        """
        if not db_session:
            raise TypeError("A SQLAlchemy Session should be provided as "
                            "`db_sessioin` parameter.")
        #
        if eagerload_ptms: # Eager loading of related collection of MS Peptide Modifications, and related Source:
            mspeptides = db_session.query(MSPeptide)\
                                   .options( joinedload('_db_modifications')
                                             .joinedload('mod_type'), 
                                             joinedload('source') )
        else:
            mspeptides = db_session.query(MSPeptide)
        #
        if exact_ptms: # Use field :attr:`seq_w_mods` for the search:
            mspeptides = mspeptides.filter(MSPeptide.seq_w_mods==self.seq_w_mods)
        else: # Use field :attr:`seq` for the search:
            mspeptides = mspeptides.filter(MSPeptide.seq==self.seq)
        #
        if only_w_sig_ptms: # Filter MS Peptides with at least one significant and clear physiological modified AA:
            mspeptides = mspeptides.filter(MSPeptide._db_mod_aas>0)
        #
        return set( mspeptides.all() )
    
    @property
    @Memoized
    def mspeptides_exact(self):
        """
        Returns the Mass Spectrometry Peptides from the database that are like
        this Digestion Peptide.

        :return set : a set of :class:`MSPeptide` instances which have the same
        `seq_w_mods` property as this Digestion Peptide.
        """
        return self.mspeptides(exact_ptms=True)
    
    @property
    @Memoized
    def mspeptides_similar(self):
        """
        Returns the Mass Spectrometry Peptides from the database that are
        similar to this Digestion Peptide.

        :return set : a set of :class:`MSPeptide` instances which have the same
        `seq` property as this Digestion Peptide.
        """
        return self.mspeptides(exact_ptms=False)
    
#     @Memoized
#     def sig_modtypes2pos(self, ptmid2threshold=None):
#         """
#         Summarize the positions with significant modifications (ascore >
#         threshold) for each Modification Type present in this Digestion Peptide.
# 
#         :param dict ptmid2threshold: a dictionary containing the threshold for each
#         Modification Type ID: { mod_type_id: threshold }. If not supplied, the
#         :attr:`ptmid2threshold` of each :class:`Source` instance will be used.
# 
#         :return dict : { ModificationType(): {position of significant modification, ...} }
#         """
#         # IMPLEMENT: use of :attr:`ptmid2threshold` of each :class:`Source`
#         # instance to generate `ptmid2threshold` (current implementation is
#         # slow, and weak because 'default' is not always present.
#         raise NotImplementedError()
# 
#         if not ptmid2threshold:
#             ptmid2threshold = dict()
#         #
#         modtype2pos = defaultdict(set)
#         for mod in self.modifications:
#             if mod.ascore and mod.ascore >= ptmid2threshold.get( mod.mod_type_id,
#                                                                  mod.source.ptmid2threshold.get(mod.mod_type_id,
#                                                                                                 mod.source.ptmid2threshold['default']) ):
#                 modtype2pos[mod.mod_type].add(mod.position - self.prot_start + 1)
#         return modtype2pos
#     
#     def sig_modtypes2n_mods(self, ptmid2threshold=None):
#         """
#         Give the number of significant modifications (ascore > threshold)
#         for each Modification Type present in this Digestion Peptide.
# 
#         :param dict ptmid2threshold: a dictionary containing the threshold for each
#         Modification Type ID: { mod_type_id: threshold }. If not supplied, the
#         base_ascore_threshold of each :class:`ModificationType` instance will
#         be used.
# 
#         :return dict : { mod_type: n_sig_mods }
#         """
#         return { mod_type: len(pos) for mod_type, pos in
#                  self.sig_modtypes2pos(ptmid2threshold).items() }
#     
#     def n_sig_mods(self, ptm2threshold=None):
#         """
#         Give the total number of significant modifications (ascore > threshold)
#         in this Digestion Peptide.
# 
#         :param dict ptm2threshold: a dictionary containing the threshold for each
#         Modification Type ID: { mod_type_id: threshold }. If not supplied, the
#         base_ascore_threshold of each :class:`ModificationType` instance will
#         be used.
# 
#         :return int : total number of significant modifications.
#         """
#         return sum( len(pos) for pos in
#                     self.sig_modtypes2pos(ptm2threshold).values() )
#     
    def _count_aa(self, aa_list):
        """
        :return defaultdict : { aa: counts }
        """
        aa2counts = defaultdict(lambda: 0)
        for aa in self.seq:
            if aa in aa_list:
                aa2counts[aa] += 1
        return aa2counts
    
    @property
    @Memoized
    def n_styk_sites(self):
        """
        :return defaultdict : { aa: counts }
        """
        return self._count_aa( ('S', 'T', 'Y', 'K') )
    
    @property
    @Memoized
    def mean_styk_dist(self):
        """
        :return int :
        """
        last_pos = 0
        distances = list()
        for pos, aa in enumerate(self.seq, start=1):
            if aa in ('S', 'T', 'Y', 'K'):
                if last_pos > 0:
                    distances.append(pos - last_pos)
                last_pos = pos
#         return pystats.mean(distances)
        return sum(distances) / len(distances)
    
    @Memoized
    def singularity(self, dig_pep=True, protein_seq=False, db_session=None): #IMPROVE!
        """
        :return bool :
        """
        if not db_session:
            raise TypeError("A SQLAlchemy Session should be provided as "
                            "`db_sessioin` parameter.")
        #
        if dig_pep: #Search other similar Digestion Peptides from other proteins:
            other_dp = db_session.query(DigestionPeptide)\
                                 .filter(DigestionPeptide.seq==self.seq, 
                                         DigestionPeptide.protein_ac!=self.protein_ac)\
                                 .first()
        else:
            other_dp = None
        if not other_dp and protein_seq: #Search for this peptide in Protein sequences:
            other_prot = (len( search_seq_in_prot_db(self.seq, db_session) ) > 1)
        else:
            other_prot = None
        #
        return not (other_dp or other_prot)
    
#     @property
#     @Memoized
#     def mean_mod_dist(self):
#         """
#         :return int :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def pred_pI(self):
#         """
#         :return float :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def pred_hydrophobicity(self):
#         """
#         :return float :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def exp_charge(self):
#         """
#         :return int :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def pred_charge(self):
#         """
#         :return int :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def n_mc_sites(self):
#         """
#         :return dict : { aa: counts }
#         """
#         return self._count_aa( ('M', 'C') )
#     
#     @property
#     @Memoized
#     def detectability_score(self):
#         """
#         :return float :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def selection_score(self):
#         """
#         :return float :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def pfam_domains(self):
#         """
#         :return dict :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def disorder_levels(self):
#         """
#         :return dict :
#         """
#         pass
#     
#     @property
#     @Memoized
#     def interacting_domain(self):
#         """
#         :return bool :
#         """
#         pass
    
#     def make_combinatorial_peptides(self): #IMPROVE: discard incompatible PTMs in the same AA, ...
#         """
#         If this is a consensus peptide (the one with all the possible
#         modifications), generates peptides like this one but with all the
#         possible combinations of its modifications.
# 
#         :return list or None : If this is a consensus peptide, it returns a
#         list like [ self, DigestionPeptide(consensus=False,
#         consensus_peptide=self), ... ]; otherwise, it returns None.
#         """
#         if self.consensus:
#             # Add `self` (this consensus peptide with all modifications), and
#             # its version with no modifications:
#             nomods_digpep = DigestionPeptide(protein=self.protein,
#                                              prot_start=self.prot_start,
#                                              prot_end=self.prot_end,
#                                              seq=self.seq,
#                                              digestion_enzyme=self.digestion_enzyme,
#                                              consensus=False,
#                                              consensus_peptide=self,
#                                              _db_modifications = set() )
#             combinatorial_peptides = [self, nomods_digpep]
#             # Get modifications grouped by position and modification type.
#             pos_modtype2modifications = self.pos_modtype2modifications
#             pos_modtypes = pos_modtype2modifications.keys()
#             # Generate all the possible semi-modified peptides from this one:
#             for n_mods in range( 1, len(pos_modtypes) ): #number of simultaneous modifications (from 1 to all - 1)
#                 for combination in itertools.combinations(pos_modtypes, n_mods): #A combination with the specified number of modifications
#                     # Create a new digestion peptide:
#                     new_digpep = DigestionPeptide(protein=self.protein,
#                                                   prot_start=self.prot_start,
#                                                   prot_end=self.prot_end,
#                                                   seq=self.seq,
#                                                   digestion_enzyme=self.digestion_enzyme,
#                                                   consensus=False,
#                                                   consensus_peptide=self,
#                                                   _db_modifications = set() )
#                     # Add the modifications corresponding to the current combination of the specified number of modifications:
#                     for pos_modtype in combination:
#                         new_digpep._db_modifications.update( pos_modtype2modifications[pos_modtype] )
#                     combinatorial_peptides.append(new_digpep)
#             #
#             self.combinatorial_peptides = combinatorial_peptides
#             return combinatorial_peptides
#         else: # Not a consensus peptide:
#             return None
#     
    def iter_combinatorial_peptides(self, only_significant=True, 
                                    only_physio=True, mandatory_modids=None,
                                    CombPepCls=None):
        """
        If this is a Consensus Peptide (the one with ALL the possible PTM types 
        per AA), then generate Combinarotial Peptides: peptides like this one 
        but with all the possible combinations of its modifications, and 
        limited to one PTM type per AA.
        
        :param bool only_significant: True -> Only Significant PTMs are 
        considered, False -> All PTMs are considered. Defaults to True.
        :param bool only_physio: True -> only PTMs with clear physiological 
        origin are considered, False -> all PTMs are considered. Defaults to True.
        :param iterable or None mandatory_modids: required Modification IDs to 
        be present in the Combinatorial Peptides generated. Should be like
        [ (id1 OR id2) AND (id3 OR id4) AND ... ]. Defaults to None.
        :param class or None CombPepCls: class to use to create resulting 
        Combinatorial Peptides and store its PTM information. If None is provided, 
        defaults to self class (:class:`DigestionPeptide`).
        
        :return generator : yields DigestionPeptide(consensus=False,
        consensus_peptide=self).
        """
        if self.consensus: # Only for Consensus Peptides the returned generator can yield something:
            # Set the default class to store resulting Combinatorial Peptides information:
            if CombPepCls is None:
                CombPepCls = self.__class__
            # Get modifications grouped by position and modification type.
            pos_modtype2mods = self._build_pos_modtype2modifications(only_significant, 
                                                                     only_physio,
                                                                     pos_sorted=False)
            # Reorganize data to meet :class:``itertools.product`` input: group 
            # Modification Types by position, and add None (the possibility of 
            # No Modification at this position):
            NO_PTM_OPT = (0, None) #Indicates that one possibility is not to have any PTM
            pos2posmodtype_opts = defaultdict( lambda: [NO_PTM_OPT] ) #{ position: [ (0, None), ( position, ModificationType() ), ... ] }
            for pos, modtype in pos_modtype2mods.keys():
                pos2posmodtype_opts[pos].append( (pos, modtype) )
            # Cache needed attributes for speed-up:
            protein = self.protein
            prot_start = self.prot_start
            prot_end = self.prot_end
            seq = self.seq
            dig_condition = self.digestion_condition
            missing_clvgs = self.missing_cleavages
            VOID_SET = set()
            # Generate all the possible combinations of peptides, with one PTM 
            # per AA, from this Digestion Peptide:
            for comb_posmodtypes in product( *pos2posmodtype_opts.values() ): #X_NOTE: use :class:``itertools.product`` instead of :func:``general.basic_func.combine`` to avoid excessive memory consumption and speed-up.
                # Post-Filter resulting PTM combination:
                modids_comb = [modtype.id for _, modtype in comb_posmodtypes
                               if modtype is not None]
                # -Filter by presence of required PTMs:
                if ( mandatory_modids is not None and #Filter is required...
                     any(map( set(modids_comb).isdisjoint, mandatory_modids )) ): #And any mandatory modification is not present in the combination of modifications:
                    continue # Skip this combination.
                #
                # Get the modifications corresponding to the current 
                # combination of the specified number of modifications:
                comb_mods = set()
                for pos_modtype in comb_posmodtypes:
                    comb_mods.update( pos_modtype2mods.get( pos_modtype, VOID_SET ) )
                # Create a new Combinatorial Peptide:
                comb_digpep = CombPepCls(protein=protein, 
                                         prot_start=prot_start, 
                                         prot_end=prot_end, 
                                         seq=seq, 
                                         digestion_condition=dig_condition, 
                                         missing_cleavages=missing_clvgs,  
                                         consensus=False, 
                                         consensus_peptide=self, 
                                         _db_modifications=comb_mods)
                #
                yield comb_digpep
    
    def make_combinatorial_peptides(self, only_significant=True, only_physio=True):
        """
        If this is a consensus peptide (the one with all the possible PTMs per 
        AA), generates peptides like this one but with all the possible 
        combinations of its modifications, but limited to one PTM per AA.
        :caution: It can use a LOT of memory!!!
        
        :return list or None : If this is a consensus peptide, returns a list
        like [ DigestionPeptide(consensus=False, consensus_peptide=self), ... ]; 
        otherwise, it returns None.
        """
        if not self.consensus: # Not a consensus peptide:
            return None
        #
        # Get ALL the possible combinations of peptides, with 1 PTM per
        # AA, from this Digestion Peptide:
        combinatorial_peptides = list( self.iter_combinatorial_peptides(only_significant, only_physio) ) # List for the Digestion Peptides with all possible combinations of PTMs.
        # "Store" and return combinatorial peptides:
        self.combinatorial_peptides = combinatorial_peptides
        return combinatorial_peptides


class DigestionPeptide(Base, DigestionPeptideNoDB):
    """
    Digestion Peptides. The peptides resulting from Protein digestion
    by a Protease (like trypsin). They map all the possible modifications found
    in their corresponding Protein region.
    This is the version of the class linked to a SQLAlchemy Database.
    For the Database-unlinked version see super-class :class:`DigestionPeptideNoDB`.
    """
    __tablename__ = 'digestionpeptides'
    
    id = Column(Integer, Sequence('digpep_id_seq'), primary_key=True,
                autoincrement=True)
    prot_start = Column(Integer) #[protein start position - protein end position] both included,
    prot_end = Column(Integer)   #and both are biological positions (with start at 1, not 0 as in python).
    seq = Column(String(200), nullable=False, index=True) #Should be provided, or it will be inferred during :method:`__init__` from :attr:``protein.seq``, :attr:`prot_start` and :attr:`prot_end`.
    missing_cleavages = Column(Integer, index=True) #Number of missing cleavages that this peptide has (None if it's unknown).
    consensus = Column(Boolean) #Should be provided, or it will default to True during :method:`__init__`.
    _db_seq_w_mods = Column( String(350) ) #Cached sequence with modifications.
    _db_mod_aas = Column(Integer, index=True) #Cached number of Significant and clear Physiological modified amino acids. Added to speed up Database queries.
#     _db_mod_type_ids = Column(JSONListField, index=True) #Cached sorted list of Significant modification type IDs. X-NOTE: Only add if more query speed up is required.
    
    # Relationships:
    # - one-to-many protein <-> Protein.digestion_peptides (set) :
    protein_ac = Column( String(25), ForeignKey(Protein.ac) )
    protein = relationship('Protein', back_populates='digestion_peptides')
    # - one-to-many digestion_condition <-> DigestionCondition.digestion_peptides (set) :
    digestion_condition_id = Column( Integer, ForeignKey(DigestionCondition.id) )
    digestion_condition = relationship('DigestionCondition', back_populates='digestion_peptides')
    # - many-to-many _db_modifications (set) <-> ProtModification.digestion_peptides (set) :
    _db_modifications = relationship('ProtModification',
                                     secondary='digestionpeptides_protmodifications',
                                     collection_class=set,
                                     back_populates='digestion_peptides')
    # - one-to-None: consensus_peptide -> DigestionPeptide :
    consensus_peptide_id = Column( Integer, ForeignKey('digestionpeptides.id') )
#     consensus_peptide = relationship('DigestionPeptide',
#                                      remote_side=[id],
#                                      back_populates='combinatorial_peptides')
    consensus_peptide = relationship('DigestionPeptide', remote_side=[id])
    
#     # Back-references:
#     # - adjacency-list many-to-one: combinatorial_peptides <-> DigestionPeptide.consensus_peptide : #X_NOTE: Removed because causing memory leaks when generating possible co-modified peptides from Consensus Digestion Peptides in :method:``tcellxtalk.views.ProteinDigestCombineView._filtered_combdigpeps``).
#     combinatorial_peptides = relationship('DigestionPeptide',
#                                           collection_class=list,
#                                           back_populates='consensus_peptide')
    
    def __init__(self, *args, **kwargs):
        """
        :caution: `protein`, `prot_start` and `prot_end` are required keyword 
        arguments.
        """
        # Avoids :method:`__init__` from super-class :class:`Base`:
        DigestionPeptideNoDB.__init__(self, *args, **kwargs)
    
    @reconstructor
    def __init_on_dbload__(self): #IMPROVE: maybe it should search for Digestion Peptides in the Database that have this one as their :attr:`consensus_peptide`...
        self.combinatorial_peptides = list()
    
    def mspeptides(self, exact_ptms=False, only_w_sig_ptms=True, 
                   eagerload_ptms=True, db_session=None):
        """
        Returns the Mass Spectrometry Peptides from the database that are like
        or similar to this Digestion Peptide.
        
        :param bool exact_ptms: if True, search for MS Peptides based in
        sequence with modifications :attr:`seq_w_mods` (take PTMs into
        account). If False, compare only sequence (:attr:`seq`). Defaults to
        False.
        :param bool only_w_sig_ptms: True -> only MS Peptides with Significant 
        and clear Physiological PTMs are considered, False -> all MS Peptides 
        are considered. Defaults to True.
        :param bool eagerload_ptms: if True, populate also the related 
        collection :attr:`_db_modifications` and the related :attr:`source` of 
        each MS Peptide found. Defaults to True.
        :param Session db_session: a SQLAlchemy :class:`Session` instance. If
        None, try to get a Session from the current Digestion Peptide instance, 
        if available.
        
        :return set : a set of :class:`MSPeptide` instances.
        """
        if not db_session:
            db_session = session_from_instance(self)
        #
        return super(DigestionPeptide, self).mspeptides(exact_ptms, only_w_sig_ptms, 
                                                        eagerload_ptms, db_session)
    
    @Memoized
    def singularity(self, dig_pep=True, protein_seq=False, db_session=None): #IMPROVE!
        """
        :return bool :
        """
        if not db_session:
            db_session = session_from_instance(self)
        #
        return super(DigestionPeptide, self).singularity(dig_pep, protein_seq, 
                                                         db_session)


digpep2protmod = Table('digestionpeptides_protmodifications', Base.metadata,
    Column('digestionpeptide_id', Integer, ForeignKey(DigestionPeptide.id), nullable=False),
    Column('protmodification_id', Integer, ForeignKey(ProtModification.id), nullable=False)
                       )


class GOAnnotation(Base, ReprMixin):
    """
    Associations between :class:`Protein` instances and :class:`GOTerm`
    instances. Supplies additional information such as the ID of the Protein in
    the associated repository.
    """
    __tablename__ = 'goannotations'
    __repr_init_attr__ = ('id', 'protein_ac', 'qualifier', 'go')
    __qualifier_allowed_vals__ = ('', 'NOT', 'CONTRIBUTES_TO', 'COLOCALIZES_WITH',
                                  'NOT|CONTRIBUTES_TO', 'NOT|COLOCALIZES_WITH')

    id = Column(Integer, Sequence('goann_id_seq'), primary_key=True,
                autoincrement=True)
    qualifier = Column(Enum(*__qualifier_allowed_vals__))
    with_from = Column(JSONDictField)
    original_source = Column( String(50) )

    # Relationships:
    # - one-to-None: go -> GOTerm :
    go_id = Column( String(10), ForeignKey(GOTerm.id) )
    go = relationship('GOTerm')
    # - one-to-many: evidence <-> Evidence.goannotations (set) :
    evidence_id = Column( Integer, ForeignKey(Evidence.id) )
    evidence = relationship('Evidence', back_populates='goannotations')
    # - one-to-many: protein <-> Protein.goannotations (set) :
    protein_ac = Column( String(25), ForeignKey(Protein.ac) )
    protein = relationship('Protein', back_populates='goannotations')
    # - one-to-None: source -> Source :
    source_id = Column( Integer, ForeignKey(Source.id) )
    source = relationship('Source')
    
    def __init__(self, *args, **kwargs):
        kwargs.setdefault( 'with_from', dict() ) #Set default for :attr:`with_from` when object is created (not loaded from database).
        super(GOAnnotation, self).__init__(*args, **kwargs)

    @validates('qualifier')
    def validate_qualifier(self, attr, value):
        return validate_enum(self, attr, value, self.__qualifier_allowed_vals__)


def create_proteases():
    trypsin = Protease(id=1, name='Trypsin with exceptions', 
                       target_seq_re="(?<!^)([RK])(?!P)(?!$)", #Search a R or a K not followed with P (?!P), not at the beginning of the sequence (?<!^), and not at the end (?!$)
                       relative_cut_pos=1, #Cut at C-Term of the only AA (1) of the target sequence (R or K)
                       target_ptm_exceptions={ 'K': [1, 121] }, #Don't cut if target sequence is an acetylated or ubiquitinated (GG) K
                       context_length=1, #Take 1 AA before and 1 AA after the target sequence, as context
                       context_rules4cuttype=[ ['[RK]$', '', 2], ['', '^[RK]', 2] ],  #Generate a 'ambiguous cut position' (2: don't cut and cut) if the previous AA is R or K OR the next AA is R or K
                       max_contiguousambiguous=2)
    
    trypsin_no_excp = Protease( id=2, name='Trypsin', 
                                target_seq_re="(?<!^)([RK])(?!P)(?!$)", #Search a R or a K not followed with P, not at the beginning of the sequence, and not at the end
                                relative_cut_pos=1, #Cut at C-Term of the only AA (1) of the target sequence (R or K)
                                target_ptm_exceptions={ 'K': [1, 121] } ) #Don't cut if target sequence is an acetylated or ubiquitinated (GG) K
    
    gluc = Protease( id=3, name='GluC (DE)', target_seq_re="(?<!^)([DE])(?!P)(?!$)", #Search a D or a E not followed with P, not at the beginning of the sequence, and not at the end
                     relative_cut_pos=1, #Cut at C-Term of the only AA (1) of the target sequence (D or E)
                     digpep_omited_ptms=[121] ) #Exclude Ubiquitination from the resulting Digestion Peptides.
    
    return [trypsin, trypsin_no_excp, gluc]

