# -*- coding: utf-8 -*-
"""
:synopsis: Common Basic Classes for Proteomics.

:created:    2013/06/26

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.3
:updated:    2014-10-16
"""

#===============================================================================
# Imports
#===============================================================================
from __future__ import division

import re
from collections import defaultdict, Mapping, namedtuple

import db_models

#===============================================================================
# Class definitions
#===============================================================================
class Container(object):
    def __init__(self, trans_tbl, DEFAULT_ATTRIB = None, csv_dict = None):
        if csv_dict:
            for attrib, (field, converter) in trans_tbl.items():
                if field and converter:
                    value = converter( csv_dict[field] )
                elif field and not converter:
                    value = csv_dict[field]
                elif not field and converter:
                    value = converter()
                
                setattr( self, attrib, value)
        elif DEFAULT_ATTRIB:
            for attrib, value in DEFAULT_ATTRIB.items():
                setattr(self, attrib, value)
        
        self._subcontainers = None
    
    def shallow_copy(self):
        new_copy = self.__class__()
        new_copy.__dict__.update(self.__dict__)
        return new_copy
    
    def common_prots_with(self, *other_containers, **kwargs):
        prots_property = kwargs.get('prots_property', 'non_control_prots')
        common_prots = getattr(self, prots_property).copy()
        
        for other_container in other_containers:
            other_prots = getattr(other_container, prots_property)
            for prot_ac in common_prots.keys():
                if not other_prots.has_key(prot_ac):
                    del(common_prots[prot_ac])
                    
        return common_prots
    
    def _prot_peps(self, protein):
        raise NotImplementedError
    
    def coverage(self, protein):
        prot_peps = self._prot_peps(protein)
        covered_pos = set()
        for pep in prot_peps:
            for match in re.finditer(pep.seq, protein.seq, re.I):
                covered_pos.update( range(match.start(), match.stop()) )
        return '{0}%'.format( len(covered_pos) / len(protein.seq) * 100 )
    

PSiteSeqData = namedtuple('PSiteData', ['proteins', 'peptides'])


ProteinPSiteData = namedtuple('ProteinPSiteData', ['protein', 'ac', 'position', 
                                                   'aa'])


PeptidePSiteData = namedtuple('PeptidePSiteData', ['peptide', 'seq', 'ascore'])


class PSite(object):
    def __init__(self, prot_ac=None, position=0, p_aa='', max_ascore=0, 
                 time_regulation=None, supporting_peps=None):
        self.prot_ac = prot_ac
        self.position = position
        self.p_aa = p_aa
        self.max_ascore = max_ascore
        if not time_regulation:
            self.time2regulations = dict() # {time: regulation} ; and regulation can be -1 (down), 0 (no-change), or 1 (up)
        if not supporting_peps:
            self.supporting_peps = set()
    
    def __repr__(self):
        return ('PSite({prot_ac}, {position}, {p_aa}, {max_ascore}, '
                '{time_regulation}, {supporting_peps})').format(**self.__dict__)


class Bacteria(Container):
    def __init__(self, **kwargs):
        for attrib in ['id', 'valid_strains', 'strains']:
            setattr( self, attrib, kwargs.get(attrib, None) )
    
    def _prot_peps(self, protein):
        prot_peps = set()
        if protein.id in self.non_control_prots.keys():
            for strain in self.strains.values():
                prot_peps.update( strain._prot_peps(protein) )
        return prot_peps
                
    @property    
    def proteins(self):
        '''
        {proteins from all samples from all its strains}
        '''
        proteins = dict()
        for strain in self.strains.values():
            proteins.update(strain.proteins)
        return proteins
    
    @property    
    def control_prots(self):
        '''
        {proteins from all Control samples from all its strains}
        '''
        control_prots = dict()        
        for strain in self.strains.values():
            control_prots.update(strain.control_prots)
        return control_prots
    
    @property    
    def test_prots(self):
        '''
        {proteins from all Test samples from all its strains}
        '''
        test_prots = dict()
        for strain in self.strains.values():
            test_prots.update(strain.test_prots)
        return test_prots
    
    @property    
    def non_control_prots(self):
        '''
        (proteins from all Test samples from all its strains) - (proteins from all Control samples from all its strains)
        '''
        non_control_prots = dict()        
        for strain in self.strains.values():
            non_control_prots.update(strain.non_control_prots)
        return non_control_prots
    

class Strain(Container):
    def __init__(self, csv_dict = None, trans_tbl = None, trans_tbl_sel = ''):
        if not trans_tbl and trans_tbl_sel:
            if trans_tbl_sel.upper() == 'OMSSA':
                #OMSSA CSV trans_tbl:
                trans_tbl = {'id': ('File', self._get_id), 
                             }
            elif trans_tbl_sel.upper() == 'PD':
                #Proteome Discoverer TSV trans_tbl:
                trans_tbl = {'id': ('File', self._get_id), 
                             }
            
        super(Strain, self).__init__(trans_tbl, None, csv_dict)
        
        self.samples = dict()
        self.proteins = dict()
    
    def _get_id(self, raw_filen):
        return raw_filen.split('_')[1]

    def _get_id_PD_TSV(self, raw_filen):
        return raw_filen.split('_')[0].lstrip('MultiConsensus')
    
    def _prot_peps(self, protein):
        prot_peps = set()
        if protein.id in self.non_control_prots.keys():
            for sample in self.samples.values():
                prot_peps.update( sample._prot_peps(protein) )
        return prot_peps

    @property
    def control_prots(self):
        '''
        {proteins from all Control samples}
        '''
        control_prots = dict()
        for sample in self.samples.values():
            if sample.control:
                control_prots.update(sample.proteins)
        return control_prots
    
    @property
    def test_prots(self):
        '''
        {proteins from all Test(not Control) samples}
        '''
        test_prots = dict()
        for sample in self.samples.values():
            if not sample.control:
                test_prots.update(sample.proteins)
        return test_prots
    
    @property
    def non_control_prots(self):
        '''
        {(Test proteins) - (Control proteins)}
        '''
        non_control_prots = dict()
        test_prots = self.test_prots
        control_prots = self.control_prots
        for prot_id, prot in test_prots.items():
            if prot_id not in control_prots:
                non_control_prots[prot_id] = prot
        return non_control_prots
    
        
class Sample(Container):
    def __init__(self, csv_dict = None, trans_tbl = None, trans_tbl_sel = ''):
        if not trans_tbl and trans_tbl_sel:
            if trans_tbl_sel.upper() == 'OMSSA':
                #OMSSA CSV trans_tbl:
                trans_tbl = {'id':         ('File', None), 
                             'control':    ('File', self._get_control),
                             'dig_hours':  ('File', self._get_hours),
                             'v_analised': ('File', self._get_v_analised),
                             }
            elif trans_tbl_sel.upper() == 'PD':
                #PD TSV trans_tbl:
                trans_tbl = {'id':      ('File', None),
                             'control': ('File', self._get_control_PD_TSV),
                             }
                         
        super(Sample, self).__init__(trans_tbl, None, csv_dict)
             
        self.psms = defaultdict(list)
        self.proteins = dict()
        #self.prots2psms = defaultdict(list)
    
    def _get_control(self, raw_filen):
        return ('_C_' in raw_filen.upper())

    def _get_control_PD_TSV(self, raw_filen):
        return ('CONTROL' in raw_filen.upper())
        
    def _get_hours(self, raw_filen):
        raw_filen = raw_filen.upper()
        match = re.search('_DIG(\d+)H_', raw_filen)
        return int(match.group(1))

    def _get_v_analised(self, raw_filen):
        raw_filen = raw_filen.upper()
        match = re.search('H_(\d+)%_', raw_filen)
        return int(match.group(1))
    
    def _prot_peps(self, protein):
        return protein.peptides.intersection(self.psms.keys())
    

class Protein_OLD(Container):
    """
    DEPRECATED! Do not use this. Use new :class:db_models.Protein instead.
    """
    def __init__(self, csv_dict = None, trans_tbl = None, trans_tbl_sel = ''):
        if not trans_tbl and trans_tbl_sel:
            trans_tbl_sel = trans_tbl_sel.upper()
            if trans_tbl_sel == 'OMSSA':
                #OMSSA CSV trans_tbl:
                trans_tbl = {'ac'  :     ('Defline', self._get_ac), 
                             'name':     ('Defline', self._get_name),
                             'gene':     ('Defline', self._get_gene),
                             'organism': ('Defline', self._get_organism),
                             'evidence': ('Defline', self._get_evidence),
                             'database': ('Defline', self._get_db),
                             }
            elif trans_tbl_sel == 'PD':
                #Proteome Discoverer TSV trans_tbl:
                trans_tbl = {'ac'  :      ('Accession', None), 
                             'name':      ('Description', self._get_name_PD),
                             'gene':      ('Description', self._get_gene),
                             'organism':  ('Description', self._get_organism),
                             'evidence':  ('Description', self._get_evidence),
                #             'coverage':  ('Sum(Coverage)', self._get_coverage),
                             'locations': ('Cellular Component', self._get_list_PD),
                             'mol_funcs': ('Molecular Function', self._get_list_PD),
                             'bio_funcs': ('Biological Process', self._get_list_PD),
                             'pfam_ids':  ('Pfam IDs', self._get_list_PD),
                             'theo_mw':   ('MW [kDa]', float),
                             'theo_pi':   ('calc. pI', float),
                             }
            elif trans_tbl_sel == 'SPHPP-FASTA':
                #SwissProt/Uniprot SpHPP FASTA trans_tbl:
                trans_tbl = {'ac'  :      ('Defline', self._get_ac), 
                             'proteinID': ('Defline', self._get_ensprot),
                             'name':      ('Defline', self._get_name),
                             'gene':      ('Defline', self._get_gene),
                             'geneID':    ('Defline', self._get_ensgene),
                             'chromosome':('Defline', self._get_chromosome),
                             'organism':  ('Defline', self._get_organism),
                             'evidence':  ('Defline', self._get_evidence),
                             'database':  ('Defline', self._get_db),
                             }
            elif trans_tbl_sel == 'SP_CHR_TXT':
                #SwissProt/Uniprot SpHPP FASTA trans_tbl:
                trans_tbl = {'ac'  :      ('Fileline', self._get_ac_chrfline), 
                             'name':      ('Fileline', self._get_name_chrfline),
                             'gene':      ('Fileline', self._get_gene_chrfline),
                             'chromosome':('Chrm', None),
                             'organism':  ('OS', None),
                             'database':  ('Filen', None),
                             }
        
        self.revised = None
        super(Protein, self).__init__(trans_tbl, None, csv_dict)
        
        self.seq = ''
        self.strains = dict()
        self.peptides = set()
        
    def __len__(self):
        return len(self.seq)
    
    def _get_ac(self, def_line):
        '''
        Get the protein accession number
        '''
        return def_line.split('|')[1]
    
    def _get_ac_chrfline(self, fileline):
        '''
        Get the protein accession number
        '''
        return fileline[27:33]
    
    def _get_ensprot(self, def_line):
        '''
        Get the ENSemble protein ID
        '''
        match = re.search('Protein=(ENSP\w*)', def_line)
        if match:
            return match.group(1)
        else:
            return ''
    
    def _get_name(self, def_line):
        match = re.search('\|[-\w]* (.*?)($| \w\w=)', def_line)
        return match.group(1)

    def _get_name_chrfline(self, fileline):
        return fileline[53:]

    def _get_name_PD(self, def_line):
        match = re.search('^(.*?)($| \w\w=)', def_line)
        return match.group(1)
        
    def _get_gene(self, def_line):
        match = re.search('GN=([-\w]*)', def_line)
        if match:
            return match.group(1)
        else:
            return ''
    
    def _get_gene_chrfline(self, fileline):
        return fileline[0:11].rstrip()
    
    def _get_ensgene(self, def_line):
        '''
        Get the ENSemble gene ID
        '''
        match = re.search('Gene=(ENSG\w*)', def_line)
        if match:
            return match.group(1)
        else:
            return ''
    
    def _get_chromosome(self, def_line):
        '''
        Get the chromosome in witch the protein is coded
        '''
        match = re.search('Chromosome=(\w*)', def_line)
        if match:
            return match.group(1)
        else:
            return ''
    
    def _get_organism(self, def_line):
        match = re.search('OS=(.*?)($| \w\w=)', def_line)
        if match:
            return match.group(1)
        else:
            return ''
    
    def _get_evidence(self, def_line):
        #Protein Evidence codes from Uniprot (http://www.uniprot.org/manual/protein_existence):
        # 1. Evidence at protein level
        # 2. Evidence at transcript level
        # 3. Inferred from homology
        # 4. Predicted
        # 5. Uncertain
        match = re.search('PE=(\d)', def_line)
        if match:
            return int(match.group(1))
        else:
            return 0
    
    def _get_db(self, def_line):
        match = re.search('^>?(.+?)\|', def_line)
        db = match.group(1).upper()
        self.revised = db == 'SP'
        return db
    
    def _get_coverage(self, coverage_str):
        return float(coverage_str.rstrip('%'))
    
    def _get_list_PD(self, list_str):
        if list_str:
            return list_str.split('; ')
        else:
            return list()
        

class PSM(Container):
    def __init__(self, csv_dict = None, trans_tbl = None):
        if not trans_tbl:
            trans_tbl = {'seq'      : ('Peptide', None), 
                         'theo_mass': ('Theo Mass', float),
                         }
        super(PSM, self).__init__(trans_tbl, None, csv_dict)
        
        self.proteins = dict()
        self.sample = None
    
    def __len__(self):
        return len(self.seq)
    
