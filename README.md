---

**WARNING!**: This is the *Old* source-code repository for Proteomics3 shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/sharedpackages/proteomics3_code/) located at https://sourceforge.net/p/lp-csic-uab/sharedpackages/proteomics3_code/**  

---  
  
  
# Proteomics3 shared Package

Python 3.x package for proteomics, and peptide and protein manipulation.


---

**WARNING!**: This is the *Old* source-code repository for Proteomics3 shared Package from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/sharedpackages/proteomics3_code/) located at https://sourceforge.net/p/lp-csic-uab/sharedpackages/proteomics3_code/**  

---  
  