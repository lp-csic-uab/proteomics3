# -*- coding: utf-8 -*-

"""
:created:    2014/10/16

:author:     Óscar (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.1.1'
__UPDATED__ = '2019-09-17'

#===============================================================================
# Global variables
#===============================================================================
# 'delta_mass' are monoisotopic masses.
UNIMOD = {1:  {'unimod_id': 1,
               'delta_mass': 42.010565,
               'unimod_name': 'Acetyl',
               'residues': 'K N-Term',
               'fixed': 'false',
               },
          4:  {'unimod_id': 4,
               'delta_mass': 57.021464,
               'unimod_name': 'Carbamidomethyl',
               'residues': 'C',
               'fixed': 'true',
               },
          21: {'unimod_id': 21,
               'delta_mass': 79.966331,
               'unimod_name': 'Phospho',
               'residues': 'S T Y',
               'fixed': 'false',
               },
          23: {'unimod_id': 23,
               'delta_mass': -18.010565,
               'unimod_name': 'Dehydrated',
               'residues': 'S T Y',
               'fixed': 'false',
               },
          35: {'unimod_id': 35,
               'delta_mass': 15.994915,
               'unimod_name': 'Oxidation',
               'residues': 'M',
               'fixed': 'false',
               },
          121:{'unimod_id': 121,
               'delta_mass': 114.042927,
               'unimod_name': 'GG',
               'residues': 'K',
               'fixed': 'false',
               },
          214:{'unimod_id': 214,
               'delta_mass': 144.102063,
               'unimod_name': 'iTRAQ4plex',
               'residues': '.',
               'fixed': 'false',
               },
          737:{'unimod_id': 737,
               'delta_mass': 229.162932,
               'unimod_name': 'TMT6plex',
               'residues': '.',
               'fixed': 'false',
               },
         }

